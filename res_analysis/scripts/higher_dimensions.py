#!/usr/bin/env python3

#from __future__ import print_function
import numpy as np
import matplotlib
import matplotlib.colors as colors
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
import mimic_alpha as ma
from mpl_toolkits.axes_grid1 import make_axes_locatable
from sklearn.decomposition import PCA
from sklearn.mixture import GMM
from reconstruct import breit_wigner, danger

np.set_printoptions(linewidth=120)
#matplotlib.rcParams.update({'font.size': 18})

def main():
  R = np.load('../data/res_compact.npy')
  R = R.astype(np.float64)
  R[np.isnan(R)] = 0
  R[np.isinf(R)] = 0
  elem = get_symbols('../../pt-data1.csv')
#  d_arr = compute_danger(np.copy(R), elem)

  plot_danger(E, np.log10(D))
#  plot_widths_ratio(np.copy(R))
#  plot_principal(np.copy(R))
#  plot_covar(np.copy(R))
#  plot_energy_width(np.copy(R))
#  plot_widths_hist(np.copy(R))

  plt.show()

def peaking_factor(J, GN, GX, ER, GT, s2, s1=0.5):
  peak = breit_wigner(ER, J, GN, GX, ER, GT, s2, s1)
  left = breit_wigner(ER-10*GT, J, GN, GX, ER, GT, s2, s1)
  right = breit_wigner(ER+10*GT, J, GN, GX, ER, GT, s2, s1)
  background = (left + right) / 2
  return peak/background

def compute_danger(X, elem):
#  X = X[np.where(X[:,0]==82),:][0]
  d_arr = np.array([danger_factor(res[11]+res[13],res[4],res[6]) for res in X])
  d_nuclide = [(elem[res[0]],res[0]+res[1],res[4],res[6],res[11]+res[13],peaking_factor(res[5],res[10],res[10],res[4],res[6],1)) for res in X]
  sorted_danger = sorted(zip(d_arr,d_nuclide), key=lambda pair: pair[0])
  smallest_danger = sorted_danger[:20]
  largest_danger = sorted_danger[-50:]
#  print('Danger                Nuclide      Energy   Width     Resonance Int.   Peaking Factor')
#  for resonance in sorted_danger:
#    print(resonance)
  return d_arr

def get_symbols(filename):
  with open(filename) as f:
    PT = f.readlines()
  Z = []
  sym = []
  for e in PT:
    Z.append(int(e.split(',')[0]))
    sym.append(e.split(',')[1].strip())
  return dict(zip(Z, sym))

def plot_covar(X):
  col_str = ['$Z$','$N$','$LRF$','$l$','$E_{r}$','$j$','$\Gamma_{t}$','$\Gamma_{f}$','$\Gamma_{fa}$','$\Gamma_{fb}$','$\Gamma_{n}$','$\int \ R_{n}(E)$','$\Gamma_{\gamma}$','$\int \ R_{\gamma}(E)$']
  CM = np.corrcoef(X, rowvar=0)
  fig, ax = plt.subplots(figsize=(11,10))
  im = ax.matshow(CM, cmap=matplotlib.cm.RdBu, vmin=-1, vmax=1)
  divider = make_axes_locatable(ax)
  cax = divider.append_axes('right', size="5%", pad=0.05)
  ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
  ax.yaxis.set_major_locator(ticker.MultipleLocator(1))
  ax.set_xticklabels([''] + col_str, fontsize=12)
  ax.set_yticklabels([''] + col_str, fontsize=12)
  fig.colorbar(im, cax=cax, cmap=matplotlib.cm.RdBu)
  plt.subplots_adjust(top=0.85, bottom=0.03, left=0.1)
  ax.set_title('Correlation matrix for ENDF/B-VII.1 resonance parameters', y=1.1)
  plt.savefig('cor.eps', format='eps', dpi=254)

def plot_widths_ratio(X, **kwargs):
  n = X[:,10]
  g = X[:,12] 
  fig = plt.figure(figsize=(12,8))
  ax = fig.add_subplot(111)
  ax.set_xscale('log')
  ax.set_yscale('log')
  ax.grid()
  ax.set_xlabel(r'Width ratio, $\frac{\Gamma_{n}}{\Gamma_{\gamma}}$ [eV]')
  ax.set_ylabel('Frequency')
  with np.errstate(divide='ignore'):
    ratio = n/g
  print(len(ratio[np.isnan(ratio)]) + len(ratio[np.isinf(ratio)]))
  ratio[np.isnan(ratio)] = 1E20
  ratio[np.isinf(ratio)] = 1E20
  n, bins, n_patches = ax.hist(ratio, bins=np.logspace(-10, 10, 200), color='purple', alpha=0.5)
  ax.set_title('Ratio of neutron to gamma widths in ENDF/B-VII.1')
  plt.subplots_adjust(bottom=0.16)
  plt.savefig('width_hist_ratio.eps', format='eps', dpi=254)

def plot_widths_hist(X, **kwargs):
  n = X[:,10]
  g = X[:,12]
#  bin_arr = np.logspace(np.floor(np.log10(n.min())), np.ceil(np.log10(n.max())), 50)
  bin_arr = np.logspace(-10, 10, 41)
  fig = plt.figure(figsize=(12,8))
  ax = fig.add_subplot(111)
  ax.set_xscale('log')
  ax.set_xlim(1E-10, 1E10)
  ax.set_yscale('log')
  ax.grid()
  ax.set_xlabel('Resonance width, $\Gamma_{x}$ [eV]')
  ax.set_ylabel('Frequency')
  n, bins, n_patches = ax.hist(n, bins=bin_arr, color='b', alpha=0.5)
  n, bins, g_patches = ax.hist(g, bins=bin_arr, color='g', alpha=0.5)
  ax.legend([n_patches[0], g_patches[0]],['Neutron scatter', 'Radiative capture'])
  ax.set_title('Resonance widths in ENDF/B-VII.1')
  plt.savefig('width_hist.png', format='png', dpi=800)

def plot_energy_width(X):
  x = X[:,4]
  y = X[:,10]
  Z = X[:,0]
  fig, ax = plt.subplots(figsize=(9,8))
  im = ax.scatter(x, y, c=Z, s=64)
  divider = make_axes_locatable(ax)
  cax = divider.append_axes('right', size="5%", pad=0.05)
  xmin = 1E-2
  xmax=x.max() * 10
  ymin = 1E-10
  ymax=y.max() * 10
  ax.set_xscale('log')
  ax.set_xlim(xmin, xmax)
  ax.set_yscale('log')
  ax.set_ylim(ymin, ymax)
  ax.set_xlabel('Resonance energy [eV]')
  ax.set_ylabel('$\Gamma_{n}$ [eV]')
  ax.grid()
  fig.colorbar(im, cax=cax, cmap=matplotlib.cm.jet, label='Z')
  ax.set_title('Resonances in ENDF/B-VII.1')
  plt.savefig('width_energy.eps', format='eps', dpi=254)

def plot_principal(X):
  E = np.copy(X[:,4])
  Z = np.copy(X[:,0])
  X /= np.std(X, axis=0)
  pca = PCA(n_components=2).fit(X)
  print('PCA components & variance')
  print(pca)
  print(pca.components_)
  print(pca.explained_variance_)
  print(pca.explained_variance_ratio_)
  Xpca = PCA(n_components=2).fit_transform(X)
# gmm = GMM(2, covariance_type='full', random_state=0)
# gmm.fit(Xpca)
# cluster_label = gmm.predict(Xpca)
  x = Xpca[:,0]
  y = Xpca[:,1]
  fig, ax = plt.subplots(figsize=(9,8))
#  im = ax.scatter(x, y, c=E, s=64, norm=colors.LogNorm(vmin=E.min(), vmax=E.max()))
  im = ax.scatter(x, y, c=Z, s=64)
  divider = make_axes_locatable(ax)
  cax = divider.append_axes('right', size="5%", pad=0.05)
  xmin=1E-4
  xmax=x.max() * 10
  ymin=1E-3
  ymax=y.max() * 10
  ax.set_xscale('log')
  ax.set_xlim(xmin, xmax)
  ax.set_yscale('log')
  ax.set_ylim(ymin, ymax)
  ax.set_xlabel('PCA 1')
  ax.set_ylabel('PCA 2')
  ax.grid()
  fig.colorbar(im, cax=cax, cmap=matplotlib.cm.jet, label='Z')
  ax.set_title('PCA of ENDF/B-VII.1')

if __name__ == "__main__":
  main()
