#!/usr/bin/env python3

import matplotlib
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.ticker as ticker
import numpy as np
from glob import glob
from scipy.integrate import quad

matplotlib.rcParams.update({'font.size': 22})
tiny = 1E-18

def main():
  check_for_resonances()
  return

def check_for_resonances():
  lrps = make_mat(fill=-1)
  lrfs = make_mat(fill=-1)
  RIN_first_binary = make_mat(fill=-1)
  first_res = make_mat(dtype=np.float64, fill=tiny)
  largest_RIN = make_mat(dtype=np.float64, fill=tiny)
  largest_RIG = make_mat(dtype=np.float64, fill=tiny)
  important_N_res = make_mat(dtype=np.float64, fill=tiny)
  important_G_res = make_mat(dtype=np.float64, fill=tiny)
  largest_RIN_width = make_mat(dtype=np.float64, fill=tiny)
  largest_RIG_width = make_mat(dtype=np.float64, fill=tiny)

  RT = np.empty(shape=(0,14))
  MP = 3321
  RT4 = np.zeros(shape=(100,155,MP,12))
  max_p = 0
  for endf_file in glob('../endfb7_data/*.dat'):
    section = get_section(endf_file, 1, 451)
    z, n = get_z_n(section[0].split()[0])
    print(z, n, endf_file)
    print('LRF, Z, N, Z+N, SPI, L, J, ER, GT, GF, GFA, GFB, GN, GG, RIN, RIG')
    LRP = int(section[0].split()[2])
    lrps[z,n] = LRP

    if LRP == 1:
      NER, EL, EH, LRU, LRF, NLS, SPI = read_resonance_header(endf_file)
      lrfs[z,n] = LRF
      RIN_first_binary[z,n] = 0
      if LRF in [1, 2, 3]:
        if LRF in [1, 2]:
          # SLBW or MLBW, resonance list format as follows:
          # ER  J  GT  GN  GG  GF
          params = read_BW_params(endf_file, LRF, NER, NLS, z, n, SPI)
        else:
          # Reich-Moore, resonance list format as follows:
          params = read_RM_params(endf_file, LRF, NER, NLS, z, n, SPI)

        if params.shape[0] > max_p:
          max_p = params.shape[0]
        RT = np.concatenate((RT, params))
        rt4_params = params[:,2:] # discard Z, N information as this is now encoded in other dimension
        pad_length = MP - rt4_params.shape[0] # pad to end of array (set by largest number of params in library)
        npad = ((0,pad_length),(0,0)) # pad after pad_length in rows, do not pad in columns
        padded_params = np.pad(rt4_params, npad, mode='constant', constant_values=0)
        RT4[z,n] = padded_params

        ###################
        # DATA PROCESSING #
        ###################

        # what's the largest resonance when integrated over ER +/- 4*GT energy?
        big_RIN = params[:,11].max()
        big_RIG = params[:,13].max()
        largest_RIN[z,n] = big_RIN
        largest_RIG[z,n] = big_RIG

        # what's the first positive valued resonance energy?
        energy = params[:,4]
        first_res_nrg  = energy.min()
        first_res[z,n] = first_res_nrg

        # is the biggest resonance also the first (+ve) resonance?
        largest_RIN_row_index = np.where(params[:,11]==big_RIN)[0][0]
        largest_RIN_energy = params[largest_RIN_row_index,4]
        largest_RIN_width[z,n] = params[largest_RIN_row_index,10]
        largest_RIG_row_index = np.where(params[:,13]==big_RIG)[0][0]
        largest_RIG_energy = params[largest_RIG_row_index,4]
        largest_RIG_width[z,n] = params[largest_RIG_row_index,12]
        print('Largest partial, resonance index, energy (eV), resonance integral (b eV)')
        print('(n,n\')           {}               {}           {}'.format(largest_RIN_row_index, largest_RIN_energy, big_RIN))
        print('(n,g)            {}               {}           {}'.format(largest_RIG_row_index, largest_RIG_energy, big_RIG))
        if largest_RIN_energy == first_res_nrg:
          RIN_first_binary[z,n] = 1

        # largest resonance collated for histogram, scatter plots
        important_N_res[z,n] = largest_RIN_energy
        important_G_res[z,n] = largest_RIG_energy

      else:
        print("LRF {} resonance formalism not supported, skipping...".format(LRF))
    else:
      print("LRP = {}, no resonance information".format(LRP))
    print('\n')

  np.save('res_compact.npy', RT)
  np.save('res_sparse.npy', RT4)

  plot_nuclides_int(lrps, "Resonance information (LRP) in ENDF/B-VII.1", plt.cm.Blues)

  plot_nuclides_int(lrfs, "Resonance formalism (LRF) in ENDF/B-VII.1", plt.cm.jet)

  plot_nuclides_float(first_res, "First resonance energy in ENDF/B-VII.1", plt.cm.viridis, label_str='Energy (eV)', vmin=smallest_non_tiny(first_res), vmax=first_res.max())

  plot_nuclides_int(RIN_first_binary, "Is the first resonance in ENDF/B-VII.1 the largest?", plt.cm.Purples, vmin=0)

  min_min = min([smallest_non_tiny(largest_RIN), smallest_non_tiny(largest_RIG)])
  max_max = max([largest_RIN.max(), largest_RIG.max()])
  plot_nuclides_float(largest_RIN, "Largest (n,n') resonances in ENDF/B-VII.1", plt.cm.inferno_r, label_str='Individual RI (b$\cdot$eV)', vmin=min_min, vmax=max_max)
  plot_nuclides_float(largest_RIG, "Largest (n,g) resonances in ENDF/B-VII.1", plt.cm.inferno_r, label_str='Individual RI (b$\cdot$eV)', vmin=min_min, vmax=max_max)

  rows = range(important_N_res.shape[0])
  cols = range(important_N_res.shape[1])
  with open('imp_res', 'w') as f:
    for Z in rows:
      for N in cols:
        EN = important_N_res[Z,N]
        EG = important_G_res[Z,N]
        if (EN > tiny) or (EG > tiny):
          f.write("{:3d} {:3d} {:1.6E} {:1.6E} {:1.6E} {:1.6E} {:1.6E} {:1.6E}\n".format(Z, Z+N, EN, largest_RIN_width[Z,N], largest_RIN[Z,N], EG, largest_RIG_width[Z,N], largest_RIG[Z,N]))

  plt.show()

def breit_wigner_glasstone(E, A, GN, GX, ER, GT, SPI):
  """Glasstone and Sesonske - Nuclear Reactor Engineering 4th ed. §2.144 pg. 89"""
  return A/E * GN*GX / (E*(E-ER)**2 + GT**2/4)

def breit_wigner(E, J, GN, GX, ER, GT, s2, s1=0.5):
  """http://www-pnp.physics.ox.ac.uk/~libby/Teaching/2005/C4-JL-lecture2.pdf
  E = energy, eV
  J = resonance spin
  GN = neutron partial width, eV
  GX = decay partial width, eV
  ER = resonance energy, eV
  GT = total width, eV,
  s1 = neutron spin
  s2 = target spin"""
  h = 4.135667E-15          # planck constant, eV s
  h_bar = h / (2*np.pi)     # reduced planck constant, eV s
  m_n = 1.674927E-27        # neutron mass, kg
  p_n = np.sqrt(2*E*m_n)    # neutron momentum, (eV kg)^0.5
  lambda_bar = h_bar / p_n  
  g = (2*J+1)/((2*s1+1)*(2*s2+1))
#  print(ER, E, lambda_bar, g, 4*np.pi*lambda_bar**2*g*GN*GX*0.25, (E-ER)**2 + (GT**2)/4)
  return (4*np.pi*lambda_bar**2*g*GN*GX*0.25) / ((E-ER)**2 + (GT**2)/4)

def read_BW_params(endf_file, LRF, NER, NLS, Z, N, SPI):
  section = get_section(endf_file, 2, 151)[4:]
  line_num = 0
  data = []
  for L in range(NLS):
    try:
      NRS = int(section[line_num][56:66])        # resonance count for l-value
    except:
      NRS = 0
    line_num += 1
    for line in section[line_num:line_num+NRS]:
      line_num += 1
      ER = ffloat(line[0:11])
      if ER < 0:
        # skip resonances of negative energy
        continue
      J = ffloat(line[11:22])
      GT = ffloat(line[22:33])
      GN = ffloat(line[33:44])
      GG = ffloat(line[44:55])
      GF = ffloat(line[55:66])
      GFA = None
      GFB = None
      if GT == 0:
        GT = GN + GG + GF
        print("No GT entry, summing partial widths.")
      RIN = None
      RIG = None
      E_min = ER-4*GT
      if E_min < 0:
        E_min = 0
      E_max = ER+4*GT
      try:
        RIN = quad(breit_wigner, E_min, E_max, args=(J,GN,GN,ER,GT,SPI))[0]
        if RIN == np.nan:
          RIN = 0
        RIG = quad(breit_wigner, E_min, E_max, args=(J,GN,GG,ER,GT,SPI))[0]
        if RIG == np.nan:
          RIG = 0
        data.append([Z, N, LRF, L, ER, J, GT, GF, GFA, GFB, GN, RIN, GG, RIG])
        print([LRF, Z, N, Z+N, SPI, L, J, ER, GT, GF, GFA, GFB, GN, GG, RIN, RIG])
      except Exception as e:
        print("Resonance integration not possible, ", e)
  return np.array(data)

def read_RM_params(endf_file, LRF, NER, NLS, Z, N, SPI):
  section = get_section(endf_file, 2, 151)[4:]
  line_num = 0
  data = []
  for L in range(NLS):
    try:
      NRS = int(section[line_num][56:66])        # resonance count for l-value
    except:
      NRS = 0
    line_num += 1
    for line in section[line_num:line_num+NRS]:
      line_num += 1
      ER = ffloat(line[0:11])
      if ER < 0:
        # skip resonances of negative energy
        continue
      J = ffloat(line[11:22])
      GN = ffloat(line[22:33])
      GG = ffloat(line[33:44])
      GF = None
      GFA = ffloat(line[44:55])
      GFB = ffloat(line[55:66])
      GT = GN + GG + GFA + GFB
      RIN = None
      RIG = None
      E_min = ER-4*GT
      if E_min < 0:
        E_min = 0
      E_max = ER+4*GT
      try:
        RIN = quad(breit_wigner, E_min, E_max, args=(J,GN,GN,ER,GT,SPI))[0]
        if RIN == np.nan:
          RIN = 0
        RIG = quad(breit_wigner, E_min, E_max, args=(J,GN,GG,ER,GT,SPI))[0]
        if RIG == np.nan:
          RIG = 0
        data.append([Z, N, LRF, L, ER, J, GT, GF, GFA, GFB, GN, RIN, GG, RIG])
        print([LRF, Z, N, Z+N, SPI, L, J, ER, GT, GF, GFA, GFB, GN, GG, RIN, RIG])
      except Exception as e:
        print("Resonance integration not possible, ", e)
  return np.array(data)

def plot_histogram(arr, title_str, x_label_str, y_label_str, xlogscale=False, ylogscale=False, bincount=40):
  fig = plt.figure(figsize=(16,12))
  ax = fig.add_subplot(111)
  ax.hist(arr, np.logspace(np.floor(np.log10(arr.min())),np.ceil(np.log10(arr.max())),bincount), color='g')
  ax.set_title(title_str)
  ax.set_xlabel(x_label_str)
  ax.set_ylabel(y_label_str)
  if xlogscale:
    ax.set_xscale('log')
  if ylogscale:
    ax.set_yscale('log')

def plot_nuclides_float(matrix, title_str, cmap, vmin, vmax, label_str=''):
  """Plot chart of nuclides (N, Z) for matrix of integers with Z rows and N columns
      expects data to be >=0, everything below coloured white"""
  fig, ax = get_fig_ax()
  axcolor = fig.add_axes([0.88, 0.1, 0.02, 0.80])
  im = ax.matshow(matrix, cmap=cmap, origin='lower', vmin=vmin, vmax=vmax, norm=mcolors.LogNorm(vmin=vmin, vmax=matrix.max()))
  fig.colorbar(im, cax=axcolor, format='$%1.1E$', label=label_str)
  im.cmap.set_under('white')
  ax.set_title(title_str)
  ax.xaxis.tick_bottom()
  return

def plot_nuclides_int(matrix, title_str, cmap, vmin=0):
  """Plot chart of nuclides (N, Z) for matrix of integers with Z rows and N columns
      expects data to be >=0, everything below coloured white"""
  fig, ax = get_fig_ax()
  ax.matshow(matrix, cmap=cmap.set_under('white'), origin='lower', vmin=vmin, vmax=matrix.max())
  ax.set_title(title_str)
  ax.xaxis.tick_bottom()
  return

def read_resonance_header(filepath):
  section = get_section(filepath, 2, 151)
  NER = int(section[1].split()[4])      # how many resonance energy ranges
  EL = ffloat(section[2].split()[0])    # range low energy bound (eV)
  EH = ffloat(section[2].split()[1])    # range high energy bound (eV)
  LRU = int(section[2].split()[2])      # RRR=1, URR=2
  LRF = int(section[2].split()[3])      # which formalism? 1=SLBW, 2=MLBW, 3=RM, 4=AA, 7=RMAT
  NLS = int(section[3].split()[4])      # how many l-values included
  SPI = ffloat(section[3].split()[0])   # spin of target nucleus
  return NER, EL, EH, LRU, LRF, NLS, SPI

def ffloat(string):
  if '+' in string:
    return np.float(string.replace('+', 'E+'))
  elif string.count('-') == 2:
    return np.float('-' + string[1:].replace('-', 'E-'))
  else:
    return np.float(string.replace('-', 'E-'))

def make_mat(rows=101, cols=157, dtype=np.int, fill=0):
  mat = np.empty((rows, cols), dtype=dtype)
  mat.fill(fill)
  return mat

def smallest_non_tiny(arr):
  for val in np.sort(arr.flatten()):
    if val > tiny:
      return val

def min_non_zero(matrix):
  temp_min = 1E18
  for row in matrix:
    for val in row:
      if 0 < val < temp_min:
        temp_min = val
  return temp_min

def get_z_n(za_str):
  za = ffloat(za_str)
  z = int(np.floor(za / 1000))
  a = za - (z * 1000)
  n = int(a - z)
  return z, n

def get_section(filepath, mf, mt):
  with open(filepath, 'r') as f:
    lines = f.readlines()
  for line_num, line in enumerate(lines):
    if (mf == get_col(line[71])) and (mt == get_col(line[72:75])):
      start_num = line_num
      break
  for line_num, line in enumerate(lines[start_num:]):
    if (mf != get_col(line[71])) or (mt != get_col(line[72:75])):
      end_num = line_num + start_num
      break
  return lines[start_num:end_num]

def get_col(chars):
  if type(chars) == str:
    return int(chars)
  elif type(chars) == list:
    return int(''.join(chars))
  else:
    exit("Didn't understand MF or MT format.")

def matshow_nuclide_coords(x, y):
  x = int(x+0.5)
  y = int(y+0.5)
  return 'Z={:d}, N={:d}, A={:d}'.format(y, x, x+y)

def get_fig_ax():
  fig = plt.figure(figsize=(16,12))
  ax = fig.add_subplot(111)
  ax.xaxis.set_major_locator(ticker.MultipleLocator(10))
  ax.xaxis.set_minor_locator(ticker.MultipleLocator(2))
  ax.yaxis.set_major_locator(ticker.MultipleLocator(10))
  ax.yaxis.set_minor_locator(ticker.MultipleLocator(2))
  ax.set_xlabel("N")
  ax.set_ylabel("Z")
  ax.format_coord = matshow_nuclide_coords
  ax.grid(which='both')
  return fig, ax

if __name__ == "__main__":
  main()
