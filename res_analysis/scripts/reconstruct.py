#!/usr/bin/env python3

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from scipy.integrate import quad

#matplotlib.rcParams.update({'font.size': 18})

def main():
  RT = np.load('../data/res_compact.npy')
  RT = RT.astype(np.float64)
  RT[np.isnan(RT)] = 0
  RT[np.isinf(RT)] = 0
  elem = get_symbols('../../pt-data1.csv')

# # plot iron-56
# nrg_arr, xs, RIGs, ERs, Ds = reconstruct(26, 56, RT, elem, 'g', 1E5, half_width=1000)
# plot_xs(nrg_arr, xs, 56, elem[26], 'g', RIGs, ERs, Ds)

  # plot gadolinium-156
  nrg_arr, xs, RIGs, ERs, Ds = reconstruct(64, 156, RT, elem, 'g', 1E5, half_width=1000)
  plot_xs(nrg_arr, xs, 156, elem[64], 'g', RIGs, ERs, Ds)

  # plot barium-1336
  nrg_arr, xs, RIGs, ERs, Ds = reconstruct(56, 135, RT, elem, 'g', 1E5, half_width=1000)
  plot_xs(nrg_arr, xs, 135, elem[56], 'g', RIGs, ERs, Ds)

# # plot tungsten-186
# nrg_arr, xs, RIGs, ERs, Ds = reconstruct(74, 186, RT, elem, 'g', 1E5, half_width=1000)
# plot_xs(nrg_arr, xs, 186, elem[74], 'g', RIGs, ERs, Ds)
#
# # plot uranium-238
# nrg_arr, xs, RIGs, ERs, Ds = reconstruct(92, 238, RT, elem, 'g', 1E5, half_width=1000)
# plot_xs(nrg_arr, xs, 238, elem[92], 'g', RIGs, ERs, Ds)

  plt.show()

def nearest(arr, val):
  return np.abs(arr-val).argmin()

def danger(RI, ER, GT, alpha=1, beta=1, gamma=1):
  return RI**alpha/(ER**beta * GT**gamma)

def reconstruct(Z, A, RT, elem, exit_channel='n', nrg_pts=1E4, half_width=50):
  N = A-Z
  RE = RT[np.where(RT[:,0]==Z)] # all resonances for element
  RN = RE[np.where(RE[:,1]==N)] # all resonances for nuclide/isotope
  nrg_arr = np.logspace(-2, 6, nrg_pts)
  xs = np.zeros_like(nrg_arr)
  if exit_channel == 'n':
    EC = 10
  elif exit_channel == 'g':
    EC = 12
  else:
    exit('Exit channel, {} not recognised.')
  Ds = []
  for R in RN:
    ER = R[4]
    GT = R[6]
    # calculate danger for resonance
    RIG = R[11]
    GG = R[12]
    D = danger(RIG, ER, GG, beta=2.5)
    Ds.append(D)
    start = nearest(nrg_arr, ER-half_width*GT)
    stop = nearest(nrg_arr, ER+half_width*GT)
    xs[start:stop] += [breit_wigner(E, R[5], R[10], R[EC], R[4], GT, 1) for E in nrg_arr[start:stop]]
  Ds = np.array(Ds)
  Ds /= Ds.max()
  return nrg_arr, xs, RN[:,11], RN[:,4], Ds

def breit_wigner(E, J, GN, GX, ER, GT, s2, s1=0.5):
  """http://www-pnp.physics.ox.ac.uk/~libby/Teaching/2005/C4-JL-lecture2.pdf
  E = energy, eV
  J = resonance spin
  GN = neutron partial width, eV
  GX = decay partial width, eV
  ER = resonance energy, eV
  GT = total width, eV,
  s1 = neutron spin
  s2 = target spin"""
  h = 4.135667E-15          # planck constant, eV s
  h_bar = h / (2*np.pi)     # reduced planck constant, eV s
  m_n = 1.674927E-27        # neutron mass, kg
  p_n = np.sqrt(2*E*m_n)    # neutron momentum, (eV kg)^0.5
  lambda_bar = h_bar / p_n  
  g = (2*J+1)/((2*s1+1)*(2*s2+1))
  return (np.pi*lambda_bar**2*g*GN*GX) / ((E-ER)**2 + (GT**2)/4)

def plot_xs(E, sigma, A, sym, exit_channel, RI, ER, Ds):
  if exit_channel == 'n':
    exit_channel += '\''
  fig, ax = plt.subplots(figsize=(9,7))
  ax.plot(E, sigma, color='b', label='Cross-section')
#  ax.legend()
  ax.set_title('$^{%d}$%s (n,%s)' % (A, sym, exit_channel))
  ax.set_yscale('log')
  ax.set_xscale('log')
  ax.grid()
  ax.set_xlim(1E-2, 1E6)
  ax.set_xlabel('E [eV]')
  ax.set_ylabel('$\sigma$(E)', color='b')
  rax = ax.twinx()
#  rax.set_ylabel('Resonance Integral [b $\cdot$ eV]', color='r')
#  rax.tick_params(axis='y', labelcolor='r')
#  rax.set_yscale('log')
#  rax.scatter(ER, RI, color='r', label='Resonance Integral')
  rax.scatter(ER, Ds, color='g', label='Danger')
  h1, l1 = ax.get_legend_handles_labels()
  h2, l2 = rax.get_legend_handles_labels()
  rax.legend(h1+h2, l1+l2, loc='best')
  ax.get_yaxis().set_ticklabels([])
  plt.tight_layout()

def get_symbols(filename):
  with open(filename) as f:
    PT = f.readlines()
  Z = []
  sym = []
  for e in PT:
    Z.append(int(e.split(',')[0]))
    sym.append(e.split(',')[1].strip())
  return dict(zip(Z, sym))

if __name__ == "__main__":
  main()
