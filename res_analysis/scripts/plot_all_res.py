#!/usr/bin/env python3

import numpy as np
import matplotlib
import matplotlib.ticker as ticker
import matplotlib.pyplot as plt
from scipy import stats
from mpl_toolkits.mplot3d import Axes3D
from reconstruct import danger

np.set_printoptions(linewidth=120)
matplotlib.rcParams.update({'font.size': 18})

def main():
  R = np.load('../data/res_sparse.npy')
  print(R.shape)
  # first GN & GG for each Z, N
  # return first resonance width for all Z and N (mostly zeros)
  e = R[:,:,0,2]
  fgn = R[:,:,0,8]
  rin = R[:,:,:,9]
  fgg = R[:,:,0,10]
  rig = R[:,:,:,11]
  # return indices of nonzero widths: tuple of arrays
  zn, nn = np.nonzero(fgn)
  zg, ng = np.nonzero(fgg)
  # access nonzero data with indices, equiv. to FGN = FGN[np.nonzero(FGN)]
  en = e[(zn, nn)]
  eg = e[(zg, ng)]
  # get non-zero widths
  fgn = fgn[(zn, nn)]
  fgg = fgg[(zg, ng)]
  # get non-zero resonances integrals
  rin = rin[(zn, nn)]
  rig = rig[(zg, ng)]

  # same again but for all widths, GN & GG for A
  # get all energies
  E = R[:,:,:,2]
  GT = R[:,:,:,6]
  GN = R[:,:,:,8]
  RIN = R[:,:,:,9]
  GG = R[:,:,:,10]
  RIG = R[:,:,:,11]
  ZN, NN, NPN = np.nonzero(GN)
  ZG, NG, NPG = np.nonzero(GG)
  # get energies for non-zero gamma widths
  EN = E[(ZN, NN, NPN)]
  EG = E[(ZG, NG, NPG)]
  # get non-zero widths
  GN = GN[(ZN, NN, NPN)]
  GG = GG[(ZG, NG, NPG)]
  # get non-zero resonance integrals
  RIN = RIN[(ZN, NN, NPN)]
  RIG = RIG[(ZG, NG, NPG)]

#  plot_RI_hist(RIG[np.isfinite(np.log10(RIG))], bins=50)
#  plot_resonance_width(ZN + NN, zn + nn, GN, R[:,:,:,10][(ZN, NN, NPN)], fgn, R[:,:,0,10][(zn,nn)]) # needs GN and GG to be same length (indexed with same Z & N)
#  plot_density(ZN + NN, EN, GN, zn + nn, en, fgn, points=10000, title_str='Scattering resonances in ENDF/B-VII.1')
#  plot_density(ZG + NG, EG, GG, zg + ng, eg, fgg, points=10000, title_str='Radiative capture resonances in ENDF/B-VII.1')
  
  plt.show()


def get_ticks(low, high):
  return np.logspace(low, high, high-low+1)

def pretty_ticks(arr):
  return ["$10^{%d}$" % np.floor(np.log10(e)) for e in arr]

def plot_density(x, y, z, x1, y1, z1, points=1000, title_str='', RI=None):
  yticks = get_ticks(-1, 7)
  zticks = get_ticks(-7, 6)
  y = np.log10(y)
  z = np.log10(z)
  y1 = np.log10(y1)
  z1 = np.log10(z1)
  xyz = np.vstack([x, y, z])
  kde = stats.gaussian_kde(xyz)
  density = kde(xyz)
  idx = x.argsort()
  idx = np.random.choice(idx, size=points, replace=False)
  x, y, z = x[idx], y[idx], z[idx]
  density = density[idx]
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')
  if RI is not None:
    I, i = RI
    I[I<=0] = 1
    I = np.log10(I)**2
    ax.scatter(x, y, z, c=density, s=I)
    i[i<=0] = 1
    i = np.log10(i)**2
    ax.scatter(x1, y1, z1, c='black', s=i)
  else:
    ax.scatter(x, y, z, c=density, s=20)
    ax.scatter(x1, y1, z1, c='pink', s=20)
  ax.set_xlabel('Atomic mass, A [amu]', labelpad=20)
  ax.set_ylabel('Resonance energy, $E_{r}$ [eV]', labelpad=20)
  ax.set_zlabel('Resonance width, $\Gamma$ [eV]', labelpad=20)
  ax.set_xlim(0, 255)
  ax.set_ylim(-1, 7)
  ax.set_zlim(-7, 6)
  ax.set_yticks(np.log10(yticks))
  ax.set_yticklabels(pretty_ticks(yticks))
  ax.set_zticks(np.log10(zticks))
  ax.set_zticklabels(pretty_ticks(zticks))
  ax.set_title(title_str)

def plot_hist(x, bins=50, **kwargs):
  x = np.log10(x)
#  xmin_b10 = np.floor(x.min())
  xmin_b10 = -20
  xmax_b10 = np.ceil(x.max())
  x_grid = np.linspace(xmin_b10, xmax_b10, 1000)

  fig, ax = plt.subplots(1)
  cmap = (plt.get_cmap('jet')(1.*i/bw_c) for i in range(bw_c))
  n, bins, patches = ax.hist(10**x, bins=np.logspace(xmin_b10, xmax_b10, bins), color='grey', alpha=0.2, **kwargs)
  kde = stats.gaussian_kde(x, bw_method=0.2)
  density = kde.pdf(x_grid)
  density *= n.max() / density.max()
  ax.plot(10**x_grid, density, color='r', lw=3)
  ax.scatter(10**x, np.zeros(x.shape) + 0.01 * n.max(), color='r', marker="|", zorder=3, lw=1)
  ax.set_ylim(0, 1.1 * n.max())
  pad = 0.1 * x.max()
  ax.set_xscale('log')
  ax.set_xlim(10**xmin_b10, 10**xmax_b10)
  ax.set_xlabel('Z')
  ax.set_ylabel('Frequency')
  ax.set_title('Distribution of resonance integrals in ENDF/B-VII.1')
  return

def plot_RI_hist(x, bins=50, **kwargs):
  x = np.log10(x)
#  xmin_b10 = np.floor(x.min())
  xmin_b10 = -20
  xmax_b10 = np.ceil(x.max())
  x_grid = np.linspace(xmin_b10, xmax_b10, 1000)
  bw_c = 4
  bandwidths = np.logspace(-1, 0, bw_c)

  fig, ax = plt.subplots(1)
  cmap = (plt.get_cmap('jet')(1.*i/bw_c) for i in range(bw_c))
  n, bins, patches = ax.hist(10**x, bins=np.logspace(xmin_b10, xmax_b10, bins), color='grey', alpha=0.2, **kwargs)
  for bw, c in zip(bandwidths, cmap):
    kde = stats.gaussian_kde(x, bw_method=bw)
    density = kde.pdf(x_grid)
    density *= n.max() / density.max()
    ax.plot(10**x_grid, density, color=c, lw=3, label="{:.2f}".format(bw))
  ax.scatter(10**x, np.zeros(x.shape) + 0.01 * n.max(), color='r', marker="|", zorder=3, lw=1)
  ax.set_ylim(0, 1.1 * n.max())
  ax.legend(title='KDE bandwidth')
  pad = 0.1 * x.max()
  ax.set_xscale('log')
  ax.set_xlim(10**xmin_b10, 10**xmax_b10)
  ax.set_xlabel('Resonance Integral, RI [b $\cdot$ eV]')
  ax.set_ylabel('Frequency')
  ax.set_title('Distribution of resonance integrals in ENDF/B-VII.1')
  return

def plot_resonance_width(A, a, GN, GG, fgn, fgg):
  print('A', len(A))
  print('GN', len(GN))
  print('GG', len(GG))
  print('a', len(a))
  print('fgn', len(fgn))
  print('fgg', len(fgg))
  f, (ax_N, ax_G) = plt.subplots(2, 1, sharex=True)
#  ymin = 0.1 * min([GN.min(), GG.max()])
  ymax = 10 * max([GN.max(), GG.max()])
  ymin = 1E-10
  print(ymin, ymax)
  for ax in [ax_N, ax_G]:
    ax.set_yscale('log')
    ax.set_ylim(ymin, ymax)
    ax.set_ylabel('$\Gamma_{x}(A)$ [eV]')
    ax.xaxis.set_major_locator(ticker.MultipleLocator(10))
    ax.xaxis.set_minor_locator(ticker.MultipleLocator(2))
    ax.grid()
  ax_N.scatter(A, GN, color='crimson', s=10, label='n', alpha=0.4)
  ax_G.scatter(A, GG, color='lightgreen', s=10, label='$\gamma$', alpha=0.4)
  ax_N.scatter(a, fgn, color='darkred', s=100, label='n for first res.', alpha=0.4)
  ax_G.scatter(a, fgg, color='green', s=100, label='$\gamma$ for first res.', alpha=0.4)
  f.suptitle('Resonance widths for ENDF/B-VII.1', fontsize=22)
  h1, l1 = ax_N.get_legend_handles_labels()
  h2, l2 = ax_G.get_legend_handles_labels()
  ax_G.set_xlabel('Atomic mass, A [amu]', labelpad=14)
  ax_N.legend(h1+h2, l1+l2, title='Resonance subset, $x$', fontsize=14)

if __name__ == "__main__":
  main()
