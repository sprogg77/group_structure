#!/usr/bin/env python3

import numpy as np

data = np.genfromtxt('../data/imp_res')
print(data.view('i8,i8,i8').sort(order=['f2'], axis=0))
print(data[-30:-1,:])
