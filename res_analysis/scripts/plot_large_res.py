#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
import mpld3

def main():
  data = np.genfromtxt('../data/imp_res')
  Z = data[:,0]
  A = data[:,1]
  EN = data[:,2]
  GN = data[:,3]
  RIN = data[:,4]
  EG = data[:,5]
  GG = data[:,6]
  RIG = data[:,7]

  #compare_density_estimates_logspace(np.concatenate((EN, EG)), hist_bins=20)
  plot_resonance_energy(Z, A, EN, EG)
  plot_resonance_width(Z, A, GN, GG)
  plt.show()

def compare_density_estimates_linspace(x):
  x_grid = np.linspace(x.min(), x.max(), 1000)
  bw_c = 7
  bandwidths = np.logspace(-1, 0, bw_c)

  fig, ax = plt.subplots(1)
  cmap = (plt.get_cmap('jet')(1.*i/bw_c) for i in range(bw_c))
  n, bins, patches = ax.hist(x, bins=20, color='grey', alpha=0.2)
  for bw, c in zip(bandwidths, cmap):
    kde = gaussian_kde(x, bw_method=bw)
    density = kde.pdf(x_grid)
    density *= n.max() / density.max()
    ax.plot(x_grid, density, color=c, lw=3, label="{:.2f}".format(bw))
  ax.scatter(x, np.zeros(x.shape) + 0.01 * n.max(), color='b', marker="|", zorder=3, lw=3)
  ax.set_ylim(0, 1.1 * n.max())
  ax.legend()
  pad = 0.1 * x.max()
  ax.set_xlim(x.min() - pad, x.max() + pad)
  ax.set_title('Determining PDF of linspace discrete data: varying kernel width')
  return

def compare_density_estimates_logspace(x, hist_bins=50):
  x = np.log10(x)
  xmin_b10 = np.floor(x.min())
  xmax_b10 = np.ceil(x.max())
  x_grid = np.linspace(xmin_b10, xmax_b10, 1000)
  bw_c = 7
  bandwidths = np.logspace(-1, 0, bw_c)

  fig, ax = plt.subplots(1)
  cmap = (plt.get_cmap('jet')(1.*i/bw_c) for i in range(bw_c))
  n, bins, patches = ax.hist(10**x, bins=np.logspace(xmin_b10, xmax_b10, hist_bins), color='grey', alpha=0.2)
  for bw, c in zip(bandwidths, cmap):
    kde = gaussian_kde(x, bw_method=bw)
    density = kde.pdf(x_grid)
    density *= n.max() / density.max()
    ax.plot(10**x_grid, density, color=c, lw=3, label="{:.2f}".format(bw))
  ax.scatter(10**x, np.zeros(x.shape) + 0.01 * n.max(), color='r', marker="|", zorder=3, lw=3)
  ax.set_ylim(0, 1.1 * n.max())
  ax.legend(title='KDE bandwidth')
  pad = 0.1 * x.max()
  ax.set_xscale('log')
  ax.set_xlim(10**xmin_b10, 10**xmax_b10)
  ax.set_xlabel('Energy (eV)')
  ax.set_ylabel('Frequency')
  ax.set_title('Density estimation of nuclide-wise largest resonances')
  return

def plot_resonance_energy(Z, A, EN, EG):
  # inner function has access to outer function scope
  def onpick(event):
    i = event.ind
    axis = event.inaxes
    print(axis)
    if event.artist.axes == ax_N:
      if len(i) == 1:
        print('[%d,%d] = %1.3E eV\n' % (int(Z[i]), int(A[i]), EN[i]))
      else:
        for n in i:
          print('[%d,%d] = %1.3E eV' % (int(Z[n]), int(A[n]), EN[n]))
        print('\n')
    elif event.artist.axes == ax_G:
      if len(i) == 1:
        print('[%d,%d] = %1.3E eV\n' % (int(Z[i]), int(A[i]), EG[i]))
      else:
        for n in i:
          print('[%d,%d] = %1.3E eV' % (int(Z[n]), int(A[n]), EG[n]))
        print('\n')
    else:
      print('Event artist: {} not recognised.'.format(event.artist))

  fig, ax_N = plt.subplots(1)
  ax_N.scatter(A, EN, color='b', picker=True, label='(n,n\')')
  ax_N.set_ylim(1E-2, 1E8)
  ax_N.set_yscale('log')
  ax_N.set_ylabel('Energy [eV]')
  ax_N.set_xlabel('Atomic mass, A')
  ax_N.set_title('Largest nuclide-wise resonances for ENDF/B-VII.1')
  ax_N.grid()
  ax_G = ax_N.twinx()
  ax_G.scatter(A, EG, color='g', picker=True, label='(n,g)')
  ax_G.set_yscale('log')
  ax_G.set_ylim(1E-2, 1E8)
  h1, l1 = ax_N.get_legend_handles_labels()
  h2, l2 = ax_G.get_legend_handles_labels()
  ax_N.legend(h1+h2, l1+l2)
  fig.canvas.mpl_connect('pick_event', onpick)

def plot_resonance_width(Z, A, GN, GG):
  width_fig, ax_N = plt.subplots(1)
  ax_N.scatter(A, GN, color='b', picker=True, label='(n,n\')')
  ax_N.set_yscale('log')
  ymax= 10* max([GN.max(), GG.max()])
  ymin= min([GN.min(), GG.max()])*0.1
  print(ymin, ymax)
  ax_N.set_ylim(ymin, ymax)
  ax_N.set_ylabel('$\Gamma_{x}(A)$')
  ax_N.set_xlabel('Atomic mass, A')
  ax_N.set_title('Largest nuclide-wise resonances for ENDF/B-VII.1')
  ax_N.grid()
  ax_G = ax_N.twinx()
  ax_G.scatter(A, GG, color='g', picker=True, label='(n,g)')
  ax_G.set_yscale('log')
  ax_G.set_ylim(ymin, ymax)
  h1, l1 = ax_N.get_legend_handles_labels()
  h2, l2 = ax_G.get_legend_handles_labels()
  ax_N.legend(h1+h2, l1+l2)

if __name__ == "__main__":
  main()
