#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import KernelDensity

def main():
  R = np.load('../data/res_compact.npy')
  R = R.astype(np.float64)
  R[np.isnan(R)] = 0
  R[np.isinf(R)] = 0
  print(R.shape)

  # normalise resonance dangers, nuclide.max() = 1
  E, D = nuc_norm_danger(R, beta=1)
  plot_danger(E, D)

  plt.show()

def danger(RI, ER, GT, alpha=1, beta=1, gamma=1):
  # calculates resonance danger on integral, energy, width
  return RI**alpha/(ER**beta * GT**gamma)

def danger_i(RI, ER, GT, i=0, alpha=1, beta=1, gamma=1):
  # calculates resonance danger on integral, energy, width and order of resonance
  if i>10:
    i=10
  return RI**alpha/(ER**beta * 2**i * GT**gamma)

def nuc_norm_danger(RT, beta, exit_channel='g'):
  """Takes resonance table, unfiltered. Returns list of nuclide-wise dangers
  each normalised to the maximum nuclide danger. Also returns list of energies."""
  if exit_channel == 'n':
    ec = 10
  elif exit_channel == 'g':
    ec = 12
  else:
    exit('Exit channel, {} not recognised.')
  # lists for all dangers and all energies
  DA = []
  EA = []
  max_z = RT[:,0].max()
  max_n = RT[:,1].max()
  # loop over all nuclides
  for z in np.linspace(1,max_z,max_z):
    for n in np.linspace(1,max_n,max_n):
      RE = RT[np.where(RT[:,0]==z)] # all resonances for element
      RN = RE[np.where(RE[:,1]==n)] # all resonances for nuclide/isotope
      if RN.any():
        # resonance lists
        DN = []
        EN = []
        # loop over resonances for each nuclide
        for i, R in enumerate(RN):
          er = R[4]
          gt = R[6]
          ri = R[ec-1]
          gc = R[ec]
          # calculate danger for resonance
          d = danger(ri, er, gc, beta=beta)
          EN.append(er)
          DN.append(d)
        # list -> numpy array
        EN = np.array(EN)
        DN = np.array(DN)
        # normalise dangers by largest for each nuclide
        DN /= DN.max()
        # append nuclide results to 'all results' lists
        EA.append(EN)
        DA.append(DN)
  return EA, DA

def get_kde(E, bw=0.2):
  E = E[:, np.newaxis]
  E_kde = np.logspace(-2,8,1000)[:, np.newaxis]
  log_dens = KernelDensity(kernel='gaussian', bandwidth=bw).fit(E).score_samples(E_kde)
  return np.exp(log_dens), E_kde

def plot_danger(E_LoA, D_LoA):
  # LoA stands for List of Arrays
  fig, ax = plt.subplots()
  D = np.empty(shape=(0))
  E = np.empty(shape=(0))
  for i, dangers in enumerate(D_LoA):
#    ax.scatter(E_LoA[i], D_LoA[i])
    # turn LoA into 1D array by concatenation
    D = np.concatenate((D,D_LoA[i]))
    E = np.concatenate((E,E_LoA[i]))
  # indexes of dangerous resonances
  I_imp = np.where(np.abs(D-1.0)<=1E-3)
  # energies of dangerous resonances
  E_imp = E[I_imp]
  # dangerous resonances
  D_imp = D[I_imp]
  kde_imp, E_grid = get_kde(E_imp, bw=10)
  kde_all, E_grid = get_kde(E, bw=10)
#  ax.plot(E_grid, kde_imp, label='Max danger, nuclide-wise KDE')
  ax.hist(E_imp, bins=np.logspace(-1,8,40))
  #ax.plot(E_grid, kde_all, label='All resonances included')
  ax.set_title('Most dangerous resonance for each nuclide')
  ax.set_xlabel('Energy [eV]')
  ax.set_ylabel('Frequency')
  ax.set_xscale('log')
  #ax.set_yscale('log')
  ax.set_xlim(5E-2, 1E7)
  max_y = kde_imp.max()
#  ax.set_ylim(max_y/1E3, max_y*1E3)
  ax.grid()
  ax.legend()
  return

if __name__ == "__main__":
  main()
