class Nuclide(object):
  def __init__(self, name, mat_num, rr_start, rr_end):
    self.name = name
    self.element = ''.join(list(filter(lambda x: x.isalpha(), self.name)))
    self.mass = int(''.join(list(filter(lambda x: x.isnumeric(), self.name))))
    self.mat_num = mat_num
    self.rr_start = rr_start
    self.rr_end = rr_end
