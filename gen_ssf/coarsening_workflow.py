#!/usr/bin/env python3

import utility
import gen_SSF
import read_output
import gather_nuclide_set
from nuclide import Nuclide
import gen_MG
import gen_PT
import os
import scipy
import uuid
from tqdm import tqdm
import numpy as np
from fill_between_steps import fill_between_steps
import matplotlib.pyplot as plt
import mimic_alpha as ma
from subprocess import call
from datetime import datetime
from collections import OrderedDict
from sys import argv

def main():
  """This script generates hyperfine nuclear data and decreases the bincount gradually by combining 
  unimportant bins. Importance is decided by the SSF(E) value, where close to 1 is unimportant.
  """

  # hardcoded inputs
  starting_nbins = 500
  iteration_limit = 5
  divisor = 10
#  elements_list = ['W']
  elements_list = ['Fe', 'W', 'Mo', 'Nd', 'Sn', 'Zr', 'Cu', 'Co', 'Ta']
  bpd_min = 10
  exponent = 0
  temp = 294
  E_min = 1E-5
  E_max = 1E8
  uid = 'testing0'
  ebins_filepath = "/Users/Fred/projects/SSF_study/ebins/ebins_{:d}_{:s}".format(int(starting_nbins), uid)
  # make the hyperfine group structure
  bins = np.logspace(np.log10(E_min), np.log10(E_max), starting_nbins + 1)
  if not os.path.exists(ebins_filepath):
    utility.make_ebins_file(bins, ebins_filepath)
  bins_list = []
  danger_pdf_list = []
  danger_integral_list = []
  threshold_list = []
  nbins = starting_nbins

  # main loop - as a general rule, try removing half the bins, recover those that stick the danger up
  i = 0
  while i < iteration_limit:
    print("****************************")
    print("        Iteration {}        ".format(i))
    print("****************************")
    print("Using {:s} group structure".format(ebins_filepath))
    # input ebins filepath and output bins are the same OLD bins
    # generate nuclear data, then compute SSF(E) by FISPACT collapse
    bins, archive_path, uid = run(exponent, temp, ebins_filepath, elements_list) 

    # read in data, pickle to database, plot nuclide-wise, return SSF(E)
    read_bins, danger_pdf = analyse(archive_path, exponent, ebins_filepath, uid)
    danger_integral = integrate(read_bins, danger_pdf)
    threshold = danger_integral / divisor

    # once danger integral starts to increase, terminate loop and report
    if i != 0:
      if danger_integral > danger_integral_list[-1]:
        print("Reached danger integral minimum.")
        break

    # store for plotting on completion
    bins_list.append(read_bins)
    danger_pdf_list.append(danger_pdf)
    danger_integral_list.append(danger_integral)
    threshold_list.append(threshold)

    # returns NEW group structure, reassigning bins to the new, coarser group
    print("Coarsening group structure by removing superfluous bins...")
    nbins = len(bins) - 1
    print("Starting with {:d} bins...".format(nbins))
    print("Using threshold of: {:1.3f}...".format(threshold))

    # make new group structure
    new_bins = [E_max]
    print("Max width of bin: ".format(1/bpd_min))
    for j, _ in enumerate(danger_pdf[1:-1:2]): # ignore highest energy bin, advance in steps of 2, ignore lowest bin
      j *= 2 # make counter match real indices (despite having stepped)
      if (danger_pdf[j] < threshold) and (danger_pdf[j+1] < threshold): # remove dividing bound if below threshold 
        width_after_removal = np.log10(new_bins[-1]) - np.log10(bins[j+2])
        if width_after_removal < 1/bpd_min: # check if bin won't be too wide, shouldn't be greater than 1/bpd
          print(danger_pdf[j], new_bins[-1], width_after_removal, bins[j+1], "remove", 1/bpd_min)
        else: # removing dividing bound would make new bin too wide, so don't remove it
          print(danger_pdf[j], new_bins[-1], width_after_removal, bins[j+1], "keep", 1/bpd_min)
          new_bins.append(bins[j])
      if (danger_pdf[j] > threshold): # danger is too high of 1st bin in pair, split it
        new_bins.append(bins[j])
        new_bins.append(bins[j+1])
        half_bin_width = (np.log10(bins[j]) - np.log10(bins[j+1])) / 2 # logged half bin width
        dividing_bound = 10**(np.log10(bins[j+1]) + half_bin_width) 
        new_bins.append(dividing_bound)
        width_after_addition = np.log10(bins[j]) - np.log10(dividing_bound) 
        print(danger_pdf[j], bins[j], width_after_addition, dividing_bound, "divide")
      if (danger_pdf[j+1] > threshold): # danger is too high of 2nd bin in pair, split it
        new_bins.append(bins[j+1])
        half_bin_width = (np.log10(bins[j+1]) - np.log10(bins[j+2])) / 2 # logged half bin width
        dividing_bound = 10**(np.log10(bins[j+2]) + half_bin_width) 
        new_bins.append(dividing_bound)
        width_after_addition = np.log10(bins[j+1]) - np.log10(dividing_bound) 
        print(danger_pdf[j], dividing_bound, width_after_addition, bins[j+2], "divide")
    new_bins.append(bins[-1]) # won't reach the last bound, so add it manually
    new_bins = sorted(list(set(new_bins)))
    bins = new_bins[:] # make a copy, because i'm not sure how python works

    if len(bins) - 1 == nbins:
      print("No more bins can be removed.")
      break
    nbins = int(len(bins)) - 1
    if nbins == 1:
      print("Error, coarsened to 1 group...")
      break
    ebins_filepath = "/Users/Fred/projects/SSF_study/ebins/ebins_{:d}_{:s}".format(nbins, uid)
    print("Reduced to {:d} bins, writing to file: {:s}".format(nbins, ebins_filepath))
    utility.make_ebins_file(bins, ebins_filepath) # record these new bins for next iteration
    i += 1
  print("Execution completed.")

  plot_danger_evolution(bins_list, danger_pdf_list, threshold_list)
  plot_bin_evolution(bins_list, danger_integral_list, threshold_list)
#  plot_bin_widths(bins_list)
#  plot_cumulative_bins(bins_list)

  plt.show()
  return

def plot_bin_evolution(bins, dangers, thresholds):
  f,ax = plt.subplots()
  indices = range(len(bins))
  lengths = [len(group) for group in bins]
  ax.scatter(indices, lengths, label="Bin count")
  ax.scatter(indices, dangers, label=r"$\int D(E) dE$")
  ax.scatter(indices, thresholds, label="Threshold")
  ax.set_yscale('log')
  bin_lengths = [len(group) for group in bins]
  ax.set_ylim(min(thresholds) / 10, max(bin_lengths) * 10)
  ax.legend()   
  ax.grid()
  ax.set_title('Convergence (or otherwise) plot')
  return

def plot_bin_widths(bins_list):
  f,ax = plt.subplots()
  for energies in bins_list:
    energies = energies[::-1]
    bin_widths = get_bin_widths(energies)
    ax.scatter(energies, 10**bin_widths, label="{:d}".format(len(energies)))
  ax.set_xscale('log')
  ax.set_yscale('log')
  ax.set_xlim(1E-6, 1E9)
  ax.set_xlabel('Energy')
#  ax.set_ylabel('$\mathrm{log}_{10}(\mathrm{Bin\ width})$')
  ax.set_ylabel('Bin width')
  ax.set_title('Group structure comparison')
  ax.legend()
  return

def plot_cumulative_bins(bins_list):
  f,ax = plt.subplots()
  for energies in bins_list:
    bin_integers = list(range(len(energies)))
    ax.step(energies[::-1], bin_integers, label="{:d}".format(len(energies)))
  ax.set_xscale('log')
  ax.set_xlabel('Energy')
  ax.set_ylabel('Cumulative bin count')
  ax.set_title('Group structure comparison')
  ax.legend()
  return

def get_bin_widths(E):
  E_roll = np.roll(E, 1)
  bin_widths = np.abs(np.log10(E) - np.log10(E_roll))
  bin_widths[0] = 0
  return bin_widths

def integrate(E, pdf):
  bin_widths = get_bin_widths(E)
  integral_vector = bin_widths * pdf
  return integral_vector.sum()

def plot_danger_evolution(bins, danger, thresholds):
  f,axarr=plt.subplots(len(bins),sharex=True,figsize=(10,20))
  y_max = max([max(d) for d in danger])
  for i, ax in enumerate(axarr):
    E = bins[i]
    pdf = danger[i]
    integral = integrate(E, pdf)
    ax.step(E, pdf, color='red', where='post', linewidth=0.5)
    ax.axhline(thresholds[i], ls='--', c='k')
    light_blue = ma.colorAlpha_to_rgb((0,0,1),0.4)[0]
    light_red = ma.colorAlpha_to_rgb((1,0,0),0.4)[0]
    fill_between_steps(ax, E, pdf, 0, step_where='post', color=light_red)
    ax.text(0.035, 0.8, r'%d bin energy integrated effective self-shielding = %1.5f' % (int(len(E)-1), integral),
            verticalalignment='bottom', horizontalalignment='left',
            transform=ax.transAxes, fontsize=10)
    ax.set_xlim(1E-2,1E5)
    ax.set_ylim(1E-5, 2 * y_max)
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.grid()
  mid_val = int(np.ceil(i/2))
  axarr[mid_val].set_ylabel('Effective self-shielding distribution')
  axarr[-1].set_xlabel('Energy [eV]')
  f.savefig('ssf_pdf_evolution.eps')
  f.savefig('ssf_pdf_evolution.pdf')
  return

def plot_ssf_pdf(E, pdf, run_path, exponent, nbins, uid):
  E_roll = np.roll(E, 1)
  bin_widths = np.log10(E_roll) - np.log10(E)
  bin_widths[0] = 0
  integral_vector = bin_widths * pdf
  integral = integral_vector.sum()
  
  fig = plt.figure(figsize=(7,5))
  ax = fig.add_subplot(111)
  ax.step(E, pdf, color='red', where='post', linewidth=0.5)
  light_blue = ma.colorAlpha_to_rgb((0,0,1),0.4)[0]
  light_red = ma.colorAlpha_to_rgb((1,0,0),0.4)[0]
  fill_between_steps(ax, E, pdf, 0, step_where='post', color=light_red)
  ax.text(0.035, 0.92, r'Energy integrated effective self-shielding = %1.2f' % integral,
          verticalalignment='bottom', horizontalalignment='left',
          transform=ax.transAxes, fontsize=10)
#  time_str = str(datetime.now())[:-3]
#  ax.text(0.69, 0.93, time_str, transform=ax.transAxes, fontsize=8)
  ax.set_title(r'Effective self-shielding with %d bins' % (nbins))
  ax.set_xlabel('Energy [eV]')
  ax.set_ylabel('Effective self-shielding distribution')
  ax.set_xlim(1E-2,1E5)
  ax.set_ylim(0,1.1 * pdf.max())
  ax.grid()
  ax.set_xscale('log')
  #ax.set_yscale('log')
  fig.savefig(os.path.join(run_path, 'ssf_pdf_hist.eps'))
  fig.savefig(os.path.join(run_path, 'ssf_pdf_hist.png'), transparent=True)
  plt.close()
  return

def plot_bins(bins, ssfs, coarse_bins, plot_path, threshold):
  f,axarr = plt.subplots(2,figsize=(11,7),sharex=True,gridspec_kw = {'height_ratios':[2, 1]})
  axarr[0].step(bins,ssfs,label="Cumulative SSF score", linewidth=0.2)
  axarr[0].axhline(threshold, label="Bin rejection threshold: SSF > {:1.3f}".format(threshold), ls='--', c='k')
  bin_lines = [([E, E], [0, 1]) for E in coarse_bins]
  pale_green = ma.colorAlpha_to_rgb('g', 0.4)[0]
  for i, line in enumerate(bin_lines):
    if i != 0:
      axarr[1].plot(line[0], line[1], color=pale_green, linewidth=0.2)
    else:
      axarr[1].plot(line[0], line[1], color=pale_green, linewidth=0.2, label="Retained bin bounds")
  energy_grid = np.linspace(np.log10(bins.min()), np.log10(bins.max()), 1E3)
  bw = 0.05
  gkde = scipy.stats.gaussian_kde(np.log10(coarse_bins), bw_method=bw)
  kdepdf = gkde.evaluate(energy_grid)
  kdepdf /= kdepdf.max() # normalise so max y-value is 1, same as bin_lines
  axarr[1].plot(10**energy_grid, kdepdf, color='r', label="Gaussian KDE of bins, bandwidth = {:1.3f}".format(bw))
  axarr[0].set_ylabel(r"$1-\sum_{n=1}^{N}(1 - \mathrm{SSF}_{n}\mathrm{(E)})$ for nuclides n={1, 2, ..., N}")
  for ax in axarr:
    ax.set_xscale('log')
    ax.set_xlim(1, 1E6)
  axarr[0].set_title("Reducing {:d} hyperfine groups to {:d} coarse groups".format(len(bins)-1,len(coarse_bins)-1))
  axarr[1].set_xlabel("Energy (eV)")
  axarr[1].set_yticks([])
  h1, l1 = axarr[0].get_legend_handles_labels()
  h2, l2 = axarr[1].get_legend_handles_labels()
  axarr[0].legend(h1+h2, l1+l2, fontsize=10)
  axarr[0].grid()
  f.savefig(plot_path)
  return

def analyse(run_path, exponent, ebins_path, uid):
  print('Assembling/reading output database and plotting results...')
  figure_path = os.path.join(run_path, 'figure')
  if os.path.exists(figure_path):
    pass
  else:
    os.mkdir(figure_path)
  output_path = os.path.join(run_path, 'output')
  ssf_data_filename = os.path.join(run_path, 'ssf_data.pkl')
  symbol_map_filename = 'elements.csv'
  E = utility.get_energies(ebins_path)
  nbins = int(len(E) - 1)
  sym_dict = read_output.get_sym_dict(symbol_map_filename)
  ssf_dict = {}
  ssf_abs_dict = {}
  
  # spectrum is simple linear function of energy in log-log space
  phi = E**exponent

  # get database of SSFs
  if os.path.exists(ssf_data_filename):
    X = read_output.load_obj(ssf_data_filename)
  else:
    X = read_output.build_database(E, phi, output_path)
    read_output.save_obj(X, ssf_data_filename)

  # initialise arrays for key metrics
  # find PDF of SSF effect for all nuclides
  pdf_gamma_all = np.zeros_like(E)

  # for each nuclide, retreive SSFs, perform integration for SSF bar and plot results
  for nuc in tqdm(X.keys()):
    E_res, rr_tot, rr_tot_sh, ssf, ssf_bar, g0, gn = read_output.fold_total(X, E, phi, nuc)
    E_res_abs, rr_abs, rr_abs_sh, ssf_abs, ssf_abs_bar, g0_abs, gn_abs = read_output.fold_abs(X, E, phi, nuc)
    pdf_tot = read_output.plot_nuclide_ssf(nuc, E_res, rr_tot, rr_tot_sh, ssf, ssf_bar, figure_path, exponent, 'tot', nbins)
    pdf_gamma = read_output.plot_nuclide_ssf(nuc, E_res_abs, rr_abs, rr_abs_sh, ssf_abs, ssf_abs_bar, figure_path, exponent, '\gamma', nbins)
    pdf_gamma_all[g0_abs:gn_abs] += pdf_gamma
    ssf_dict[nuc] = ssf_bar
    ssf_abs_dict[nuc] = ssf_abs_bar

  read_output.plot_ssf_pdf(E, pdf_gamma_all, run_path, exponent, nbins, uid)
  with open(os.path.join(run_path, 'ssf_pdf.out'), 'w') as f:
    for i, value in enumerate(E):
      f.write("{} {}\n".format(E[i], pdf_gamma_all[i]))

  # capture SSF bar for all nuclides, print output
  ssf_dict = OrderedDict(sorted(ssf_dict.items(), key=lambda t: t[1], reverse=True))
  ssf_list = []
  ssf_abs_dict = OrderedDict(sorted(ssf_abs_dict.items(), key=lambda t: t[1], reverse=True))
  ssf_abs_list = []
  with open(os.path.join(run_path, 'ssf_summary.out'), 'w') as f:
    for nuc in ssf_abs_dict.keys():
      ssf_bar = ssf_dict[nuc]
      ssf_abs_bar = ssf_abs_dict[nuc]
      if np.isfinite(ssf_bar):
        result_str = "{:8s} {:1.4f} {:1.4f}\n".format(nuc, ssf_bar, ssf_abs_bar)
        ssf_list.append(ssf_dict[nuc])
        ssf_abs_list.append(ssf_abs_dict[nuc])
        f.write(result_str)
  return E, pdf_gamma_all

def coarsen_group(old_bins, pdf_gamma_all, archive_path, old_uid, E_min=1E-5, E_max=1E8, new_uid=None, threshold=0.9):
  """Using SSF(E) information, cull bin boundaries separating SSF approx = 1 bins. Return the new group structure for analysis.
  """
  # argument checking, is energy ascending?
  for i, val in enumerate(old_bins[:-1]):
    if old_bins[i+1] > old_bins[i]:
      continue
    else:
      # reverse input arrays to ascending energy order
      old_bins = old_bins[::-1]
      pdf_gamma_all = pdf_gamma_all[::-1]
  if new_uid: # passed by argument
    pass
  else:
    # new group UID
    new_uid = str(uuid.uuid4()).split('-')[0] # generate new UID and corresponding path

  # coarsen the group structure
  # old_bins must be in ascending energy order
  # ssf[0] is from zero energy to old_bins[0], ignore"""
  new_bins = []
  new_bins.append(old_bins[0])
  new_bins.append(old_bins[-1])
  for i in range(0, len(old_bins) - 1):
    lower_bound = old_bins[i]
    upper_bound = old_bins[i + 1]
    if pdf_gamma_all[i + 1] < threshold:
      new_bins.append(lower_bound)
      new_bins.append(upper_bound)
  new_bins = np.array(sorted(list(set(new_bins))))

  nbins = len(new_bins) - 1
  new_ebins_filepath = '/Users/Fred/projects/SSF_study/ebins/ebins_{:d}_{:s}'.format(nbins, new_uid)
  plot_path = os.path.join(archive_path, 'coarsened_bounds_threshold_{:1.3f}.eps'.format(threshold))
  plot_bins(old_bins, pdf_gamma_all, new_bins, plot_path, threshold)
  return new_bins, new_ebins_filepath, plot_path

def run(exponent, temp, ebins_filepath, elements_list=None, fluxes=None):
  """With spectral information and a group structure, run FISPACT collapse for specified elements
  making nuclear data if necessary (PENDFs, GENDFs & PROBTABLES), then read output and store in
  .pkl database, then analyse and plot results."""
  print("Computing self-shielding factors...")
  # get initial bin bounds array
  ebins_filename = ebins_filepath.split('/')[-1]
  if len(ebins_filename.split('_')) != 3:
    exit('ebins filename, {:s} does not contain a unique ID'.format(ebins_filename))
  uid = ebins_filename.split('_')[2]
  nbins = int(ebins_filename.split('_')[1])
  bins = utility.get_energies(ebins_filepath)
  assert nbins == len(bins) - 1
  print('Using {} bins with group structure ID {}...'.format(nbins, uid))

  # directories and paths for SSF generation
  reduced_index_path = 'reduced_nuclide_index'
  fispact_files_filename = 'files.tendl15-n'
  ssf_data_dir = '/Users/Fred/projects/SSF_study/ssf_data/'

  # directories for nuclide list
  endfb7_transport_gxs_dir = '/opt/FISPACT-II-3-20/ENDFdata/ENDFB71data/endfb71-n/gxs-709/'
  tendl_probt_dir = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tal2015-n/tp-709-294/'
  tendl_decay_index = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tendl15_decay12_index'

  # gather list of desired nuclides (compare available ENDFs and desired files) and write nuclide index file 
  print('Assembling nuclide list...')
  nuclide_str_list, lines, mat_nums = gather_nuclide_set.get_list(endfb7_transport_gxs_dir, tendl_decay_index, tendl_probt_dir, elements_list)
  print('Writing {} nuclides to a reduced index...'.format(len(nuclide_str_list)))
  print(nuclide_str_list)
  gather_nuclide_set.write_list(lines, reduced_index_path)
  nuclide_object_list = [Nuclide(tup[0], tup[1]) for tup in zip(nuclide_str_list, mat_nums)]

  # bin dependent paths
  # does the MG and PT data exist?
  probt_dir = '/Users/Fred/projects/SSF_study/probt_data/tp-{:d}-{:d}_{:s}'.format(nbins, temp, uid)
  if not os.path.exists(probt_dir):
    gen_PT.run(ebins_filepath, nuclide_object_list, temp)
    if not os.path.exists(probt_dir):
      exit("Probability table data wasn't available, tried to create it and failed...")
  gendf_dir = '/Users/Fred/projects/SSF_study/gendf_data/gxs-{:d}_{:s}'.format(nbins, uid)
  if not os.path.exists(gendf_dir):
    gen_MG.run(ebins_filepath, nuclide_object_list, temp)
    if not os.path.exists(gendf_dir):
      exit("GENDF data wasn't available, tried to create it and failed...")
    
  if fluxes: # passed from argument
    # make fluxes file for MCNP sampled spectral information
    utility.write_arb_fluxes(phi)
    time_str = "_".join(str(datetime.now()).split())
    time_str = time_str.replace(":", "-")
    archive_path = os.path.join(ssf_data_dir, "{:d}_{:s}".format(nbins, uid), "arb-{:s}".format(time_str))
  else:
    # make fluxes file given phi(E) = E*exponent
    print('Calculating fluxes from exponent and writing to file...')
    utility.write_fluxes(ebins_filepath, exponent, nbins)
    archive_path = os.path.join(ssf_data_dir, "{:d}_{:s}".format(nbins, uid), "E^{:1.5f}".format(exponent))

  # paths for archiving of results
  input_path = os.path.join(archive_path, 'input')
  output_path = os.path.join(archive_path, 'output')
  log_path = os.path.join(archive_path, 'log')

  # make symlinks to MG and PT data in working dir
  print('Symlinking MG and PT data into working directory...')
  init_dir = os.getcwd()
  gendf_symlink = 'gxs-{:d}'.format(nbins)
  probt_symlink = 'tp-{:d}-{:d}'.format(nbins, temp)
  call('ln -s {:s} {:s}'.format(gendf_dir, gendf_symlink), shell=True)
  call('ln -s {:s} {:s}'.format(probt_dir, probt_symlink), shell=True)

  # make files file
  print('Generating FISPACT files file...')
  gen_SSF.make_files_file(ebins_filepath, gendf_symlink, probt_symlink, fispact_files_filename)

  # do main loop of FISPACT runs for all nuclides (threaded)
  gen_SSF.run_FISPACT(fispact_files_filename, input_path, output_path, log_path, nbins, nuclide_str_list, init_dir, compressed=False)

  # delete MG and PT symlinks
  print('Removing symlinks...')
  os.chdir(init_dir)
  os.remove(gendf_symlink)
  os.remove(probt_symlink)
  return bins, archive_path, uid

if __name__ == "__main__":
  main()

