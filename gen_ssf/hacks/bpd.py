#!/usr/bin/env python3
import utility
import numpy as np

path_list=['/opt/FISPACT-II-3-20/ENDFdata/ebins/ebins_175',
           '/opt/FISPACT-II-3-20/ENDFdata/ebins/ebins_315',
           '/opt/FISPACT-II-3-20/ENDFdata/ebins/ebins_709',
           '/Users/Fred/projects/SSF_study/ebins/ebins_650_ea4ba38b',
           '/Users/Fred/projects/SSF_study/ebins/ebins_280_406ce462']

def get_bpd(path):
  a=utility.get_energies(path)
  a=a[::-1]
  print("***************")
  print("     {:d}       ".format(len(a) - 1))
  print("***************")
  U=np.log10(np.roll(a,1))-np.log10(a)
  dec=np.linspace(-5,9,15)
  la = np.log10(a)
  bpds=[]
  print("i, bpd, start, end")
  for i in dec:
    start_idx = np.abs(la-i).argmin()
    end_idx = np.abs(la- i-1).argmin()
    bpd = int(end_idx - start_idx)
    print("{:2.0f} {:2d} {:2.3f} {:2.3f}".format(i, bpd, la[start_idx], la[end_idx]))
    bpds.append(bpd)
  bpds=np.array(bpds)
  bpds=bpds[np.nonzero(bpds)]
  print(bpds)
  print("mean value: {}".format(bpds.mean()))
  print("bpd sum: {}".format(bpds.sum()))
  return 

for path in path_list:
  get_bpd(path)
