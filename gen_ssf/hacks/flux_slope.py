#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np

def lethargy(e):
  """compute lethargy array from energy boundary array 
  uses ln(E2/E1) i.e. natural log of bin width to 
  calculate lethargy interval"""
  e_roll = np.roll(e, 1)
  e_roll[0] = e[0] / (e[1] / e[0])
  return np.log(e/e_roll)

E = np.logspace(-3,7,709)

f,ax=plt.subplots()
for exponent in [-1]:
  phi = E**exponent
  ax.step(E, E*phi/lethargy(E), label=r"$E \cdot \phi\mathrm{(E)}$")
  ax.step(E, (phi)/lethargy(E), label=r"$\phi\mathrm{(E)}$")
ax.set_yscale('log')
ax.set_xscale('log')
ax.set_xlabel('Energy [eV]')
ax.set_ylabel(r'per lethargy')
ax.set_ylim(0,100)
ax.set_xlim(0,100)
ax.legend()
ax.grid()
plt.show()
