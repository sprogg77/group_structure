#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
from scipy.integrate import quad
from tqdm import tqdm

E_min = -3
E_max = 8

def main():
  E_arr = np.logspace(E_min, E_max, 1000)

  g,bx = plt.subplots()
  bx.plot(E_arr, zero_to_one(E_arr))
  bx.plot(E_arr, rho_sin(E_arr))
  bx.plot(E_arr, rho_linear(E_arr, m=-0.5, c=1))
  bx.set_xscale('log')
  plt.show()

  f,ax = plt.subplots()
  ax.set_xscale('log')
  ax.set_ylabel(r'Energy density, $\rho(E)$')
  ax.set_xlabel('Energy, E [eV]')

  ax.plot(E_arr, rho_sin(E_arr), label='zto sin(E)', color='b')
  ax.plot(E_arr, rho_sin_lin(E_arr), label='lin sin(E)', color='m')
  grp_arr = determine_group(rho_sin_lin, 1E-3, 1E8, 100)
  ax.plot(grp_arr, np.zeros_like(grp_arr), marker="|", markersize=12, color='b')

  ax.plot(E_arr, rho_linear(E_arr, m=-0.5, c=1), label='zto 1 - 0.5E', color='r')
  ax.plot(E_arr, rho_lin(E_arr, m=-0.5, c=1), label='1 - 0.5E', color='k')
  grp_arr = determine_group(rho_lin, 1E-3, 1E8, 150, -0.5, 1)
  ax.plot(grp_arr, np.ones_like(grp_arr), marker="|", markersize=12, color='r')

  ax.set_ylim(0,1)
  ax.legend()
  ax.grid()
  plt.show()
  return

def zero_to_one(E):
  return 1/(E_max - E_min) * (np.log10(E) + np.abs(E_min))

def rho_sin(E):
  return np.sin(np.pi * zero_to_one(E))

def rho_sin_lin(E):
  return np.sin(np.pi * E)

def rho_linear(E, m, c):
  return m * zero_to_one(E) + c

def rho_lin(E, m, c):
  return m * E + c

def determine_group(func, E_min, E_max, n_bins, *args):
  """Calculates bin bound positions for energy group array by principle of equal area.
  Argument func is some description of bin density shape as a function of energy, limits
  and required bin count also given. Pass-through arguments to func are args.
  """
  # integrate whole function
  A_total = quad(func, zero_to_one(1E-3), zero_to_one(1E8), args=args)[0]
  A_bin_avg = A_total / n_bins
  # create empty group array
  grp_arr = np.zeros(n_bins + 1)
  # set first value to minimum energy
  grp_arr[0] = E_min
  # move through bound indices, looking for energies whose bounding integral is close to A_bin_avg
  for i in tqdm(range(len(grp_arr) - 2)):
    grp_arr[i+1] = grp_arr[i]
    while True:
      A_bin = quad(func, zero_to_one(grp_arr[i]), zero_to_one(grp_arr[i+1]), args=(args))[0]
      if A_bin/A_bin_avg > 1.01:
        # bin too large, energy bound too far away
        grp_arr[i+1] = grp_arr[i+1] / 2
      elif A_bin/A_bin_avg < 0.99:
        # bin too small, energy bound too close
        grp_arr[i+1] = grp_arr[i+1] * 2.5
      else:
        # bin integral within 1% of desired, bound placement acceptable, move to next bin
        break
  # set last value to maximum energy
  grp_arr[-1] = E_max
  return grp_arr

if __name__ == "__main__":
  main()
