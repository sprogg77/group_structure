#!/usr/bin/env python3

import os
import re
import shutil
from subprocess import call, DEVNULL
import multiprocessing
from glob import glob
from tqdm import tqdm
import numpy as np
import traceback
import gather_nuclide_set 
from utility import monitor_threaded_execution

def main():
  endfb7_gxs_dir = '/opt/FISPACT-II-3-20/ENDFdata/ENDFB71data/endfb71-n/gxs-709/'
  tendl_probt_dir = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tal2015-n/tp-709-294/'
  tendl_decay_index = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tendl15_decay12_index'
  fispact_files_filename = 'files.tendl15-n'
  input_path = os.path.join(run_path, 'input')
  output_path = os.path.join(run_path, 'output')
  log_path = os.path.join(run_path, 'log')
  
  nbins = 709

  nuclide_list, lines, mat_nums = gather_nuclide_set.get_list(endfb7_gxs_dir, tendl_decay_index, tendl_probt_dir)
  run_FISPACT(fispact_files_filename, input_path, output_path, log_path, nbins, nuclide_list)

def make_files_file(ebins_path, gendf_dir, probt_dir, files_filename):
  files_file_str = """# gamma attenuation data
absorp  /opt/FISPACT-II-3-20/ENDFdata/decay/abs_2012
# index of nuclides to be included
ind_nuc reduced_nuclide_index
# Library cross section data
enbins {ebins_path}
xs_endf ../{gendf_dir}
xs_endfb tal2015-n.bin
# Library probability tables for self-shielding
prob_tab ../{probt_dir}
#fluxes
fluxes  fluxes
arb_flux  spectra
# Library decay data
dk_endf /opt/FISPACT-II-3-20/ENDFdata/decay/decay_2012
# Library fission  data
fy_endf /opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tal2015-n/gef52_nfy
# Spontaneous fission data
sf_endf /opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tal2015-n/gef52_sfy
# Library regulatory data
hazards /opt/FISPACT-II-3-20/ENDFdata/decay/hazards_2012
clear   /opt/FISPACT-II-3-20/ENDFdata/decay/clear_2012
a2data  /opt/FISPACT-II-3-20/ENDFdata/decay/a2_2012
# collapsed cross section data (in and out)
collapxi  COLLAPX
collapxo  COLLAPX
# condensed decay and fission data (in and out)
arrayx  ARRAYX""".format(ebins_path=ebins_path, gendf_dir=gendf_dir, probt_dir=probt_dir)
  with open(files_filename, 'w') as f:
    f.write(files_file_str)
  return

def run_FISPACT(fispact_files_filename, input_path, output_path, log_path, nbins, nuclide_list, init_dir, compressed=True):
  """Ensures directory structure in place, opens threading pool and then spawns FISPACT runs of nuclide_list.
  """
  # ensure output directories exist
  for directory in [input_path, log_path, output_path]:
    if not os.path.exists(directory):
      os.makedirs(directory)

  # multithreaded calculation of SSF values
  cpu_count = multiprocessing.cpu_count()
  print("Initiating multi-threaded calculation of SSFs on {} threads.".format(cpu_count))
  # maxtasksperchild=1 kills process upon completion of single task, then spawns a new process
  # this is needed to avoid clashes with getcwd(), chdir(), etc. 
  # still parallel!
  pool = multiprocessing.Pool(processes=cpu_count, maxtasksperchild=1)
  arg_list = [(nuclide, fispact_files_filename, input_path, output_path, log_path, nbins, init_dir, compressed) for nuclide in nuclide_list]
  chunksize = int(np.ceil(len(arg_list)/(2*cpu_count)))
  result = pool.map_async(threaded_FISPACT_wrapper, arg_list, chunksize) # result updates during execution
  pool.close() # no more work to be added
  monitor_threaded_execution(result)
  return

def execute_FISPACT(nuclide, fispact_files_filename, input_path, output_path, log_path, nbins, parent_dir_path, compressed):
  """Function for main loop (over nuclides) to call. Needed as a seperate function for threading.
  """
  if nuclide[-1] == 'm':
    element = remove_digits(nuclide[:-1])
  else:
    element = remove_digits(nuclide)
  input_filename = nuclide + '.i'
  output_filename = nuclide + '.out'
  if output_filename in os.listdir(output_path):
    return
  fispact_input_str = """<< -----collapse cross section data----- >>
CLOBBER
GETXS {xs_data_format} 1
SSFCHOOSE 1 1
  {nuclide}
SSFMASS 0.001 1
  {element} 100.0
PROBTABLE 1 1
FISPACT
* COLLAPSE tal2015-n/gxs-{nbins} with tp-{nbins}-294
PRINTLIB 4
END
* END OF RUN"""
  working_dir_path = os.path.join(parent_dir_path, nuclide)
  os.makedirs(working_dir_path)
  # make symlinks for the input files shared by all nuclides
  for filename in [fispact_files_filename, 'fluxes', 'reduced_nuclide_index']:
    call("ln -s {} {}".format(os.path.join(parent_dir_path, filename), 
                              os.path.join(working_dir_path, filename)), 
                              shell=True)
  os.chdir(working_dir_path)
  if compressed == True:
    xs_data_format = '-1'
  else:
    xs_data_format = '1'
  with open(input_filename, 'w') as f:
    f.write(fispact_input_str.format(nbins=nbins, nuclide=nuclide, element=element, xs_data_format=xs_data_format))
  # run simulation, suppressing output
  call('fispact {} {}'.format(input_filename[:-2], fispact_files_filename), shell=True, stdout=DEVNULL, stderr=DEVNULL)
  # using subprocess.call for mv commands was failing, shutil seems to work
  shutil.move("{}.i".format(nuclide), input_path)
  shutil.move("{}.out".format(nuclide), output_path)
  shutil.move("{}.log".format(nuclide), log_path)
  os.chdir(parent_dir_path)
  shutil.rmtree(nuclide)
  return

def threaded_FISPACT_wrapper(args):
  return execute_FISPACT(*args)

def remove_digits(string):
  return ''.join([i for i in string if not i.isdigit()])

if __name__ == "__main__":
  main()

