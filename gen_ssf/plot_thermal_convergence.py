#!/usr/bin/env python3

import utility
import gen_SSF
import read_output
import gather_nuclide_set
import gen_MG
import gen_PT
import workflow
import run_printlib
import conv_comp_workflow
from nuclide import Nuclide
from parse_mcnp_output import Simulation, Reaction, MG_Tally, PW_Tally
import pickle
import os
import numpy as np
import matplotlib.pyplot as plt
from sys import argv
from collections import OrderedDict

def main():
  # MCNP output and pickled database location arguments
  if len(argv) != 3:
    exit("Please give MCNP output and pickle file paths as arguments.")
  output_path = argv[1]
  pickle_filepath = argv[2]

  # hardcoded inputs
  N = ('O', 16)
  MT = 102
  MF = 3
  elements_list = ['O']
  pw_tally_num = 994
  temp = 294
  cell_num = 2
  cell_volume = 1
  d = {64:'/Users/Fred/projects/SSF_study/ebins/ebins_64_converge',
       128:'/Users/Fred/projects/SSF_study/ebins/ebins_128_converge',
#       175:'/Users/Fred/projects/SSF_study/ebins/ebins_175_xxxxxxxx',
       256:'/Users/Fred/projects/SSF_study/ebins/ebins_256_converge',
#       315:'/Users/Fred/projects/SSF_study/ebins/ebins_315_xxxxxxxx',
       512:'/Users/Fred/projects/SSF_study/ebins/ebins_512_converge',
#       709:'/Users/Fred/projects/SSF_study/ebins/ebins_709_xxxxxxxx',
       1024:'/Users/Fred/projects/SSF_study/ebins/ebins_1024_converge',
       2048:'/Users/Fred/projects/SSF_study/ebins/ebins_2048_converge',
       4096:'/Users/Fred/projects/SSF_study/ebins/ebins_4096_converge',
       8192:'/Users/Fred/projects/SSF_study/ebins/ebins_8192_converge',
       16384:'/Users/Fred/projects/SSF_study/ebins/ebins_16384_converge'}

  paths = OrderedDict(sorted(d.items(), key=lambda t: t[0]))
  mg_tally_nums = []
  for i, nbin in enumerate(paths.keys()):
    tally_num = i * 10 + 4
    mg_tally_nums.append(tally_num)

  #################################
  # READ SPECTRA FROM MCNP OUTPUT #
  #################################
  nuclides = [N]
  mt_numbers = [MT] * len(nuclides)
  # only works for one nuclide
  pw_tally_dict = {N: pw_tally_num}
  mg_tally_dict = {N: mg_tally_nums}
  if os.path.exists(pickle_filepath):
    X = pickle.load(open(pickle_filepath, 'rb'))
  else:
    X = Simulation(output_path, nuclides, mt_numbers, pw_tally_dict, mg_tally_dict)
    pickle.dump(X, open(pickle_filepath, 'wb'))

  #####################
  # COLLAPSE MANUALLY #
  #####################
  nuc = "".join([N[0], str(N[1])])
  bpds = []
  bin_counts = []
  mg_sigmas = []
  for bin_count, ebins_path in paths.items():
    uid = ebins_path.split('_')[-1]
    nuc_str = "{:s}{:d}".format(N[0], N[1])
    gendf_path = '/Users/Fred/projects/SSF_study/gendf_data/gxs-{:d}_{:s}/{:s}g.asc'.format(bin_count, uid, nuc_str)
    if not os.path.isfile(gendf_path):
      nuclide_list = [Nuclide(nuc_str, mat_num)]
      gen_MG.run(ebins_path, nuclide_list)
    mg_e, mg_xs = utility.read_gendf(gendf_path, mf=MF, mt=MT)
    RR = X.reactions[N, MT].pw_tally.nps[-1]['tally']
    phi = X.reactions[N, MT].mg_tallies[bin_count].nps[-1]['tally']
    mcnp_e = X.reactions[N, MT].mg_tallies[bin_count].nps[-1]['energy']
    pw_xs = RR/phi.sum()
    mg_RR = (mg_xs[:-1] * phi).sum() # reject last GENDF bin, always 0 for beyond EMAX
    mg_sigma = mg_RR / phi.sum()
    max_e = max(mg_e)
    min_e = min(mg_e)
    decades = np.log10(max_e) - np.log10(min_e)
    bpd = bin_count / decades
    bpds.append(bpd)
    bin_counts.append(bin_count)
    mg_sigmas.append(mg_sigma)

  for i, nbin in enumerate(bin_counts):
    print(i, nbin, mg_sigmas[i], pw_xs, 100*((mg_sigmas[i] - pw_xs) / pw_xs))

  plot_xs_group_structure(N, MT, bpds, mg_sigmas, pw_xs)
  return

def plot_xs_group_structure(N, MT, nbins, mg_sigmas, pw_sigma):
  f,ax = plt.subplots(figsize=(5.5,4.3))
  percent_error = 100 * (mg_sigmas - pw_sigma) / pw_sigma
  ax.set_xscale('log')
  ax.set_xticks(nbins)
  ax.set_xticklabels(labels=["{:d}".format(int(nbin)) for nbin in nbins], rotation='vertical', fontsize=10)
  ax.minorticks_off()
  ax.set_xlabel("Bins per energy decade")
  ax.set_ylabel(r"$\%\Delta$ on pointwise")
  ax.set_yscale('log')
  ax.set_ylim(2E-2, 2E0)
  nuclide_file_str = '-'.join([N[0], str(N[1])])
  nuclide_str = '$^{%d}$%s' % (int(N[1]), N[0])
  ax.set_title(r'Thermal convergence for {:s}(n,$\gamma$)'.format(nuclide_str))
  ax.scatter(nbins, percent_error, color='red', marker='o', facecolors='None')
  plt.tight_layout()
  plt.subplots_adjust(left=0.17,right=0.83)
  f.savefig(nuclide_file_str + '.png', transparent=True)
  f.savefig(nuclide_file_str + '.eps')
  return
     
if __name__ == "__main__":
  main()

