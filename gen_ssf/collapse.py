#!/usr/bin/env python3

import os
import pickle
import re
import math
import numpy as np
import matplotlib.pyplot as plt
import gen_MG 
from sys import argv
from collections import OrderedDict
from parse_mcnp_output import Simulation, Reaction, MG_Tally, PW_Tally
from utility import get_energies, read_gendf
from nuclide import Nuclide

def main():
  cell_num = 2
  cell_volume = 1
  N = ('O', 16)
  mat_num = 825
  MT = 102
  MF = 3
  d = {64:'/Users/Fred/projects/SSF_study/ebins/ebins_64_converge',
       128:'/Users/Fred/projects/SSF_study/ebins/ebins_128_converge',
       175:'/Users/Fred/projects/SSF_study/ebins/ebins_175_xxxxxxxx',
       256:'/Users/Fred/projects/SSF_study/ebins/ebins_256_converge',
       315:'/Users/Fred/projects/SSF_study/ebins/ebins_315_xxxxxxxx',
       512:'/Users/Fred/projects/SSF_study/ebins/ebins_512_converge',
       709:'/Users/Fred/projects/SSF_study/ebins/ebins_709_xxxxxxxx',
       1024:'/Users/Fred/projects/SSF_study/ebins/ebins_1024_converge',
       2048:'/Users/Fred/projects/SSF_study/ebins/ebins_2048_converge',
       4096:'/Users/Fred/projects/SSF_study/ebins/ebins_4096_converge',
       8192:'/Users/Fred/projects/SSF_study/ebins/ebins_8192_converge',
       16384:'/Users/Fred/projects/SSF_study/ebins/ebins_16384_converge'}

  paths = OrderedDict(sorted(d.items(), key=lambda t: t[0]))
  nuclides = [N]
  mt_numbers = [MT] * len(nuclides)
  group_structures = paths.keys()
  pw_tally_dict, mg_tally_dict = make_tally_dicts(group_structures, nuclides)

  if len(argv) != 3:
    exit("Please give MCNP output and pickle file paths as arguments.")
  output_path = argv[1]
  pickle_filepath = argv[2]
  if os.path.exists(pickle_filepath):
    X = pickle.load(open(pickle_filepath, 'rb'))
  else:
    X = Simulation(output_path, nuclides, mt_numbers, pw_tally_dict, mg_tally_dict)
    pickle.dump(X, open(pickle_filepath, 'wb'))

  print('\nMCNP data\n')
  print('nbins  PW    MG    MG/PW err %')
  g,bx = plt.subplots(figsize=(6,4))
  errors = []
  bin_counts = []
  for bin_count, ebins_path in paths.items():
    bin_counts.append(bin_count)
    uid = ebins_path.split('_')[-1]
    nuc_str = "{:s}{:d}".format(N[0], N[1])
    gendf_path = '/Users/Fred/projects/SSF_study/gendf_data/gxs-{:d}_{:s}/{:s}g.asc'.format(bin_count, uid, nuc_str)
    if not os.path.isfile(gendf_path):
      nuclide_list = [Nuclide(nuc_str, mat_num)]
      gen_MG.run(ebins_path, nuclide_list)
    mg_e, mg_xs = read_gendf(gendf_path, mf=MF, mt=MT)
    RR = X.reactions[N, MT].pw_tally.nps[-1]['tally']
    phi = X.reactions[N, MT].mg_tallies[bin_count].nps[-1]['tally']
#    bx.step(mg_e[:-1], phi*mg_xs[:-1], label=bin_count, where='mid')
    bx.step(mg_e[:-1], phi/get_U(mg_e[:-1]), label=bin_count, where='mid', ls='-')
    mcnp_e = X.reactions[N, MT].mg_tallies[bin_count].nps[-1]['energy']
    pw_xs = RR/phi.sum()
    mg_RR = (mg_xs[:-1] * phi).sum() # reject last GENDF bin, always 0 for beyond EMAX
    mg_ogxs = mg_RR / phi.sum()
    ratio = mg_ogxs/pw_xs
    error = 100* (mg_ogxs - pw_xs) / pw_xs
    errors.append(error)
    print("{:6d} {:3.3f} {:3.3f} {:3.3f} {:3.3f} {:s}".format(bin_count, pw_xs, mg_ogxs, mg_ogxs/pw_xs, error, gendf_path.split('/')[-2]))
  f,ax = plt.subplots(figsize=(7,5))
  plt.subplots_adjust(left=0.15, bottom=0.15)
  ax.scatter(bin_counts, errors)
  ax.set_yscale('log')
  ax.set_title(r'Convergence of $^{16}$O(n,$\gamma$) with increasingly fine GENDFs')
  ax.set_xlabel('$\mathrm{n_{bins}}$')
  ax.set_xscale('log')
  ax.set_xticks([])  
  ax.set_xticks(bin_counts)
  ax.set_xticklabels(["{:d}".format(xtick) for xtick in bin_counts], rotation='vertical')
  ax.set_ylabel('% error on point-wise')
  ax.set_yscale('log')
  ax.grid(which='both', axis='y')
  bx.legend()
#  bx.set_ylabel('Reaction rate (E) [arb.]')
  bx.set_ylabel(r'Flux per lethargy interval (E/$\Delta$U) [arb.]')
  bx.set_xlabel('Energy [eV]')
  bx.set_title('Thermal spectrum for various groups')
  bx.set_yscale('log')
  bx.set_xscale('log')

#  plot_spectrum(X, N, MT)
  plt.show()
  return

def bin_ndarray(ndarray, new_shape, operation='sum'):
    """
    Bins an ndarray in all axes based on the target shape, by summing or
        averaging.

    Number of output dimensions must match number of input dimensions and 
        new axes must divide old ones.

    Example
    -------
    >>> m = np.arange(0,100,1).reshape((10,10))
    >>> n = bin_ndarray(m, new_shape=(5,5), operation='sum')
    >>> print(n)

    [[ 22  30  38  46  54]
     [102 110 118 126 134]
     [182 190 198 206 214]
     [262 270 278 286 294]
     [342 350 358 366 374]]

    """
    operation = operation.lower()
    if not operation in ['sum', 'mean']:
        raise ValueError("Operation not supported.")
    if ndarray.ndim != len(new_shape):
        raise ValueError("Shape mismatch: {} -> {}".format(ndarray.shape,
                                                           new_shape))
    compression_pairs = [(d, c//d) for d,c in zip(new_shape,
                                                  ndarray.shape)]
    flattened = [l for p in compression_pairs for l in p]
    ndarray = ndarray.reshape(flattened)
    for i in range(len(new_shape)):
        op = getattr(ndarray, operation)
        ndarray = op(-1*(i+1))
    return ndarray

def plot_xs_group_structure(X, N, MT, atomic_densities):
  f,ax = plt.subplots(figsize=(4.5,3.7))
  RR = X.reactions[N, MT].pw_tally.nps[0]['tally']
  phi = X.reactions[N, MT].mg_tallies[650].nps[-1]['tally'].sum()
  density = atomic_densities[N]
  pw_sigma = RR/(density*phi)
  tuple_list = []
  for bin_count in X.reactions[(N, MT)].shielded.keys():
    tuple_list.append((bin_count, 
                       X.reactions[(N, MT)].shielded[bin_count], 
                       X.reactions[(N, MT)].unshielded[bin_count])) 
  def unsh_sort(x):
    return np.abs(x - pw_sigma)
  tuple_list = sorted(tuple_list, key=lambda triplet: unsh_sort(triplet[2]), reverse=True)
  bin_count_list = [x[0] for x in tuple_list]
  shielded_xs_list = [x[1] for x in tuple_list]
  unshielded_xs_list = [x[2] for x in tuple_list]
  shielded_xs_percent_list = 100 * (shielded_xs_list - pw_sigma) / pw_sigma
  unshielded_xs_percent_list = 100 * (unshielded_xs_list - pw_sigma) / pw_sigma
  width = 0.2
  ax.set_xticks(np.arange(len(bin_count_list)) + width / 2)
  ax.set_xticklabels(bin_count_list, rotation='horizontal', fontsize=12)
  ax.set_xlabel("Group structure")
  ax.set_ylabel(r"$\%\Delta$ on pointwise")
  minmin = min(min(shielded_xs_percent_list), min(unshielded_xs_percent_list))
  maxmax = max(max(shielded_xs_percent_list), max(unshielded_xs_percent_list))
  span = maxmax - minmin
  gap = 0.05 * span 
  ymin = minmin - gap
  ymax = maxmax + gap
  ax.set_ylim(ymin, ymax)
  bx = ax.twinx()
  aymin = (ymin * pw_sigma) / 100 + pw_sigma
  aymax = (ymax * pw_sigma) / 100 + pw_sigma
  bx.set_ylim(aymin, aymax)
  bx.set_ylabel('Collapsed cross-section, $\sigma_{n,\gamma}$ [barns]')
  nuclide_str = '-'.join([N[0], str(N[1])])
  ax.set_title('{:s} collapsed cross-sections'.format(nuclide_str))
  rects_ush = ax.bar(np.arange(len(bin_count_list)) + width, unshielded_xs_percent_list, width=width, color='red', label="Unshielded")
  rects_sh = ax.bar(np.arange(len(bin_count_list)), shielded_xs_percent_list, width=width, color='blue', label="Shielded")
  ax.axhline(0, color='black', linewidth=1)
  handles, labels = ax.get_legend_handles_labels()
  ax.legend()
  plt.tight_layout()
  plt.subplots_adjust(left=0.17,right=0.83)
  f.savefig(nuclide_str + '.png', transparent=True)
  f.savefig(nuclide_str + '.eps')
  return

def make_tally_dicts(paths, nuclides):
  """Generate the tally numbering scheme"""
  if len(nuclides) != 1:
    exit("Modify make_tally_dicts, currently only 1 nuclide permitted")
  pw_tally_dict = {}
  mg_tally_dict = {}
  for i, nuclide in enumerate(nuclides):
    pw_tally_num = 994
    pw_tally_dict[nuclide] = pw_tally_num
    mg_tally_list = []
    for i, nbins in enumerate(paths):
#      exponent = math.log(nbins) / math.log(2)
#      mg_tally_num = int(exponent) * 10 + 4
      mg_tally_num = i * 10 + 4
      mg_tally_list.append(mg_tally_num)
    mg_tally_dict[nuclide] = mg_tally_list
  return pw_tally_dict, mg_tally_dict  

def plot_spectrum(X, N, MT):
  f,ax = plt.subplots(figsize=(6,4))
  for bin_count, data in X.reactions[(N, MT)].mg_tallies.items():
    E = data.nps[-1]['energy']
    phi = data.nps[-1]['tally']
    ax.step(E, phi/get_U(E), label='{:d}'.format(bin_count))
  ax.set_xscale('log')
  ax.set_yscale('log')
  ax.set_xlabel(r'$\mathrm{Energy}, E\ [eV]$')
  ax.set_ylabel(r'$\mathrm{Fluence}, \phi(E)\ [n \cdot n^{-1}_{src} \cdot cm^{-2} \cdot \Delta U^{-1}]$')
  nuclide_str = '-'.join([N[0], str(N[1])])
  handles, labels = ax.get_legend_handles_labels()
  # sort both labels and handles by labels
  labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
  ax.legend(handles, labels)
  ax.grid()
  ax.set_title('{:s} spectrum'.format(N[0]))
  plt.subplots_adjust(left=0.15,bottom=0.16,right=0.94)
#  ax.set_xlim(1E-2, 1E8)
#  ax.set_ylim(1E-9, 1E-4)
  f.savefig('w_spectrum.png', transparent=True)
  f.savefig('w_spectrum.eps', transparent=True)
  return

def get_U(E):
  """Compute lethargy width for input energy group structure."""
  U = np.log10(E) - np.log10(np.roll(E, 1))
  U[0] = 0
  return U

if __name__ == "__main__":
  main()





