#!/usr/bin/env python3

from utility import get_energies
from collections import OrderedDict
import numpy as np
import matplotlib.pyplot as plt

summary_path = '/Users/Fred/projects/SSF_study/ssf_data/{:d}/E^0.0/ssf_summary.out'
ebins_path = '/Users/Fred/projects/SSF_study/ebins/ebins_{:d}'
nbins_list = [139,277,552,1102]
ncols = 15
nrows = 5
plot_count = ncols * nrows

def fetch_ssfs(data, nuclide):
  d = data[nuclide]
  bin_ssf_sorted_tuple = [(k, d[k]) for k in sorted(d,key=d.get)]
  ssfs = []
  bins = []
  for nbins, ssf in bin_ssf_sorted_tuple:
    ssfs.append(ssf)
    bins.append(nbins)
  return bins,ssfs

data = {}
    
for nbins in nbins_list:
  with open(summary_path.format(nbins), 'r') as f:
    lines = f.readlines()
  for line in lines:
    nuclide = line.split()[0]
    gamma = float(line.split()[2])
    try:
      data[nuclide][nbins] = gamma
    except:
      data[nuclide] = {nbins:gamma}

for nuclide in data.keys():
  print(nuclide, data[nuclide])

with open(summary_path.format(nbins_list[0]), 'r') as f:
  lines = f.readlines()
bad_nuclides = []
for line in lines[-plot_count::-1]:
  nuclide = line.split()[0]
  bad_nuclides.append(nuclide)

xticklabels = ['{:d}'.format(i) for i in nbins_list]
xticks=list(range(len(nbins_list)))

f,axarr = plt.subplots(nrows,ncols,sharex=True,sharey=True)
for row in list(range(nrows)):
  for col in list(range(ncols)):
    bni = (row + 1) * (col + 1) - 1
    nbins, ssfs = fetch_ssfs(data, bad_nuclides[bni])
    f.suptitle(r'$\overline{SSF}$ as function of energy bin count, most poorly represented nuclides', fontsize=24)
    axarr[row,col].bar(list(range(len(nbins))), ssfs)
    axarr[row,col].set_ylim(0,1)
    axarr[row,col].set_title(bad_nuclides[bni])
    axarr[row,col].set_xticks(xticks)
    axarr[row,col].set_xticklabels(xticklabels, rotation=270)

plt.subplots_adjust(hspace=0.3)

plt.show()


