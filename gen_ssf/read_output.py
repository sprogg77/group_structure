#!/usr/bin/env python3

from utility import get_energies
import os
import re
import pickle
import numpy as np 
import mimic_alpha as ma
from fill_between_steps import fill_between_steps
from scipy.stats import gaussian_kde
from glob import glob
from tqdm import tqdm
from collections import OrderedDict
from sys import argv
from matplotlib import cm
from datetime import datetime
from time import sleep
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.colors as mcolors

np.set_printoptions(linewidth=144)

def main():
  if len(argv) != 2:
    exit("Please give run dir, e.g.: \'../ssf_data/280_345ga2dd/E^0.0\'")
  else:
    run_path = argv[1]
  split_path = [s for s in run_path.split('/') if s]
  bins_str = split_path[-2]
  nbins = int(bins_str.split('_')[0])
  uid = bins_str.split('_')[1]
  exponent = float(split_path[-1].split('^')[-1])
  ebins_path = '/Users/Fred/projects/SSF_study/ebins/ebins_{:d}_{:s}'.format(nbins, uid)
  analyse(run_path, exponent, ebins_path, nbins, uid)
  plt.show()

def analyse(run_path, exponent, ebins_path, nbins, uid):
  figure_path = os.path.join(run_path, 'figure')
  if os.path.exists(figure_path):
    pass
  else:
    os.mkdir(figure_path)
  output_path = os.path.join(run_path, 'output')
  ssf_data_filename = os.path.join(run_path, 'ssf_data.pkl')
  symbol_map_filename = 'elements.csv'

  E = get_energies(ebins_path)
  phi = E**exponent

  # get database of SSFs
  if os.path.exists(ssf_data_filename):
    X = load_obj(ssf_data_filename)
  else:
    X = build_database(E, phi, output_path)
    save_obj(X, ssf_data_filename)

  sym_dict = get_sym_dict(symbol_map_filename)
  ssf_dict = {}
  ssf_abs_dict = {}
  ssf_mat = make_mat(dtype=np.float64, fill=-1)
  
  # find PDF of SSF effect for all nuclides
  pdf_gamma_all = np.zeros_like(E)
  # for each nuclide, retreive SSFs, perform integration for SSF bar and plot results
  for nuc in tqdm(X.keys()):
    E_res, rr_tot, rr_tot_sh, ssf, ssf_bar, g0, gn = fold_total(X, E, phi, nuc)
    E_res_abs, rr_abs, rr_abs_sh, ssf_abs, ssf_abs_bar, g0_abs, gn_abs = fold_abs(X, E, phi, nuc)
    pdf_tot = plot_nuclide_ssf(nuc, E_res, rr_tot, rr_tot_sh, ssf, ssf_bar, figure_path, exponent, 'tot', nbins)
    pdf_gamma = plot_nuclide_ssf(nuc, E_res_abs, rr_abs, rr_abs_sh, ssf_abs, ssf_abs_bar, figure_path, exponent, '\gamma', nbins)
    pdf_gamma_all[g0_abs:gn_abs] += pdf_gamma
    ssf_dict[nuc] = ssf_bar
    ssf_abs_dict[nuc] = ssf_abs_bar
    Z, N = get_Z_N(sym_dict, nuc)
    ssf_mat[Z,N] = ssf_bar
  
  plot_ssf_pdf(E, pdf_gamma_all, run_path, exponent, nbins, uid)
  with open(os.path.join(run_path, 'ssf_pdf.out'), 'w') as f:
    for i, value in enumerate(E):
      f.write("{} {}\n".format(E[i], pdf_gamma_all[i]))

  # capture SSF bar for all nuclides, print output
  ssf_dict = OrderedDict(sorted(ssf_dict.items(), key=lambda t: t[1], reverse=True))
  ssf_list = []
  ssf_abs_dict = OrderedDict(sorted(ssf_abs_dict.items(), key=lambda t: t[1], reverse=True))
  ssf_abs_list = []
  with open(os.path.join(run_path, 'ssf_summary.out'), 'w') as f:
    for nuc in ssf_abs_dict.keys():
      ssf_bar = ssf_dict[nuc]
      ssf_abs_bar = ssf_abs_dict[nuc]
      if np.isfinite(ssf_bar):
        result_str = "{:8s} {:1.4f} {:1.4f}\n".format(nuc, ssf_bar, ssf_abs_bar)
        ssf_list.append(ssf_dict[nuc])
        ssf_abs_list.append(ssf_abs_dict[nuc])
        f.write(result_str)

#  plot_nuclides_float(ssf_mat, r'$\overline{\mathrm{SSF}}(\mathrm{Z, N})$ for TENDL2015 in $\phi(E) = E^{%1.1f}$ spectrum with %d groups' % (exponent, nbins), cm.jet, 0.0, 1, run_path, label_str=r'$\overline{\mathrm{SSF}}$')
#  plot_histogram(np.array(ssf_abs_list), r'$\overline{\mathrm{SSF}}$ for (n,$\gamma$) with $\phi(E) = E^{%1.1f}$ spectrum with %d groups' % (exponent, nbins), r'$\overline{\mathrm{SSF}}$', 'Frequency', run_path, xlogscale=False, ylogscale=True, bincount=40)
#  plot_kde(np.array(ssf_abs_list), r'$\overline{\mathrm{SSF}}$ for (n,$\gamma$) with $\phi(E) = E^{%1.1f}$ spectrum with %d groups' % (exponent, nbins), r'log$_{10}(\mathrm{Density})$', r'$\overline{\mathrm{SSF}}$', run_path)
  return E, pdf_gamma_all

def plot_ssf_pdf(E, pdf, run_path, exponent, nbins, uid):
  E_roll = np.roll(E, 1)
  bin_widths = np.log10(E_roll) - np.log10(E)
  bin_widths[0] = 0
  integral_vector = bin_widths * pdf
  integral = integral_vector.sum()
  
  fig = plt.figure(figsize=(7,5))
  ax = fig.add_subplot(111)
  ax.step(E, pdf, color='red', where='post', linewidth=0.5)
  light_blue = ma.colorAlpha_to_rgb((0,0,1),0.4)[0]
  light_red = ma.colorAlpha_to_rgb((1,0,0),0.4)[0]
  fill_between_steps(ax, E, pdf, 0, step_where='post', color=light_red)
  ax.text(0.035, 0.92, r'Energy integrated effective self-shielding = %1.2f' % integral,
          verticalalignment='bottom', horizontalalignment='left',
          transform=ax.transAxes, fontsize=10)
#  time_str = str(datetime.now())[:-3]
#  ax.text(0.69, 0.93, time_str, transform=ax.transAxes, fontsize=8)
  ax.set_title(r'Effective self-shielding with %d bins' % (nbins))
  ax.set_xlabel('Energy [eV]')
  ax.set_ylabel('Effective self-shielding distribution')
  ax.set_xlim(1E-2,1E5)
  ax.set_ylim(0,1.1 * pdf.max())
  ax.grid()
  ax.set_xscale('log')
  #ax.set_yscale('log')
  fig.savefig(os.path.join(run_path, 'ssf_pdf_hist.eps'))
  fig.savefig(os.path.join(run_path, 'ssf_pdf_hist.png'), transparent=True)
  return

def read_output(filename):
  with open(filename, 'r') as f:
    lines = f.readlines()
  total_start_found = False
  macro_start_found = False
  for line_num, line in enumerate(lines):
    if (total_start_found == False) and ("group     en-min" in line):
      total_start = line_num + 1
      total_start_found = True
    if (total_start_found == True) and ("group ip mt   sigma-d" in line):
      total_end = line_num - 1
      macro_start = line_num + 1
      macro_start_found = True
    if (macro_start_found == True) and ("P R O B A B I L I T Y" in line):
      macro_end = line_num - 3
      break
  # grab total SSFs... easy
  total = np.genfromtxt(filename, skip_header=total_start, max_rows=total_end-total_start, filling_values=-1)
  # robustly get macro-partials for absorption... harder
  first_line = lines[macro_start]
  abs_present = False
  for col_num, entry in enumerate(first_line.split()):
    if (entry == '101') and (col_num != 0):
      abs_present = True
      abs_col = col_num
  for line_num, line in enumerate(lines[macro_start:]):
    if ('101' not in line.split()) and (abs_present == True):
      abs_end = line_num + macro_start
      break
  cols = [abs_col + 1, abs_col + 2, abs_col + 3]
  rows = []
  for line in lines[macro_start:abs_end]:
    row = [float(line.split()[i]) for i in cols]
    rows.append(row)
  if abs_present == False:
    return total, None
  else:
    absorption = np.array(rows)
    return total, absorption

def build_database(E, phi, output_path):
  X = {}
  file_list = glob(os.path.join(output_path, '*.out'))
  total_header_str = "group     en-min       en-max      dilution   sigma_tot_d sigma_tot_inf sigma_tot_eaf      tot_ssf"
  abs_header_str = "abs_sigma-d   abs_sigma-inf      abs_ssf"
  for filename in tqdm(file_list):
    nuc = filename.split('/')[-1].split('.')[0]
    try: 
      total_xs_data, abs_xs_data = read_output(filename)
    except:
      continue
    total = {col_name:total_xs_data[:,col_num] for col_num, col_name in enumerate(total_header_str.split())}
    absorption = {col_name:abs_xs_data[:,col_num] for col_num, col_name in enumerate(abs_header_str.split())}
    X[nuc] = {**total, **absorption}
  return X

def fold_abs(X, E, phi, nuc):
  g0 = int(X[nuc]['group'][0]) - 1
  gn = int(X[nuc]['group'][-1])
  E_res = E[g0:gn]
  rr = phi[g0:gn] * X[nuc]['abs_sigma-inf']
  rr_bar = rr.sum()
  rr_sh = rr * X[nuc]['abs_ssf']
  rr_sh_bar = rr_sh.sum()
  ssf = X[nuc]['abs_ssf']
  ssf_bar = rr_sh_bar/rr_bar
  return E_res, rr, rr_sh, ssf, ssf_bar, g0, gn

def fold_total(X, E, phi, nuc):
  g0 = int(X[nuc]['group'][0]) - 1
  gn = int(X[nuc]['group'][-1])
  E_res = E[g0:gn]
  rr = phi[g0:gn] * X[nuc]['sigma_tot_inf']
  rr_bar = rr.sum()
  rr_sh = rr * X[nuc]['tot_ssf']
  rr_sh_bar = rr_sh.sum()
  ssf = X[nuc]['tot_ssf']
  ssf_bar = rr_sh_bar/rr_bar
  return E_res, rr, rr_sh, ssf, ssf_bar, g0, gn

def plot_nuclides_float(matrix, title_str, cmap, vmin, vmax, run_path, label_str=''):
  """Plot chart of nuclides (N, Z) for matrix of integers with Z rows and N columns
      expects data to be >=0, everything below coloured white"""
  fig, ax = get_fig_ax()
  axcolor = fig.add_axes([0.87, 0.19, 0.02, 0.62])
  im = ax.matshow(matrix, cmap=cmap, origin='lower', vmin=vmin, vmax=vmax)
  cbar_ticks = np.linspace(np.floor(vmin), np.ceil(vmax), 11)
  cb = fig.colorbar(im, cax=axcolor, format='$%1.1f$', ticks=cbar_ticks)
  # aesthetics
  cb.ax.tick_params(labelsize = 18)
  cb.set_label(label_str, size=22)
  im.cmap.set_under('white')
  ax.set_title(title_str)
  ax.xaxis.tick_bottom()
  for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
    ax.get_xticklabels() + ax.get_yticklabels()):
      item.set_fontsize(22)
  plt.subplots_adjust(left=0.08, right=0.86)
  fig.savefig(os.path.join(run_path, 'ssf_z_n.eps'))
  return

def get_Z_N(sym_dict, string):
  nuclide = string.split('.')[0]
  split = list(filter(None, re.split(r'(\d+)', nuclide)))
  Z = int(sym_dict[split[0]])
  N = int(split[1])
  if (len(split) == 3) and (split[2] == 'm'):
    metastable = True
  return Z, N

def plot_kde(x, title_str, y_label_str, x_label_str, run_path):
  x_grid = np.logspace(np.floor(np.log10(x.min())), 1, 1E5)
  kde = gaussian_kde(x, bw_method=0.2)
  density = kde.pdf(x_grid)
  fig = plt.figure(figsize=(7,6))
  ax = fig.add_subplot(111)
  ax.plot(x_grid, density, label='Kernel Density Estimator')
  ax.scatter(x, np.zeros(x.shape) + 0.01 * density.max(), color='b', marker="|", zorder=3, lw=1, label='Nuclide')
  ax.grid()
  ax.legend()
  pad = 0.1 * x.max()
  ax.set_xlim(x.min() - pad, x.max() + pad)
  ax.set_ylabel(y_label_str)
  ax.set_xlabel(x_label_str)
  ax.set_title(title_str)
  fig.savefig(os.path.join(run_path, 'ssf_density.eps'))

def plot_histogram(arr, title_str, x_label_str, y_label_str, run_path, xlogscale=False, ylogscale=False, bincount=50):
  fig = plt.figure(figsize=(7,6))
  ax = fig.add_subplot(111)
  ax.hist(arr, np.linspace(0, 1, bincount), color='g')
  ax.set_title(title_str)
  ax.set_xlabel(x_label_str)
  ax.set_ylabel(y_label_str)
  ax.grid()
  if xlogscale:
    ax.set_xscale('log')
  if ylogscale:
    ax.set_yscale('log')
  fig.savefig(os.path.join(run_path, 'ssf_hist.eps'))

def plot_nuclide_ssf(nuc, E_res, rr, rr_sh, ssf, ssf_bar, figure_path, exponent, channel, nbins):
  """Calculate certain derived quantities from RR(E) and SSF(E), cumulative and probability
  distributions of the self-shielding effects, plot these in twin x with RRs and SSFs."""

  # compute cumulative self shielding effect (SSF weighted by effect on RR)
  cdf = np.cumsum(rr[::-1] - ssf[::-1]*rr[::-1])
  # normalise and flip so CDF runs from 1 down to SSF_bar
  norm = cdf[-1]
  cdf_norm = 1 - ((1 - ssf_bar) * cdf/norm)
  # identify where CDF changes by subtracting CDF from CDF rolled along by one element
  pdf = np.roll(cdf_norm, 1) - cdf_norm
  # threshold array to zero, culls rounding errors and handles special case of
  # first bin, which is otherwise always negative... uses value rolled around from last bin
  pdf[np.where(pdf<0)] = 0

  suffix = "_" + channel.replace("\\","")
  filepath = os.path.join(figure_path, nuc + suffix)
  if os.path.exists(filepath + '.eps'):
    return pdf[::-1]

#  f,axarr = plt.subplots(2,figsize=(6,9),sharex=True,gridspec_kw = {'height_ratios':[2, 1]})
  f,axarr = plt.subplots(4,figsize=(6,9),sharex=True,gridspec_kw = {'height_ratios':[3,1,1,1]})
  axarr[0].step(E_res,rr,label='Unshielded $\mathrm{n,%s}$' % (channel), linewidth = 0.75,color='red')
  axarr[0].step(E_res,rr_sh,label='Shielded $\mathrm{n,%s}$' % (channel), linewidth=0.75,color='blue')
  axarr[0].set_yscale('log')
  axarr[0].set_ylabel('Reaction rate')
  axarr[0].set_xscale('log')
  axarr[0].grid()
  axarr[0].set_title(super_nuclide_str(nuc) + r' reaction rates and shielding metrics')
  axarr[0].legend()
  axarr[1].step(E_res,ssf, linewidth=0.75,color='green')
  axarr[1].set_ylabel('Self-shielding \nfactor (SSF)')
#  h1, l1 = axarr[0].get_legend_handles_labels()
#  h2, l2 = axarr[1].get_legend_handles_labels()
  axarr[2].step(E_res[::-1],cdf_norm,label='CDF self-shielding effect',linewidth=0.75,color='magenta',where='post')
  axarr[2].set_ylabel('Cumulative \nSSF effect')
  ax_sec = axarr[2].twinx()
  ax_sec.set_yticks([ssf_bar])
  ax_sec.set_yticklabels([r'$\overline{\mathrm{SSF}} = %1.3f$' % ssf_bar])
  light_red = ma.colorAlpha_to_rgb((1,0,0),0.4)[0]
  axarr[3].step(E_res[::-1],pdf,label='PDF self-shielding effect',linewidth=0.75,color='red',where='post')
  fill_between_steps(axarr[3], E_res[::-1], pdf, 0, step_where='post', color=light_red)
  axarr[3].set_ylabel('Effective self-\nshielding distribution')
  axarr[3].set_xlabel('Energy [eV]')
  for ax in [axarr[1], axarr[2], axarr[3]]:
    ax.set_ylim(0, 1.05)
    ax.grid()
    ax.set_yscale('linear')
    ax.set_yticks([0,1])
    ax.set_yticklabels(['0','1'])
#  axarr[0].legend(h1+h2, l1+l2, fontsize=8)
  plt.tight_layout()
#  plt.subplots_adjust(right=0.83)
  try:
    f.savefig(filepath + '.eps')
    f.savefig(filepath + '.png', transparent=True)
  except:
    pass
  plt.close(f)
  return pdf[::-1]

def super_nuclide_str(nuc_str):
  for i, char in enumerate(nuc_str):
    if char.isnumeric():
      A = nuc_str[i:]
      E = nuc_str[:i]
      break
  return r"$^{%s}\mathrm{%s}$" % (A, E)

def plot_nuclide(nuc, E_res, rr, rr_sh, ssf, ssf_bar, figure_path, exponent, channel, nbins):
  suffix = "_" + channel.replace("\\","")
  filepath = os.path.join(figure_path, nuc + suffix + '.eps')
  if os.path.exists(filepath):
    return
  f,ax = plt.subplots()
  ax.step(E_res,rr,label='Reaction rate', linewidth = 0.75)
  ax.step(E_res,ssf,label='Self-shielding factors', linewidth=0.75)
  ax.step(E_res,rr_sh,label='Shielded reaction rate', linewidth=0.75)
  ax.text(0.05, 0.05, r'$\overline{\mathrm{SSF}} = %1.3f$' % ssf_bar,
          verticalalignment='bottom', horizontalalignment='left',
          transform=ax.transAxes, fontsize=12)
  ax.set_title(r'SSF$_{\mathrm{n,%s}}$ for %s in $\phi(E) = E^{%1.3f}$ spectrum in %d groups' % (channel, nuc, exponent, nbins))
#  ax.set_xlim(1E-1, 1E5)
  ax.legend(fontsize=8, loc='upper right')
  ax.grid()
  ax.set_xlabel('Energy [eV]')
  ax.set_yscale('log')
  ax.set_xscale('log')
  try:
    f.savefig(filepath)
  except:
    pass
  plt.close(f)
  return

def make_mat(rows=120, cols=260, dtype=np.int, fill=0):
  mat = np.empty((rows, cols), dtype=dtype)
  mat.fill(fill)
  return mat

def get_sym_dict(filename):
  symbols = []
  Z = []
  with open(filename) as f:
    lines = f.readlines()
  for line in lines:
    Z.append(int(line.split(',')[0]))
    symbols.append(line.split(',')[1])
  sym_dict = dict(zip(symbols, Z))
  return sym_dict

def save_obj(obj, filename):
  with open(filename, 'wb') as f:
    pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(filename):
  with open(filename, 'rb') as f:
    return pickle.load(f)

def get_fig_ax():
  fig = plt.figure(figsize=(16,9))
  ax = fig.add_subplot(111)
  ax.xaxis.set_major_locator(ticker.MultipleLocator(10))
  ax.xaxis.set_minor_locator(ticker.MultipleLocator(2))
  ax.yaxis.set_major_locator(ticker.MultipleLocator(10))
  ax.yaxis.set_minor_locator(ticker.MultipleLocator(2))
  ax.set_xlabel("N")
  ax.set_ylabel("Z")
  ax.format_coord = matshow_nuclide_coords
  ax.grid(which='both')
  return fig, ax

def matshow_nuclide_coords(x, y):
  x = int(x+0.5)
  y = int(y+0.5)
  return 'Z={:d}, N={:d}, A={:d}'.format(y, x, x+y)

if __name__ == "__main__":
  main()



