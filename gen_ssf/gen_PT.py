#!/usr/bin/env python3

from subprocess import call
from tqdm import tqdm
import numpy as np
import multiprocessing
import os
import sys
import time
from nuclide import Nuclide
from utility import endf_nuclide_name, monitor_threaded_execution, coarsen_group, get_energies
import gather_nuclide_set

def main():
  if len(sys.argv) != 3:
    print('Please give temperature in K and ebins filepath, e.g.')
    exit('/Users/Fred/projects/ebins/ebins_280_erg5y2ac 294')
  bins_filepath = sys.argv[1]
  temp = int(sys.argv[2])
  run(bins_filepath, temp)
  return

def run(bins_filepath, nuclide_list, temp=294, parallel=True):
  endf_dir = '/Users/Fred/nd/endf/TENDL2015'
  endf_path_list = os.listdir(endf_dir)

  # new PTs go here...
  dest_probt_dir = '/Users/Fred/projects/SSF_study/probt_data'

  # TENDL2015 PT resonance range energy bounds here...
  pt_bounds_filepath = '/Users/Fred/projects/SSF_study/iso-mat-pte.tendl'

  # get fine structure
  uid = bins_filepath.split('/')[-1].split('_')[2]
  nbins = int(bins_filepath.split('/')[-1].split('_')[1])
  bins = get_energies(bins_filepath)
  assert nbins == len(bins) - 1

  # which nuclides can we make prob tables for?
  pt_nuclide_list = []
  print("Checking {} nuclides for PT generation parameters.".format(len(nuclide_list)))
  for nuclide in tqdm(nuclide_list):
    for endf_file in endf_path_list:
      if endf_nuclide_name(nuclide) == endf_file:
        pt_nuclide_list.append(nuclide)
        nuclide.endf_path = os.path.join(endf_dir, endf_file)
        res_start, res_end = get_energy_bounds(pt_bounds_filepath, nuclide.mat_num)
        nuclide.res_start = res_start
        nuclide.res_end = res_end
  print("Found {} candidate nuclides.".format(len(pt_nuclide_list)))
  
  parent_dir_path = os.getcwd()
  # check for and if necessary make directories (do this before threaded execution begins)
  group_dir = "tp-{:d}-{:d}_{:s}".format(nbins, temp, uid)
  # group path
  group_path = os.path.join(dest_probt_dir, group_dir)
  if not os.path.exists(group_path):
    os.makedirs(group_path)
  # log path
  log_path = os.path.join(dest_probt_dir, group_dir, 'log')
  if not os.path.exists(log_path):
    os.makedirs(log_path)
  if parallel:
    # multithreaded production of prob tables
    cpu_count = multiprocessing.cpu_count()
    print("Initiating multi-threaded generation of PTs on {} threads.".format(cpu_count))
  else:
    cpu_count = 1
    print("Initiating serial generation of PTs on {} thread.".format(cpu_count))
  pool = multiprocessing.Pool(processes=cpu_count, maxtasksperchild=1)
  arg_list = [(bins, nuclide, group_path, log_path, uid, parent_dir_path, temp) for nuclide in pt_nuclide_list]
  chunksize = int(np.ceil(len(arg_list)/(2*cpu_count)))
  # result updates during execution
  result = pool.map(threaded_make_probtable_wrapper, arg_list, chunksize)
  pool.close() # no more work to be added
#  monitor_threaded_execution(result)
  return

def threaded_make_probtable_wrapper(args):
  return make_probtable(*args)

def make_probtable(grp_arr, nuclide, group_path, log_path, uid, parent_dir_path, temp=294):
  # input variables used for naming and calendf inputs
  temp = int(np.round(temp))
  bin_count = len(grp_arr) - 1

  # path information
  probt_filename = '{nuclide}-{temp:<3}.tpe'.format(nuclide=nuclide.three_name, temp=temp)
  probt_path = os.path.join(group_path, probt_filename)
  # does the PT file already exist?
  if os.path.isfile(probt_path):
    #print("{} in {} groups already exists, skipping generation...".format(probt_filename, bin_count))
    return

  # make folder for working CALENDF files to be clustered by nuclide
  working_dir_path = os.path.join(parent_dir_path, nuclide.name)
  os.makedirs(working_dir_path)
  os.chdir(working_dir_path)

  # calendf wants the grp in descending order, and 0 <= E <= 2E7 eV
  if grp_arr[0] < grp_arr[-1]:
    grp_arr = grp_arr[::-1]
  calendf_threshold = 2E7
  # calendf needs the threshold value (less than or equal to) but wants it on the line above the main block
  trunc_grp_arr = grp_arr[grp_arr < calendf_threshold]
  #print("{} of {} bin bounds above {:1.3E}eV threshold, filtering".format(len(grp_arr)-len(trunc_grp_arr), len(grp_arr), calendf_threshold))
  trunc_bin_count = len(trunc_grp_arr)

  calendf_input_str = """CALENDF
  MODIFOPT LCORSCT .FALSE.
  MODIFOPT LPSTPOS .TRUE. 
  ENERGIES {e_start:1.4E} {e_end:1.4E}
  MAILLAGE GENE {bin_count:<4}
   1 Zones
   {trunc_bin_count:<4} -1 {calendf_threshold:1.6E}
{group_structure}
  TEFF  {temp:<3}
  NDIL 1
  1.0E+10
  NFEV  9  {mat_num}  '{endf_path}'
  SORTies
    NFSF   10 './{nuclide}-{temp:<3}.sf'
    NFSFTP 11 './{nuclide}-{temp:<3}.sft'
    NFTP   12 './{nuclide}-{temp:<3}.tp'
  IPRECI  4
  NIMP 0   80
REGROUTP
    NFTP 13 './{nuclide}-{temp:<3}.tp'
    NFTPR 17 './{nuclide}-{temp:<3}.tpr'
  NIMP 0  80
LECRITP
    MODIFOPT LPSPPOS .TRUE.  
    NFTPR  17 './{nuclide}-{temp:<3}.tpr'
    NFTPP  118 './{nuclide}-{temp:<3}.tpe'
  NIMP 0 80  
END"""

  group_structure = typewriter_return(trunc_grp_arr, precision=6).rstrip()
  with open('in{nuclide}-{temp:<3}'.format(nuclide=nuclide.three_name, temp=temp), 'w') as calendf_input:
    to_write = calendf_input_str.format(nuclide=nuclide.three_name, 
                                        bin_count=bin_count, 
                                        trunc_bin_count=trunc_bin_count, 
                                        group_structure=group_structure, 
                                        e_start=nuclide.res_start, 
                                        e_end=nuclide.res_end, 
                                        mat_num=nuclide.mat_num, 
                                        calendf_threshold=calendf_threshold, 
                                        temp=temp, 
                                        endf_path=nuclide.endf_path)
    calendf_input.write(to_write)

  call("xcalendf<in{nuclide}-{temp:<3} > out{nuclide}-{temp:<3}".format(nuclide=nuclide.three_name, temp=temp), shell=True)
  call("mv -f in{nuclide}-{temp:<3} {log_path}".format(nuclide=nuclide.three_name, temp=temp, log_path=log_path), shell=True)
  call("mv -f out{nuclide}-{temp:<3} {log_path}".format(nuclide=nuclide.three_name, temp=temp, log_path=log_path), shell=True)
  call("mv -f {nuclide}-{temp:<3}.tpe {group_path}".format(nuclide=nuclide.three_name, temp=temp, group_path=group_path), shell=True)
  call("rm {nuclide}-{temp:<3}.*".format(nuclide=nuclide.three_name, temp=temp), shell=True)
  os.chdir(parent_dir_path)
  os.rmdir(working_dir_path)
  return

def get_energy_bounds(filepath, mat_num):
  with open(filepath, 'r') as f:
    lines = f.readlines()
  for line in lines:
    if "{:d}".format(mat_num) == line.split()[1]:
      e_start = float(line.split()[2])
      e_end = float(line.split()[3])
  return e_start, e_end

def typewriter_return(array, precision=4, char_lim=79):
  # '1.<precision>E+01 '
  # '12<--------->34567'
  non_prec_chars = 7
  string = ''
  entries_per_line = np.floor(char_lim/(precision + non_prec_chars))
  for line in range(int(np.ceil(len(array)/entries_per_line))):
    line_arr = array[int(line*entries_per_line):int((line + 1)*entries_per_line)]
    line_str = ["{number:1.{precision}E}".format(number=number, precision=precision) for number in line_arr]
    concatenated_str = ' ' + ' '.join(line_str) + '\n'
    string += concatenated_str
  return string

if __name__ == "__main__":
  main()

