import time
import os
import numpy as np
from math import ceil, floor

def read_gendf(path, mf, mt):
  """sample PREPRO produced GENDF data, whitespace preserved 
 1.00000E-5 1903.09810 1.00200E-5 1901.28417 1.00390E-5 1899.470297443 3  1    4
 .047427000 28.1948592 .047520000 28.1672485 .047614000 28.13949017443 3  1 1438
 998030000. 2.80081600 1.00000E+9 0.0                             7443 3  1 5465
  """
  if mf != 3:
    exit("Please expand functionality of read_gendf, currently only reads MF=3")
  with open(path, 'r') as f:
    lines = f.readlines()
  mf_found = False
  for line_num, line in enumerate(lines):
    if (int(line[71]) == mf) and (mf_found == False):
      mf_start_line = line_num
      mf_found = True
    if (int(line[71]) != mf) and (mf_found == True):
      mf_end_line = line_num
      break
  mt_found = False
  mf_block = lines[mf_start_line:mf_end_line]
  for line_num, line in enumerate(mf_block):
    if (int(line[72:75]) == mt) and (mt_found == False):
      mt_start_line = line_num + 3
      mt_found = True
    if (int(line[72:75]) == mt) and (mt_found == True):
      mt_end_line = line_num + 1
  mt_block = mf_block[mt_start_line:mt_end_line]
  energy = []
  xs = []
  for line in mt_block:
    try:
      energy.append(float(line[1:11]))
      xs.append(float(line[12:22]))
    except:
      pass
    try:
      energy.append(float(line[23:33]))
      xs.append(float(line[34:44]))
    except:
      pass
    try:
      energy.append(float(line[45:55]))
      xs.append(float(line[56:66]))
    except:
      pass
  return energy, xs

def coarsen_group(fine, step=1):
  start = fine[0]
  end = fine[-1]
  coarse = fine[1:-1:step]
  coarse = np.insert(coarse, 0, start)
  coarse = np.append(coarse, end)
  return coarse

def make_mcnp_energies(arr, precision=6):
  """Remember, MCNP energies must be ascending and in MeV, not eV.
  Returns string for printing or writing to file.
  """
  if arr[0] > arr[1]: # descending, reverse arr
    arr = arr[::-1]
  if (arr[0] > 1E-7) or (arr[-1] > 2E3):
    print("Warning, input data likely in eV, not MeV")
    exit("Emin = {:1.2E} and Emax = {:1.2E}...".format(arr[0], arr[-1]))
  max_entry_length = precision + 7 # 1.PRECISIE+00_
  entries_per_line = floor(65/max_entry_length)
  mcnp_str = ''
  for line in range(ceil(len(arr)/entries_per_line)):
    line_str = ''
    for val in arr[line*entries_per_line:(line + 1)*entries_per_line]:
      line_str += "{1:1.{0}E} ".format(precision, val)
    line_str += '\n'
    if line == 0:
      mcnp_str += line_str
    else:
      mcnp_str += ' '*5 + line_str
  return mcnp_str

def write_fluxes(ebins_filepath, exponent, nbins):
  E = get_energies(ebins_filepath)
  phi = E**exponent
  with open('fluxes', 'w') as f:
    for val in phi[:-1]:
      f.write("    {:.5E}\n".format(val))
    f.write("  1.000\n")
    f.write("E^{:1.5f} spectrum in {}\n".format(exponent, nbins))
  return

def write_arb_fluxes(spectrum):
  """Must receive spectrum in energy descending order!"""
  with open('fluxes', 'w') as f:
    for val in spectrum:
      f.write("    {:.5E}\n".format(val))
    f.write("  1.000\n")
    f.write("Arb. spectrum in {}\n".format(len(spectrum)))
  return

def get_energies(filename):
  if not os.path.exists(filename):
    return None
  with open(filename, 'r') as f:
    lines = f.readlines()[2:]
  grp = []
  for line in lines:
    for val in line.split(','):
      try: 
        grp.append(float(val))
      except:
        continue
  return np.array(grp)

def monitor_threaded_execution(result):
  widget_str_list = ['|', '/', '-', '\\']
  while (True):
    if (result.ready()): 
      break
    for char in widget_str_list:
      remaining = result._number_left
      print("\rWaiting for", remaining, "chunks to complete... {}".format(char), end='')
      time.sleep(0.5)
  print("")
  return

def endf_nuclide_name(nuclide):
  mass = int(''.join(list(filter(lambda x: x.isnumeric(), nuclide.name))))
  mass_str = "{:03d}".format(mass)
  if nuclide.metastable == True:
    symbol = ''.join(list(filter(lambda x: x.isalpha(), nuclide.name[:-1])))
    endf_name = symbol + mass_str + 'm' + '-n.tendl'
  else:
    symbol = ''.join(list(filter(lambda x: x.isalpha(), nuclide.name)))
    endf_name = symbol + mass_str + '-n.tendl'
  return endf_name

def make_ebins_file(arr, ebins_path):
  """Takes bin bounds and makes FISPACT readable ebins file."""
  # make sure array is descending
  if arr[0] < arr[1]:
    arr = arr[::-1]

  nbins = len(arr) - 1
  if os.path.exists(ebins_path):
    return
  else:
    with open(ebins_path, 'w') as f:
      header_str = "'{} groups'\n".format(nbins)
      f.write(header_str)
      f.write('{:d}\n'.format(nbins))
      arr_str = ['{:1.5E}'.format(i) for i in arr]
      bins_str = ",\n".join(arr_str)
      f.write(bins_str)
  return

