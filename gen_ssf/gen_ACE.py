#!/usr/bin/env python3

import os
from tqdm import tqdm
from subprocess import call
import gather_nuclide_set
from nuclide import Nuclide
from gen_MG import endf_nuclide_name

def main():
  # broadening temperature
  temp=294
  # get nuclide list
  endfb7_gxs_dir = '/opt/FISPACT-II-3-20/ENDFdata/ENDFB71data/endfb71-n/gxs-709/'
  tendl_probt_dir = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tal2015-n/tp-709-294/'
  tendl_decay_index = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tendl15_decay12_index'
  nuclide_str_list, lines, mat_nums = gather_nuclide_set.get_list(endfb7_gxs_dir, tendl_decay_index, tendl_probt_dir)
  nuclide_list = [Nuclide(tup[0], tup[1]) for tup in zip(nuclide_str_list, mat_nums)]
  # get available endf file list
  endf_dir = '/Users/Fred/nd/endf/TENDL2015'
  endf_path_list = os.listdir(endf_dir)
  # process data
  for nuclide in tqdm(nuclide_list[:3]):
    for endf_file in endf_path_list:
      # look for nuclide's endf file (need to modify string)
      if endf_nuclide_name(nuclide) == endf_file:
        print(endf_file)
        nuclide.endf_path = os.path.join(endf_dir, endf_file)
        # make ace file
        ace_path = make_ace(nuclide, temp)
        break
  return

def make_ace(nuclide, temp):
  pendf_dir = '/Users/Fred/projects/SSF_study/pendf_data/{}K'.format(temp)
  pendf_path = os.path.join(pendf_dir, nuclide.name + 'p.asc')
  if not os.path.exists(pendf_path):
    print('PENDF file does not exist, please create...')
    return False

  ace_dir = '/Users/Fred/projects/SSF_study/ace_data/{}K'.format(temp)
  if not os.path.exists(ace_dir):
    print('Creating ACE data subdirectory for temperature={}K...'.format(temp))
    os.makedirs(ace_dir)
  ace_path = os.path.join(ace_dir, nuclide.name + '.ace')
  xsdir_path = os.path.join(ace_dir, nuclide.name + '.xsdir')
  plot_path = os.path.join(ace_dir, nuclide.name + '.eps')

  for path in [ace_path, xsdir_path, plot_path]:
    if os.path.exists(path):
      print('ACE file already exists, not overwriting...')
      return False

  njoy_input_filename = "input"
  njoy_input_str = """ acer
 20 21 0 31 32
 1 1 1 .99 0/
 '{nuclide_name} from tendl2015'/
 {mat_num:d} {temp:1.1f} /
 1 1
 /
 acer
 0 31 33 34 35 / file33 is plotting output
 7 1 2/ read type-1 file, make listing, write type-2 file (binary)
 '{nuclide_name} from tendl2015'/
 viewr
 33 36/ convert the acer plot output to postscript
 stop""".format(nuclide_name=nuclide.name, mat_num=nuclide.mat_num, temp=temp)

  # symlink ENDF and PENDF files
  call("ln -sf {} tape20".format(nuclide.endf_path), shell=True)
  call("ln -sf {} tape21".format(pendf_path), shell=True)
  # write njoy input to file
  with open(njoy_input_filename, 'w') as f:
    f.write(njoy_input_str)
  # run njoy acer and viewr
  call("njoy<{}".format(njoy_input_filename), shell=True)
  # move output file to pendf storage dir, name set above
  call("mv tape31 {}".format(ace_path), shell=True)
  call("mv tape32 {}".format(xsdir_path), shell=True)
  call("mv tape36 {}".format(plot_path), shell=True)
  # read xsdir file and replace 3rd word, 'filename' with contents of filename variable
  with open(xsdir_path, 'r') as f:
    line = f.readlines()[0]
  line_by_word = line.split()
  line_by_word[2] = ace_path.split('/')[-1]
  new_line = " ".join(line_by_word)
  with open(xsdir_path, 'w') as f:
    f.write(new_line)
  # delete generated files
  files = ['tape20', 'tape21', 'tape33', 'tape34', 'tape35', 'input', 'output']
  for filename in files:
    os.remove(filename)
  return ace_path

if __name__ == "__main__":
  main()

