#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import math
from lmfit import Model, Parameters, Minimizer, fit_report
from scipy.special import erf

def main():
  # read data
  dataset = np.genfromtxt('ssf_pdf.out')
  E = np.log10(dataset[:,0])
  freq = dataset[:,1]

  # do fit
  params = Parameters()
  params.add('amplitude', value=1)
  params.add('center', value=0)
  params.add('sigma', value=1)
  params.add('gamma', value=0)
  fitter = Minimizer(objective, params, fcn_args=(E, skewed_gaussian), fcn_kws={'data':freq})
  result = fitter.minimize('least_squares')
  fitted_params = [result.params[p].value for p in result.params]
  print(fit_report(result))

  # plot
  fig = plt.figure()
  ax = fig.add_subplot(111)
  fine_E = np.linspace(-5,9,1000)
  ax.step(E, freq, label='Summed SSF effect')
  lmskew_label_str = r'Skew normal fit'
  ax.plot(fine_E, objective(result.params, fine_E, skewed_gaussian), label=lmskew_label_str, ls='--', c='r') 
  ax.set_xlim(-3, 5)
  ax.set_xlabel(r'log$_{10}$(E)')
  ax.legend()
  ax.grid()
  plt.show()
  return fitted_params

def objective(params, x, func, data=None, errors=None):
  """Determines residuals (difference between model and data, weighted by errors.
  If errors not passed, don't weight. If data not passed, compute model.
  """
  parvals = params.valuesdict()
  A = parvals['amplitude']
  mu = parvals['center']
  sigma = parvals['sigma']
  gamma = parvals['gamma']
  model = func(x, A, mu, sigma, gamma)
  if data is None:
    return model
  if errors is None:
    return (model - data)
  return (model - data) / errors

def skewed_gaussian(x, amplitude=1.0, center=0.0, sigma=1.0, gamma=0.0, constant=0.0):
  """Return a Gaussian lineshape, skewed with error function.
  Equal to: gaussian(x, center, sigma)*(1+erf(beta*(x-center)))
  with beta = gamma/(sigma*sqrt(2))
  with  gamma < 0:  tail to low value of centroid
        gamma > 0:  tail to high value of centroid
  see http://en.wikipedia.org/wiki/Skew_normal_distribution
  """
  asym = 1 + erf(gamma*(x-center)/(np.sqrt(2)*sigma))
  return asym * gaussian(x, amplitude, center, sigma) + constant

def gaussian(x, amplitude=1.0, center=0.0, sigma=1.0):
  """Return a 1-dimensional Gaussian function.
  gaussian(x, amplitude, center, sigma) =
      (amplitude/(s2pi*sigma)) * exp(-(1.0*x-center)**2 / (2*sigma**2))
  """
  return (amplitude/(np.sqrt(2*np.pi)*sigma)) * np.exp(-(1.0*x-center)**2 / (2*sigma**2))

if __name__ == "__main__":
  main()

