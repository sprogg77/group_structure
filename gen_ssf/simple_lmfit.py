#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import math
import lmfit

def main():
  # read data
  dataset = np.genfromtxt('ssf_pdf.out')
  E = np.log10(dataset[:,0])
  freq = dataset[:,1]

  # do fit
  model = lmfit.models.SkewedGaussianModel()
  params = model.make_params(amplitude=1, center=0, sigma=1, gamma=0)
  #result = model.fit(freq, params, x=E, weights=np.random.random(len(E)))
  result = model.fit(freq, params, x=E, weights=None)
  print(lmfit.fit_report(result))

  # plot
  skew_label_str = r'Skewed Gaussian fit: '
  for param in result.params.keys():
    skew_label_str += "{}={:1.2f}, ".format(param, result.params[param].value)
  fig = plt.figure()
  ax = fig.add_subplot(111)
  fine_E = np.linspace(-5,9,1000)
  ax.step(E, freq, label='Summed SSF effect')
  ax.plot(E, result.best_fit, label=skew_label_str[:-2], ls='--', c='r') 
  ax.set_xlim(-3, 5)
  ax.set_xlabel(r'log$_{10}$(E)')
  ax.legend()
  ax.grid()
  plt.show()
  return

if __name__ == "__main__":
  main()

