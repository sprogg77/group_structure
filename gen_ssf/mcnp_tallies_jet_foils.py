#!/usr/bin/env python3

from utility import make_mcnp_energies
from utility import get_energies

paths = {175:'/opt/FISPACT-II-3-20/ENDFdata/ebins/ebins_175',
         280:'/Users/Fred/projects/SSF_study/ebins/ebins_280_c9c95cd6',
         315:'/opt/FISPACT-II-3-20/ENDFdata/ebins/ebins_315',
         650:'/Users/Fred/projects/SSF_study/ebins/ebins_650_43accaf8',
         709:'/opt/FISPACT-II-3-20/ENDFdata/ebins/ebins_709'}

header_strs =["""F4:N 15000 
FM4 0.000238 750 102
FC4 mt=102 in Fe-58""",
"""F14:N 15001 
FM14 0.090948 751 102
FC14 mt=102 in Co-59""",
"""F24:N 15002 
FM24 0.010347 752 102
FC24 mt=102 in Mo-95""",
"""F34:N 15003 
FM34 0.004970 753 102
FC34 mt=102 in Nd-146""",
"""F44:N 15004 
FM44 0.055548 754 102
FC44 mt=102 in Ta-181""",
"""F54:N 15005 
FM54 0.016883 755 102
FC54 mt=102 in W-182""",
"""F64:N 15005
FM64 0.017722 756 102
FC64 mt=102 in W-186"""]

# F524  tally_num
# F  4  <- constant
#  5    <- index of group structure
#   2   <- index of reaction

cell_volume=0.50893801

total_str=''
for i, header in enumerate(header_strs):
  print(header.split('\n')[-1].split(' ')[-1])
  first_line = header.split('\n')[0]
  second_line = header.split('\n')[1]
  third_line = header.split('\n')[2]
  comment_str = 'c ' + " ".join(third_line.split()[1:])
  base_tally_num = int(second_line.split()[0][2:])
  pw_tally_num = i * 10 + 4
  print(pw_tally_num)
  cell_num = int(first_line.split()[-1])
  c = 10
  for nbins in paths:
    tally_num = base_tally_num + (c * 10)
    print(tally_num)
    arr=get_energies(paths[nbins])
    arr/=1E6
    tally_str = 'c\n{:s}\nc {:s}\n'.format(comment_str,paths[nbins])
    tally_str += 'F{:d}:N {:d}\n'.format(tally_num, cell_num)
    tally_str += 'SD{:d} {:1.8f}\n'.format(tally_num, cell_volume)
    tally_str += 'E{:d}  '.format(tally_num)
    tally_str += make_mcnp_energies(arr)
    total_str += tally_str
    c+= 10

# print(total_str)




