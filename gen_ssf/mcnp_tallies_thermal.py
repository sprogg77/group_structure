#!/usr/bin/env python3

from utility import make_mcnp_energies
from utility import get_energies
import math
from collections import OrderedDict

def main():
  d = {64:'/Users/Fred/projects/SSF_study/ebins/ebins_64_converge',
       128:'/Users/Fred/projects/SSF_study/ebins/ebins_128_converge',
       175:'/Users/Fred/projects/SSF_study/ebins/ebins_175_xxxxxxxx',
       256:'/Users/Fred/projects/SSF_study/ebins/ebins_256_converge',
       315:'/Users/Fred/projects/SSF_study/ebins/ebins_315_xxxxxxxx',
       512:'/Users/Fred/projects/SSF_study/ebins/ebins_512_converge',
       709:'/Users/Fred/projects/SSF_study/ebins/ebins_709_xxxxxxxx',
       1024:'/Users/Fred/projects/SSF_study/ebins/ebins_1024_converge',
       2048:'/Users/Fred/projects/SSF_study/ebins/ebins_2048_converge',
       4096:'/Users/Fred/projects/SSF_study/ebins/ebins_4096_converge',
       8192:'/Users/Fred/projects/SSF_study/ebins/ebins_8192_converge',
       16384:'/Users/Fred/projects/SSF_study/ebins/ebins_16384_converge'}
  paths = OrderedDict(sorted(d.items(), key=lambda t: t[0]))
  cell_num = 2
  cell_volume = 1

  total_str=''
  for i, nbins in enumerate(paths):
    exponent = math.log(nbins) / math.log(2)
#    tally_num = int(exponent) * 10 + 4
    tally_num = i * 10 + 4
    arr = get_energies(paths[nbins])
    arr /= 1E6
    tally_str = 'c\nc {:s}\n'.format(paths[nbins])
    tally_str += 'F{:d}:N {:d}\n'.format(tally_num, cell_num)
    tally_str += 'SD{:d} {:1.4f}\n'.format(tally_num, cell_volume)
    tally_str += 'E{:d}  '.format(tally_num)
    tally_str += make_mcnp_energies(arr)
    total_str += tally_str

  print(total_str)
  return

if __name__== "__main__":
  main()
