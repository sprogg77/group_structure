#!/usr/bin/env python3

import os
import pickle
import re
import numpy as np
import matplotlib.pyplot as plt
from sys import argv

def main():
  nuclides = [('Fe', 58), 
              ('Co', 59), 
              ('Mo', 95), 
              ('Nd', 146), 
              ('Ta', 181), 
              ('W', 182), 
              ('W', 186)]

  mt_numbers = [102] * len(nuclides)

  group_structures = [175, 280, 315, 650, 709]

  pw_tally_dict, mg_tally_dict = make_tally_dicts(nuclides, group_structures)

  if len(argv) != 3:
    exit("Please give MCNP output and pickle file paths as arguments.")
  output_path = argv[1]
  pickle_filepath = argv[2]

  if os.path.exists(pickle_filepath):
    X = pickle.load(open(pickle_filepath, 'rb'))
  else:
    X = Simulation(output_path, nuclides, mt_numbers, pw_tally_dict, mg_tally_dict)
    pickle.dump(X, open(pickle_filepath, 'wb'))

  N = nuclides[-1]
  MT = 102
  print(X.reactions[(N, MT)].mg_tallies[1024].nps[-1]['tally'])

# for N in nuclides:
#   plot_foil_spectrum(X, N, MT)
# plt.show()
  return

def plot_foil_spectrum(X, N, MT):
  f,ax = plt.subplots(figsize=(6,4))
  for bin_count, data in X.reactions[(N, MT)].mg_tallies.items():
    E = data.nps[-1]['energy']
    phi = data.nps[-1]['tally']
    ax.step(E, phi/get_U(E), label='{:d}'.format(bin_count))
  ax.set_xscale('log')
  ax.set_yscale('log')
  ax.set_xlabel(r'$\mathrm{Energy}, E\ [eV]$')
  ax.set_ylabel(r'$\mathrm{Fluence}, \phi(E)\ [n \cdot n^{-1}_{src} \cdot cm^{-2} \cdot \Delta U^{-1}]$')
  nuclide_str = '-'.join([N[0], str(N[1])])
  handles, labels = ax.get_legend_handles_labels()
  # sort both labels and handles by labels
  labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
  ax.legend(handles, labels)
  ax.grid()
  ax.set_title('JET LTIS {:s} foil spectrum'.format(N[0]))
  plt.subplots_adjust(left=0.15,bottom=0.16,right=0.94)
  if N == ('W', 186):
    ax.set_title('JET LTIS {:s} foil spectrum'.format(N[0]))
    ax.set_xlim(1E-2, 1E8)
    ax.set_ylim(1E-9, 1E-4)
    f.savefig('w_spectrum.png', transparent=True)
    ax.set_title('Expanded view')
    ax.set_xlim(6.7, 110)
    ax.set_ylim(6.7E-10, 7E-7)
    f.savefig('w_spectrum_detail.png', transparent=True)
  return

def get_U(E):
  """Compute lethargy width for input energy group structure."""
  U = np.log10(E) - np.log10(np.roll(E, 1))
  U[0] = 0
  return U

class Simulation(object):
  def __init__(self, output_path, nuclides, mt_numbers, pw_tally_dict, mg_tally_dict):
    self.output_path = output_path
    self.reactions = {}
    for nuclide, mt_number in zip(nuclides, mt_numbers):
      self.reactions[(nuclide, mt_number)] = Reaction(self, nuclide, mt_number, pw_tally_dict[nuclide], mg_tally_dict[nuclide])

class Reaction(object):
  def __init__(self, simulation, nuclide, mt_number, pw_tally_num, mg_tally_nums):
    # if mg_tally_nums is a list, then reactions[N, MT].mg_tallies[nbins].nps[-1]
    # if mg_tally_nums is a dict, then reactions[N, MT].mg_tallies[(nbins,uid)].nps[-1]
    self.simulation = simulation
    self.nuclide = nuclide
    self.element = nuclide[0]
    self.atomic_mass = nuclide[1]
    self.mt_number = mt_number
    self.fispact_xs = {}
    self.pw_tally_num = pw_tally_num
    self.pw_tally = PW_Tally(self.simulation, self, pw_tally_num)
    self.mg_tallies = {}
    if type(mg_tally_nums) == dict:
      # tally num is key
      # points to tuple of nbins and uid respectively
      tally_nums = mg_tally_nums.keys()
      for tally_num in tally_nums:
        nbin = mg_tally_nums[tally_num][0]
        uid = mg_tally_nums[tally_num][1]
        tally = MG_Tally(self.simulation, self, tally_num)
        bin_count = tally.bin_count
        # use nbin and uid in one tuple, then a unique memory address and valid as a dictionary key, only nbin or only uid insufficient
        self.mg_tallies[(bin_count,uid)] = tally
        # tally.bin_count set from reading MCNP output file, check whether it matches nbin 
        if nbin != bin_count:
          print(nbin, bin_count)
          exit("... inside Reaction.__init__(), supplied nbin != MCNP output read bin_count")
    else:
      # mg_tally_nums is a list, procede without knowledge of uid or nbins... (original method)
      self.mg_tally_nums = mg_tally_nums
      for tally_num in mg_tally_nums:
        tally = MG_Tally(self.simulation, self, tally_num)
        bin_count = tally.bin_count
        self.mg_tallies[bin_count] = tally

class MG_Tally(Reaction):
  def __init__(self, simulation, reaction, tally_num):
    # tally_num, bin_count and dimension are constant functions of nps, so are stored here
    self.simulation = simulation
    self.reaction = reaction
    self.tally_num = tally_num
    self.bin_count = 0
    self.dimension = 0
    # all data within nps varies as a function of nps
    self.nps = self.get_data()

  def get_data(self, dimension_offset=6, data_offset=10):
    """Given a MG tally number and a filename, find all occurrences (all nps values) and compile a list
       of dictionary entries for energy, tally and error lists.
    """
    spectra_list = []
    start_pattern = "1tally\s+{}\s+nps\s=\s+\d+".format(self.tally_num)
    end_pattern = 'total'
    found_data = False
    with open(self.simulation.output_path, 'r') as file_object:
      lines = file_object.readlines()
      for line_num, line in enumerate(lines):
        if re.search(start_pattern, line):
          print("Found tally {} start line ({}) in {}".format(self.tally_num, line_num, self.simulation.output_path))
          start_line_num = line_num
          found_data = True
        if found_data and re.search(end_pattern, line):
          end_line_num = line_num
          found_data = False
          self.bin_count = end_line_num - start_line_num - data_offset - 1 # don't use first bin of tally
          self.dimension = float(lines[start_line_num + dimension_offset]), 
          spectra_list.append({'nps':float(lines[start_line_num].split()[-1]),
                               'tally_start_line':start_line_num,
                               'tally_end_line':end_line_num})
    for spectrum in spectra_list:
      data = np.genfromtxt(self.simulation.output_path, 
                           dtype='f8', 
                           skip_header=spectrum['tally_start_line'] + data_offset + 1, # don't use first bin of tally
                           max_rows=self.bin_count)
      spectrum['energy'] = data[:,0] * 1E6
      spectrum['tally'] = data[:,1]
      spectrum['rel_error'] = data[:,2]
    return spectra_list

class PW_Tally(object):
  def __init__(self, simulation, reaction, tally_num):
    # tally_num and dimension are constant functions of nps, so are stored here
    self.simulation = simulation
    self.reaction = reaction
    self.tally_num = tally_num
    self.dimension = 0
    # all data within nps varies as a function of nps
    self.nps = self.get_data()

  def get_data(self, dimension_offset=7, data_offset=10):
    """Given a PW tally number and a filename, find all occurrences (all nps values) and compile a list
       of dictionary entries for the tally and error.
    """
    results_list = []
    start_pattern = "1tally\s+{}\s+nps\s=\s+\d+".format(self.tally_num)
    with open(self.simulation.output_path, 'r') as file_object:
      lines = file_object.readlines()
      for line_num, line in enumerate(lines):
        if re.search(start_pattern, line):
          print("Found tally {} start line ({}) in {}".format(self.tally_num, line_num, self.simulation.output_path))
          start_line_num = line_num
          try:
            self.dimension = float(lines[start_line_num + dimension_offset]),
          except:
            pass
          data_line = lines[start_line_num + data_offset]
          results_list.append({'nps':float(lines[start_line_num].split()[-1]),
                               'tally_start_line':start_line_num,
                               'tally':float(data_line.split()[0]),
                               'rel_error':float(data_line.split()[1])})
    return results_list

def make_tally_dicts(nuclides, group_structures):
  """Generate the tally numbering scheme used in the August 2017 runs of the JET / LTIS model.
  """
  pw_tally_dict = {}
  mg_tally_dict = {}
  for i, nuclide in enumerate(nuclides):
    pw_tally_num = i * 10 + 4
    pw_tally_dict[nuclide] = pw_tally_num
    c = 10
    mg_tally_list = []
    for j in range(len(group_structures)):
      mg_tally_num = pw_tally_num + (c * 10)
      mg_tally_list.append(mg_tally_num)
      c+= 10
    mg_tally_dict[nuclide] = mg_tally_list
  return pw_tally_dict, mg_tally_dict  

if __name__ == "__main__":
  main()
