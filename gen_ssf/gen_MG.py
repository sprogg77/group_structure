#!/usr/bin/env python3

from subprocess import call, DEVNULL
from tqdm import tqdm
import numpy as np
import os
import gather_nuclide_set
from nuclide import Nuclide
from utility import make_ebins_file, get_energies, coarsen_group, endf_nuclide_name

def main():
  # make group structure
  nuclide_list = [Nuclide('W186', 7443)]
  for nbins in 2**np.arange(10,15):
    group = np.logspace(-5, 9, nbins + 1)
    uid = 'converge'
    ebins_path = '/Users/Fred/projects/SSF_study/ebins/ebins_{:d}_{:s}'.format(nbins, uid)
    make_ebins_file(group, ebins_path)
    run(ebins_path, nuclide_list, 294)
  return

def run(ebins_path, nuclide_list, temp=294):
  # nuclide_list is a list of Nuclide objects
  # get fine structure
  uid = ebins_path.split('/')[-1].split('_')[2]
  nbins = int(ebins_path.split('/')[-1].split('_')[1])
  group = get_energies(ebins_path)
  # check read bin length matches filename
  assert nbins == len(group) - 1

  # get available endf file list
  endf_dir = '/Users/Fred/nd/endf/TENDL2015'
  endf_path_list = os.listdir(endf_dir)

  print("Producing multi-group data in {} groups for {} nuclides...".format(group.shape[0] - 1, len(nuclide_list)))
  # check directories exist
  dest_dir = '/Users/Fred/projects/SSF_study/gendf_data/gxs-{:d}_{:s}'.format(len(group) - 1, uid)
  if not os.path.exists(dest_dir):
    os.makedirs(dest_dir)
  # UID not necessary for PENDF files, no group structure
  pendf_dir = '/Users/Fred/projects/SSF_study/pendf_data/{:d}K'.format(temp)
  if not os.path.exists(pendf_dir):
    os.makedirs(pendf_dir)

  # process data
  for nuclide in tqdm(nuclide_list):
    for endf_file in endf_path_list:
      if endf_nuclide_name(nuclide) == endf_file:
        nuclide.endf_path = os.path.join(endf_dir, endf_file)
    # make pointwise file, won't if already exists
    pendf_path = make_pendf(nuclide.name, nuclide.mat_num, nuclide.endf_path, pendf_dir, temp=temp)
    # make groupwise file, won't if already exists
    gendf_path = make_gendf(nuclide.name, pendf_path, dest_dir, group)
  return

def make_group(start, stop, bins_per_decade):
  nbins = bins_per_decade * (stop - start) + 1
  return np.logspace(start, stop, nbins)

def get_prepro_ebins(arr):
  val_per_line = 6
  string = ''
  word_count = 0
  for val in arr:
    string += " {:1.4E}".format(val)
    word_count += 1
    if word_count == 6:
      string += '\n'
      word_count = 0
  return string

def make_pendf(nuclide, mat, endf_path, pendf_dir, temp=293.6, sigma_zero=1E10):
  # output naming
  pendf_name = nuclide + 'p.asc'
  pendf_path = os.path.join(pendf_dir, pendf_name)
  # does this file already exist?
  if os.path.exists(pendf_path):
    return pendf_path
  else:
    pass
  # build njoy input
  njoy_input_filename = '{}.njoy'.format(nuclide)
  njoy_input_str = """reconr
20 21/        c1
'pendf tape for W186 from TENDL2015'/ c2
{mat:d} 2/       c3:mat, two comments below
.001 0/       c4:tolerance
'{nuclide} from TENDL2015'/
'processed with NJOY2016'/
0/
broadr
20 21 22/     c1
{mat:d}/         c2:mat
.001/         c3:tolerance
{temp:1.1f}/         c4:temp (K)
0/            c5
unresr
20 22 23/ c1: endf, pendfin, pendfout
{mat:d}/     c2: mat
{temp:1.1f}/    c3: temp (K)
{sigma_zero:1.1E}/    c4: sigmaz
0/
stop""".format(mat=mat, nuclide=nuclide, temp=temp, sigma_zero=sigma_zero)
  # write njoy input to file
  with open(njoy_input_filename, 'w') as f:
    f.write(njoy_input_str)
  # symlink ENDF file
  call("ln -sf {} tape20".format(endf_path), shell=True)
  # run njoy reconcr, broadr, unresr
  call("njoy<{}".format(njoy_input_filename), shell=True)
  # move output file to pendf storage dir, name set above
  call("mv tape23 {}".format(pendf_path), shell=True)
  # delete generated files
  os.remove("{}.njoy".format(nuclide))
  files = ['output', 'tape22', 'tape21', 'tape20']
  for filename in files:
    os.remove(filename)
  return pendf_path

def make_gendf(nuclide, pendf_path, dest_dir, ebins):
  # make sure array is ascending for PREPRO/GROUPIE
  if ebins[0] > ebins[1]:
    ebins = ebins[::-1]
  gendf_filename = nuclide + 'g.asc'
  gendf_path = os.path.join(dest_dir, gendf_filename)
  if os.path.exists(gendf_path):
    return gendf_path
  num_bins = len(ebins) - 1
  # symlink pendf file
  call("ln -sf {} GROUPIE.IN".format(pendf_path), shell=True)
  # build prepro input 
  prepro_input_str = """         0      {num_bins:>5d}         0          -1 1.00000-03           0   1
GROUPIE.IN
GROUPIE.OUT
          1          1         1           1          1
Groupie {num_bins} groups
     1 1  1  999999999
                        (blank line terminate request list)
{ebins_str}""".format(num_bins=num_bins, pendf_path=pendf_path, ebins_str=get_prepro_ebins(ebins))
  # write prepro input to file
  with open('GROUPIE.INP', 'w') as f:
    f.write(prepro_input_str)
  # run prepro groupie
  call("groupie<GROUPIE.INP", shell=True, stdout=DEVNULL, stderr=DEVNULL)
#  call("groupie<GROUPIE.INP", shell=True)
  # does output dir exist?
  if os.path.exists(dest_dir):
    pass
  else:
    print("Destination gxs directory does not exist, creating...")
    os.mkdir(dest_dir)
  # mv gendf to gxs dir
  call("mv GROUPIE.OUT {gendf_path}".format(gendf_path=gendf_path), shell=True)
  # delete generated files
  files = ['GROUPIE.LST','PLOTTAB.CUR','SHIELD.LST','UNSHIELD.LST','GROUPIE.IN','GROUPIE.INP']
  for filename in files:
    os.remove(filename)
  return gendf_path

if __name__ == "__main__":
  main()

