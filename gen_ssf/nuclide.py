class Nuclide(object):
  def __init__(self, name, mat_num):
    self.name = name
    if name[-1].lower() == 'm':
      self.element = ''.join(list(filter(lambda x: x.isalpha(), self.name[:-1])))
      self.metastable = True
    else:
      self.element = ''.join(list(filter(lambda x: x.isalpha(), self.name)))
      self.metastable = False
    self.mass = int(''.join(list(filter(lambda x: x.isnumeric(), self.name))))
    self.mat_num = mat_num
    if self.metastable == True:
      self.three_name = ''.join([self.element, '{:03d}'.format(self.mass), 'm'])
    else:
      self.three_name = ''.join([self.element, '{:03d}'.format(self.mass)])
