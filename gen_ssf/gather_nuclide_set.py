#!/usr/bin/env python3

from collections import OrderedDict
from glob import glob
import re
 
def main():
  endfb7_gxs_dir = '/opt/FISPACT-II-3-20/ENDFdata/ENDFB71data/endfb71-n/gxs-709/'
  tendl_probt_dir = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tal2015-n/tp-709-294/'
  tendl_decay_index = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tendl15_decay12_index'
  reduced_index_path = 'reduced_nuclide_index'

  nuclide_list, lines, mat_nums = get_list(endfb7_gxs_dir, tendl_decay_index, tendl_probt_dir)
  write_list(lines, reduced_index_path)

def get_list(endfb7_gxs_dir, master_index_path, probt_dir, desired_element_list=None):
  """Contruct list of nuclide name strings, list of lines (string) with zaids/other info and list of mat nums. Uses intersection of ENDFB7 transport library, stuff FISPACT 709 TENDL has PTs for and an input (optional list of elements).
  """
  # get list of nuclides ENDFB7 has transport data for
  # Fe56g.asc
  endfb7_paths_list = glob(endfb7_gxs_dir + '*.asc')
  endfb7_list = [filename.split('/')[-1].split('.')[0][:-1] for filename in endfb7_paths_list]

  # get list of nuclides with TENDL probability tables
  # Fe056-294.tpe
  probt_paths_list = glob(probt_dir+ '*.tpe')
  probt_list = []
  for filename in probt_paths_list:
    nuclide = filename.split('/')[-1].split('-')[0]
    nuclide, element = clean_nuclide_name(nuclide)
    probt_list.append(nuclide)

  # get list of nuclides in TENDL index
  # Fe056
  with open(master_index_path, 'r') as f:
    lines = f.readlines()
  # need to map from nuclide to TENDL index line
  tendl_dict = {}
  tendl_dict = OrderedDict(tendl_dict)
  for line in lines:
    nuclide = line.split()[0]
    nuclide, element = clean_nuclide_name(nuclide)
    tendl_dict[nuclide] = line

  # make a new copy of the tendl nuclide index, intersection of ENDF transport, prob table and TENDL
  intersection_index_lines = []
  intersection_nuclide_list = []
  if desired_element_list != None:
    for nuclide in tendl_dict.keys():
      _, element = clean_nuclide_name(nuclide)
      if (nuclide in endfb7_list) and (nuclide in probt_list) and (element in desired_element_list):
        intersection_index_lines.append(tendl_dict[nuclide])
        intersection_nuclide_list.append(nuclide)
  else:
    for nuclide in tendl_dict.keys():
      if (nuclide in endfb7_list) and (nuclide in probt_list):
        intersection_index_lines.append(tendl_dict[nuclide])
        intersection_nuclide_list.append(nuclide)

  # get list of mat nums
  mat_nums_list = [int(line.split()[-1]) for line in intersection_index_lines]

  # returning strings (nuclide names) are of ENDF form i.e. O16, W176
  return intersection_nuclide_list, intersection_index_lines, mat_nums_list

def clean_nuclide_name(nuclide):
  non_decimal = re.compile(r'[^\d]+')
  A = int(non_decimal.sub('', nuclide))
  if nuclide[-1] == 'm':
    element = remove_digits(nuclide[:-1])
    nuclide = "{}{}m".format(element, A)
  else:
    element = remove_digits(nuclide)
    nuclide = "{}{}".format(element, A)
  return nuclide, element

def write_list(lines, filename):
  with open(filename, 'w') as f:
    for line in lines:
      f.write(line)
  return

def remove_digits(string):
  return ''.join([i for i in string if not i.isdigit()])

if __name__ == "__main__":
  main()
