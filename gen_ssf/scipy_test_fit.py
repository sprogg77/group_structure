from scipy.optimize import leastsq

def skew_normal(vars, x):
  """Synthesis of normal PDF and CDF to calculate skew normal PDF, (see wikipedia).
     xi, location (real)
     omega, scale (positive, real)
     alpha, shape (real)"""
  xi = vars[0]
  omega = vars[1]
  alpha = vars[2]
  A = vars[3]
  return A / (np.sqrt(2*np.pi) * omega) * phi((x-xi)/omega) * Phi(alpha * ((x-xi)/omega))

def phi(x):
  """PDF of normal distribution."""
  return 1/np.sqrt(2*np.pi) * np.exp(-(x**2/2))
  
def Phi(x):
  """CDF of normal distribution."""
  return 0.5 * (1 + erf(x/np.sqrt(2)))

def erf(arr):
  """Convenience function, allows erf computation for vector, not just real."""
  return np.array([math.erf(x) for x in arr])

def GEV(vars, x):
  """Generalised Extreme Value (GEV) distribution
     xi = shape
     sigma = scale
     mu = location"""
  xi = vars[0]
  sigma = vars[1]
  mu = vars[2]
  A = vars[3]
  # GEV only valid for some subset of x reals if xi nonzero
  if xi > 0:
    x_min = mu - sigma / xi
    x_max = x.max()
  elif xi < 0:
    x_min = x.min()
    x_max = mu - sigma / xi
  else:
    x_min = x.min()
    x_max = x.max()
  # calculate t(x), form dependent on xi
  if xi != 0:
    x_valid_idx = np.where(np.logical_and(x >= x_min, x <= x_max))
    x_invalid_idx = np.where(np.logical_or(x < x_min, x > x_max))
    t = (1 + xi * (x - mu) / sigma)**(-1/xi)
  if xi == 0:
    t = np.exp(-(x - mu) / sigma)
  GEV = A/sigma * t**(xi + 1) * np.exp(-t)
  GEV[x_invalid_idx] = 0
  return GEV

def residuals(vars, func, x, data):
  """Returns array of differences between sample and model predicted, 
  required for scipy.optimize.leastsq"""
  model = func(vars, x)
  return (data-model)

def moving_average(arr, width):
  if arr.size < width:
    exit("Input vector needs to be bigger than window size.")
  if width < 3:
    return arr
  # np.r_[x, y, z] concatenates x, y and z
  # make array with additional elements at start and end, reflected
  arr_ext = np.r_[arr[width-1:0:-1], arr, arr[-1:-width:-1]]
  avg = []
  for i, val in enumerate(arr):
    i += width
    avg.append(arr_ext[i-width+1:i+width-1].sum() / (2 * width - 2))
  return np.array(avg)

