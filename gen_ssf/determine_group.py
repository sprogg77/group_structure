#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import plotly.plotly as py
import plotly 
import mimic_alpha as ma
import uuid
from sys import argv
from tqdm import tqdm
from scipy.integrate import quad
from scipy.interpolate import interp1d
from mpl_toolkits.mplot3d import Axes3D
from scipy.special import erf
from lmfit import Model, Parameters, Minimizer, fit_report
from advanced_lmfit import objective, skewed_gaussian, gaussian

plotly.tools.set_credentials_file(username='sprogg77', api_key='cClx0cW6nB9aAXc3HjXA')
np.set_printoptions(linewidth=140)

def main():
  if len(argv) < 4:
    print("Please give minimum bins per decade, path to SSF effect data, boolean (1 or 0) for fitting skewed Gaussian and fractional tolerance for bin area determination.")
    exit("15 ssf_pdf.out 1 0.001")
  bpd_min = int(argv[1])
  ssf_effect_data = argv[2]
  fitting = bool(int(argv[3]))
  if fitting:
    try:
      tolerance = float(argv[4])
    except:
      exit("Please specify a tolerance if fitting...")
  # limits of group structure
  E_min = 1E-5
  E_max = 1E9

  # read SSF effect data
  dataset = np.genfromtxt(ssf_effect_data)
  E = np.log10(dataset[:,0])
  freq = dataset[:,1]
  E = E[::-1]
  freq = freq[::-1]
  n_bins = len(E) - 1

  # number of decades
  d = np.log10(E_max) - np.log10(E_min)
  if bpd_min * d > n_bins:
    print("No excess bins available for increased resolution...")
    exit("{} * {} = {}, less than requested bins: {}".format(bpd_min, d, int(bpd_min * d), n_bins))

  # do fit
  if fitting:
    func = skewed_gaussian
    fitted_params, result = skewed_gaussian_fit(E, freq)
    bin_bounds, offset_params, A_ssf = sk_gauss_rebin(func, E_min, E_max, n_bins, d, bpd_min, *fitted_params, tol=tolerance, plot=True)
    background = offset_params[-1]
  # or work straight from discrete...
  else:
    bin_bounds, background, A_ssf = discrete_rebin(E, freq, n_bins, d, bpd_min, plot=True)

  # new group UID
  uid = str(uuid.uuid4()).split('-')[0] # generate new UID and corresponding path
  ebins_filepath = '/Users/Fred/projects/SSF_study/ebins/ebins_{:d}_{:s}'.format(n_bins, uid)

  # plot SSF effect, group density and groups
  f,ax = plt.subplots(figsize=(11,6))
  bin_lines = [([E, E], [0, max(freq + background) * 1.1]) for E in np.log10(bin_bounds)]
  pale_green = ma.colorAlpha_to_rgb('g', 0.4)[0]
  for i, line in enumerate(bin_lines):
    if i != 0:
      ax.plot(line[0], line[1], color=pale_green, linewidth=0.5)
    else:
      ax.plot(line[0], line[1], color=pale_green, linewidth=0.5, label="Equal area bin bounds")
  ax.step(E, 
    freq + background,
    label="SSF effect histogram")
  if fitting:
    params_str = "".join(["{:>14} = {: 1.2f}\n".format(p, result.params[p].value) for p in result.params])
    E_fine = np.linspace(-5,9,2000)
    ax.plot(E_fine, 
      skewed_gaussian(E_fine, *offset_params), 
      label="\nSkewed Gaussian fit:\n\n{:>14} = {: 1.2f}\n".format('const. offset', background) + params_str)
  ax.set_xlabel('log$_{10}$(E)')
  ax.set_ylabel(r'Bin density, $\rho(E)$')
  ax.set_title("Optimised {} bin group structure, minimum of {} bins/decade".format(n_bins, bpd_min))
  ax.legend(prop={'family':'monospace'})
  ax.grid()
  plt.show()
  return

def background_level(A_ssf, n_bins, d, bpd_min):
  """The SSF effect PDF zero for much of the desired group structure domain, we need a 'background'
  level of bin rho. This function calculates the appropriate y-axis offset given total bin
  count, number of decades and minimum bins per decade.
  """
  return A_ssf / ((n_bins / bpd_min) - d)

def discrete_rebin(y, rho, n_bins, d, bpd_min, plot=False):
  """y is input group structure with rho as density of SSF effect as a function of y. n_bins is the length of the desired group structure. d stands for the decades over which the group structure spans, bpd_min the minimum number of bins per decade. The function steps through the old group structure, looking to allocate area under a raised, discontinuous density function to a new group structure where every bin has an equal area. The new group structure, x is returned.
  """
  y_widths = y - np.roll(y, 1)
  y_widths[0] = 0
#  print("y", y)
#  print("y_widths", y_widths)
  A_ssf = np.sum(y_widths * rho)
  print("SSF effect integral of {}...".format(A_ssf))
#  print("A_ssf", A_ssf)
  background = A_ssf / ((n_bins / bpd_min) - d)
  print("Increasing bin density by {} to ensure minimum bins / decade of {}...".format(background, bpd_min))
  rho += background # now includes rho for thermal and fast regions
  A_y_bins = y_widths * rho
#  print("A_y_bins", A_y_bins)
  A_target = np.sum(A_y_bins) / n_bins
#  print("A_target", A_target)
  x = np.zeros_like(y) + y[-1]
  x[0] = y[0]
  assert len(x) == n_bins + 1
  A_y_bins_cum = np.cumsum(A_y_bins)
#  print("A_y_bins_cum", A_y_bins_cum)

  i = 1 # x counter
  j = 0 # y counter
  A_avail = 0
  check_same_bin = False
  print("Placing bins...")
  while i < n_bins: 
    if check_same_bin == False:
      A_avail += A_y_bins[j]
#    print("\ni", i)
#    print("j", j)
#    print("A_y_bins[{:d}] = {}".format(j, A_y_bins[j]))
#    print("A_avail", A_avail)
    if np.abs(A_avail / A_target - 1) < 1E-6:
#      print("A_avail == A_target... perfect match, increment i and j")
      x[i] = y[j]
#      print("x[{:d}] = {}".format(i, x[i]))
      A_avail = 0
      check_same_bin = False
      i += 1
      j += 1
      continue
    elif A_avail > A_target:
#      print("A_avail > A_target... allocating part of bin, increment i")
      check_same_bin = False
      x[i] = y[j] - (A_avail - A_target) / rho[j]
#      print("x[{:d}] = {}".format(i, x[i]))
#      print("A_avail now = {}".format(A_avail - A_target))
      A_avail = A_avail - A_target # excess available
      if A_avail > A_target:
#        print("... A_avail still big enough for another bin, go round without inc. j")
        check_same_bin = True
      else:
#        print("... A_avail too small for a second bin, inc. j")
        j += 1
      i += 1
      continue
    else:
#      print("A_avail < A_target... incrementing j")
      j += 1
      continue
      
#  print("New group...")
#  print("x", x)
  x_widths = x - np.roll(x, 1)
  x_widths[0] = 0
  print("New group widths...")
  print("x_widths", x_widths)

  # quality control
  assert len(x) == n_bins + 1
  try:
    assert 10**x.all() != 0
  except AssertionError:
    print(x)
    exit("Failed because at least one bound was a zero...")
  for i, val in enumerate(x[:-1]):
    if x[i+1] > x[i]:
      continue
    else:
      print("Warning, bin bounds not ascending in energy at index {}...".format(i))
      print("{} {} {}".format(x[i-1], x[i], x[i+1]))
      break
  else:
    print("Bin bounds all ascending.")
  print("Returning {} bin bounds for {} bins.".format(len(x), len(x) - 1))
  
  x = 10**x
  y = 10**y
  return x, background, A_ssf

def sk_gauss_rebin(func, E_min, E_max, n_bins, d, bpd, *params, tol=0.001, plot=False):
  """Calculates bin bound positions for energy group array by principle of equal area.
  Argument func is some description of bin density shape as a function of energy, limits
  and required bin count also given. Pass-through arguments to func are args.
  """
  print("Integrating SSF effect integral...")
  A_ssf = quad(func, np.log10(E_min), np.log10(E_max), args=params)[0] # integrate area of SSF distribution
  print("... {}".format(A_ssf))
  print("Calculating background offset for minimum bins per decade...")
  background = background_level(A_ssf, n_bins, d, bpd)
  print("... {}".format(background))
  params += (background,) # add background bin offset to parameter tuple
  print("Integrating offset SSF effect integral...")
  A_total = quad(func, np.log10(E_min), np.log10(E_max), args=params)[0] # integrate total area of bumped up skewed gaussian
  print("... {}".format(A_total))
  print("Corroborating with manual calculation...")
  print("... {}".format(background * d + A_ssf))
  max_iterations = 250
  data, PVs = [], []
  SP = A_total / n_bins # SP = set point i.e. target value for area
  print("Target area per bin of {}...".format(SP))
  logspaced_E_grid = np.logspace(np.log10(E_min), np.log10(E_max), 1E6)
  first_effect_nrg = 0.01 * 1.01
  last_effect_nrg = 1E5 * 0.99
  master = np.logspace(np.log10(E_min), np.log10(E_max), bpd * d + 1) # background bpd, insert extra
  lower = master[master<first_effect_nrg]
  higher = master[master>last_effect_nrg]
  middle = np.zeros(n_bins + 1 - len(lower) - len(higher))
  grp_arr = np.concatenate((lower, middle, higher))
  start_idx = min(np.where(grp_arr==0)[0]) # when should we jump in to place additional bins?
  i = start_idx
  print("Initial group structure...")
  print(grp_arr)
  print("Begin placing SSF effected bounds at index {}...".format(start_idx))

  while i < (len(lower) + len(middle)): # bin by bin
    Kp = 1.00
    Ki = 0.25 
    Kd = 0.20
    Rs, Es, Ps, Is, Ds, Us = [], [], [], [], [], [] # initialise lists to track control parameters
    U = 1.1 * grp_arr[i-1] # U is control variable, bin bound to vary, initial guess is last confirmed bound and a bit
    oscillating = False # not stuck in oscillation
    for iteration in range(max_iterations): # iterations to find right bin bound
      PV = quad(func, np.log10(grp_arr[i-1]), np.log10(U), args=params)[0]
      R = SP / PV
      E = SP - PV
      Rs.append(R)
      Es.append(E)
      try:
        dEsdi = np.gradient(Es) # derivative of error with respect to iteration
        int_E = np.sum(Es[-2:])
      except: # exception for start when gradient is incalculable
        dEsdi = [0]
        int_E = 0
      if (1 - tol <= R <= 1 + tol): # bound found to desired tol, move to next bin
        data.append(dict(type='scatter3d',mode='lines',x=np.array(range(len(Rs))),y=np.ones_like(Rs) * i,z=Rs,name='',line=dict(width=4),))
        PVs.append(PV)
        break
      else: # revise bin bound (U)
        P = Kp * E
        I = Ki * int_E
        D = Kd * dEsdi[-1]
        U += grp_arr[i-1] * (P + I + D) # previous bound required to get in the right order of magnitude, perhaps logging input data would also work
        Ps.append(P)
        Is.append(I)
        Ds.append(D)
        if iteration > 10:
          if (1 - 1E-6) <= np.abs(Ds[-1] / Ds[-2]) <= (1 + 1E-6) and not oscillating:
            print("Found oscillation...")
            oscillating = True
            for idx in [-4, -3, -2, -1]:
              print(Ds[idx])
            print("Disturbing control parameter U with random number...")
            U += grp_arr[i] * 10 * (np.random.random() - 0.5) # can we disturb it? using random value in range [-10,10)
            print("Reducing proportional control gain...")
            print("Disabling integral and derivative control gain...")
            Kp /= 5
            Ki = 0
            Kd = 0
      Us.append(U)
      print("SP:{: 1.7E}, PV:{: 1.7E}, E:{: 1.8f}, U:{: 1.7E}, P:{: 1.2E}, I:{: 1.3E}, D:{: 1.2E}, bin:{:5d}, iteration:{:4d}".format(SP, PV, E, U, P, I, D, i, iteration))
    else: 
      print('Taking too long ({} iterations) on bin {} with energy {:1.3E} eV, moving on...'.format(iteration, i, grp_arr[i-1]))      
      plot_control(Us, Ps, Is, Ds, i)
      data.append(dict(type='scatter3d',mode='lines',x=np.array(range(len(Rs))),y=np.ones_like(Rs) * i,z=Rs,name='',line=dict(width=4),))
      PVs.append(PV)
      U = (Us[-1] + Us[-2]) / 2
      Us.append(U)
    grp_arr[i] = U # actually add U (control variable) as energy to array of bounds
    print(grp_arr) # refresh grp_arr
    i += 1
  PVs = np.array(PVs)
  if plot:
    plot_convergence(data)
    plot_bin_areas(PVs, SP)
    plot_bin_width(grp_arr)

  # quality control
  assert len(grp_arr) == n_bins + 1
  assert grp_arr.all() != 0
  for i, val in enumerate(grp_arr[:-1]):
    if grp_arr[i+1] > grp_arr[i]:
      continue
    else:
      print("Warning, bin bounds not ascending in energy at index {}...".format(i))
      print("{} {} {}".format(grp_arr[i-1], grp_arr[i], grp_arr[i+1]))
      break
  else:
    print("Bin bounds all ascending.")
  print("Returning {} bin bounds for {} bins.".format(len(grp_arr), len(grp_arr) - 1))
  return grp_arr, params, A_ssf

def skewed_gaussian_fit(E, freq):
  # do fit
  params = Parameters()
  params.add('amplitude', value=1)
  params.add('center', value=0)
  params.add('sigma', value=1)
  params.add('gamma', value=0)
  # remember that the 'objective' function explicitly uses skewed_gaussian, not general!
  fitter = Minimizer(objective, params, fcn_args=(E, skewed_gaussian), fcn_kws={'data':freq})
  result = fitter.minimize('least_squares')
  return [result.params[p].value for p in result.params], result

def plot_bin_width(arr):
  i,ex = plt.subplots()
  ex.scatter(np.arange(len(arr)), arr, s=12)
  ex.set_yscale('log')
  ex.set_ylabel('Energy (eV)')
  ex.set_ylim(1E-6, 1E10)
  ex.set_xlabel('Bin #')
  ex.grid()

def plot_convergence(data):
  layout = dict(
    title='Convergence of bin bounds',
    showlegend=False,
    scene=dict(
      xaxis=dict(title='Iteration'),
      yaxis=dict(title='Bin number'),
      zaxis=dict(title='Guess / target energy'),
      camera=dict(eye=dict(x=-1.7, y=-1.7, z=0.5))))
  fig = dict(data=data, layout=layout)
  py.plot(fig, filename='bound_calc_convergence', auto_open=False) # send to web
  return

def plot_control(Us, Ps, Is, Ds, i):
  h,cx = plt.subplots()
  steps = np.arange(len(Us))
  cx.scatter(steps, Us, label="U")
  cx.scatter(steps, Ps, label="P")
  cx.scatter(steps, Is, label="I")
  cx.scatter(steps, Ds, label="D")
  cx.set_title("Failed to converge for bin {}".format(i))
  cx.set_yscale('log')
  cx.set_ylim(1E-20, 1E10)
  cx.set_xlabel('Iteration')
  cx.set_ylabel('Magnitude')
  cx.grid()
  cx.legend()
  return

def plot_bin_areas(PVs, SP):
  f,ax=plt.subplots()
  PVs_error = (PVs/SP) * 100 - 100
  ax.hist(PVs_error, color='g')
  ax.axvline(0, linestyle='dashed', color='r')
  ax.set_title('Distribution of bin areas')
  ax.set_xlabel('% change on target bin area')
  ax.set_ylabel('Frequency')
  ax.grid()
  g,bx=plt.subplots()
  bx.scatter(np.arange(len(PVs)), PVs_error)
  bx.set_title('Area error by bin')
  bx.set_xlabel('Bin number')
  bx.set_ylabel('% error on target area')
  bx.grid()
  return

if __name__ == "__main__":
  main()



