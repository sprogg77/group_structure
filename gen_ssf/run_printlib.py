#!/usr/bin/env python3

from sys import argv
import numpy as np
import os
from subprocess import call, DEVNULL
from datetime import datetime
import pickle
import re
import shutil
import matplotlib.pyplot as plt
import gen_SSF
import utility
from nuclide import Nuclide
from parse_mcnp_output import Simulation, Reaction, PW_Tally, MG_Tally
import gen_PT
import gen_MG

def main():
  if len(argv) != 2:
    exit("Please give pickled results file as argument.")
  pickle_filepath = argv[1]
  if os.path.exists(pickle_filepath):
    X = pickle.load(open(pickle_filepath, 'rb'))

  nuclides = [('Fe', 58), 
              ('Co', 59), 
              ('Mo', 95), 
              ('Nd', 146), 
              ('Ta', 181), 
              ('W', 182), 
              ('W', 186)]
  mat_nums = [2637,
              2725,
              4234,
              6037,
              7328,
              7431,
              7443]
  atomic_densities = {('Fe', 58):0.000237755,
                      ('Co', 59):0.090947879,
                      ('Mo', 95):0.010346976,
                      ('Nd', 146):0.004970198,
                      ('Ta', 181):0.055547699,
                      ('W', 182):0.016883539,
                      ('W', 186):0.017722989}
  group_structures= {175:'/Users/Fred/projects/SSF_study/ebins/ebins_175_xxxxxxxx',
                     280:'/Users/Fred/projects/SSF_study/ebins/ebins_280_c9c95cd6',
                     315:'/Users/Fred/projects/SSF_study/ebins/ebins_315_xxxxxxxx',
                     650:'/Users/Fred/projects/SSF_study/ebins/ebins_650_43accaf8',
                     709:'/Users/Fred/projects/SSF_study/ebins/ebins_709_xxxxxxxx'}
  MT = 102
  temp = 294
  time_str = "_".join(str(datetime.now()).split())
  time_str = time_str.replace(":", "-")

  for N, mat_num in zip(nuclides, mat_nums):
    X.reactions[(N, MT)].unshielded = {}
    X.reactions[(N, MT)].shielded = {}
    nuclide_name_str = "".join([N[0], str(N[1])])
    print("\n\nCalculating MT={:d} reaction rate for {:s}".format(MT, nuclide_name_str))

    for nbins, ebins_path in group_structures.items():

      # DIAGNOSTIC
#      nbins = 709
#      ebins_path = group_structures[nbins]
      # DIAGNOSTIC

      print("\n\n... {:d} groups...".format(nbins))

      uid = ebins_path.split('/')[-1].split('_')[-1]

      # get spectrum, is spectrum in descending energy order?
      print('Reading energy and spectral data from MCNP pickle...')
      E = X.reactions[(N, MT)].mg_tallies[nbins].nps[-1]['energy']
      phi = X.reactions[(N, MT)].mg_tallies[nbins].nps[-1]['tally']
      ebins_E = utility.get_energies(ebins_path)

      if E[0] < E[1]:
        print('Reversed energy and spectral arrays, need to be in descending energy order...')
        E = E[::-1]
        phi = phi[::-1]

#      print('parsed from tally (paired with read phi):', E[0:2], E[-3:-1])
#      print('read from ebins (used for making ptable): ', ebins_E[0:2], ebins_E[-3:-1])

      # make fluxes file 
      print('Generating FISPACT fluxes file...')
      utility.write_arb_fluxes(phi)

      # various path stuff
      ssf_data_dir = '/Users/Fred/projects/SSF_study/ssf_data/'
      archive_path = os.path.join(ssf_data_dir, "{:d}_{:s}".format(nbins, uid), "arb-{:s}".format(time_str))
      input_path = os.path.join(archive_path, 'input')
      output_path = os.path.join(archive_path, 'output')
      log_path = os.path.join(archive_path, 'log')
      for path in [archive_path, input_path, output_path, log_path]:
        if not os.path.exists(path):
          os.makedirs(path)

      # make ND if necessary
      # probt data
      nuclide_obj_list = [Nuclide(nuclide_name_str, mat_num)] # always a singleton
      nuclide = nuclide_obj_list[0]
      probt_dir = '/Users/Fred/projects/SSF_study/probt_data/tp-{:d}-{:d}_{:s}'.format(nbins, temp, uid)
      if nbins == 709:
        probt_dir = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tal2015-n/tp-709-294/'
      probt_filename = "{nuclide}-{temp:<3}.tpe".format(nuclide=nuclide.three_name, temp=temp)
      probt_path = os.path.join(probt_dir, probt_filename)
      if not os.path.exists(probt_path):
        gen_PT.run(ebins_path, nuclide_obj_list, temp)
        if not os.path.exists(probt_path):
          exit("Probability table data wasn't available, tried to create it and failed...")

      # MG xs data
      gendf_dir = '/Users/Fred/projects/SSF_study/gendf_data/gxs-{:d}_{:s}'.format(nbins, uid)
      gendf_filename = nuclide.name + 'g.asc'
      if nbins == 709:
        gendf_dir = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tal2015-n/gxs-709/'
        gendf_filename = nuclide.three_name + 'g.asc'
      gendf_path = os.path.join(gendf_dir, gendf_filename)
      if not os.path.exists(gendf_path):
        gen_MG.run(ebins_path, nuclide_obj_list, temp)
        if not os.path.exists(gendf_path):
          exit("GENDF data wasn't available, tried to create it and failed...")


      # make symlinks for ND
      print('Symlinking MG and PT data into working directory...')
      init_path = os.getcwd()
      gendf_symlink = 'gxs-{:d}'.format(nbins)
      probt_symlink = 'tp-{:d}-{:d}'.format(nbins, temp)
      call('ln -s {:s} {:s}'.format(gendf_dir, gendf_symlink), shell=True)
      call('ln -s {:s} {:s}'.format(probt_dir, probt_symlink), shell=True)

      # make files file
      print('Generating FISPACT files file...')
      files_filename = 'files'
      gen_SSF.make_files_file(ebins_path, gendf_symlink, probt_symlink, files_filename)

      # make nuclide specific working directory
      working_dir_path = os.path.join(init_path, nuclide_name_str)
      os.makedirs(working_dir_path)

      # make symlinks for the common input files 
      for filename in [files_filename, 'fluxes', 'reduced_nuclide_index']:
        call("ln -s {} {}".format(os.path.join(init_path, filename), 
                                  os.path.join(working_dir_path, filename)), 
                                  shell=True)
      os.chdir(working_dir_path)

      # make FISPACT input file & run FISPACT printlib
      print('Running FISPACT...')
      output_file_path = run_FISPACT(N, files_filename, input_path, output_path, log_path, nbins)

      # delete MG and PT symlinks
      print('Removing simulation files, working folder and symlinks...')
      os.chdir(init_path)
      shutil.rmtree(nuclide_name_str)
      os.remove(gendf_symlink)
      os.remove(probt_symlink)
      
      # read collapsed cross-section
      X.reactions[(N, MT)].unshielded[nbins], X.reactions[(N, MT)].shielded[nbins] = get_collapsed_xs(N, output_file_path)

    # plot comparison of MCNP PW RR, MG SH, MG UNSH as function of group structure
    plot_xs_group_structure(X, N, MT, atomic_densities)

  plt.show()
  return

def plot_xs_group_structure(X, N, MT, atomic_densities):
  f,ax = plt.subplots(figsize=(4.5,3.7))
  RR = X.reactions[N, MT].pw_tally.nps[-1]['tally']
  phi = X.reactions[N, MT].mg_tallies[650].nps[-1]['tally'].sum()
  density = atomic_densities[N]
  pw_sigma = RR/(density*phi)
  tuple_list = []
  for bin_count in X.reactions[(N, MT)].shielded.keys():
    tuple_list.append((bin_count, 
                       X.reactions[(N, MT)].shielded[bin_count], 
                       X.reactions[(N, MT)].unshielded[bin_count])) 
  def unsh_sort(x):
    return np.abs(x - pw_sigma)
  tuple_list = sorted(tuple_list, key=lambda triplet: unsh_sort(triplet[2]), reverse=True)
  bin_count_list = [x[0] for x in tuple_list]
  shielded_xs_list = [x[1] for x in tuple_list]
  unshielded_xs_list = [x[2] for x in tuple_list]
  shielded_xs_percent_list = 100 * (shielded_xs_list - pw_sigma) / pw_sigma
  unshielded_xs_percent_list = 100 * (unshielded_xs_list - pw_sigma) / pw_sigma
  width = 0.2
  ax.set_xticks(np.arange(len(bin_count_list)) + width / 2)
  ax.set_xticklabels(bin_count_list, rotation='horizontal', fontsize=12)
  ax.set_xlabel("Group structure")
  ax.set_ylabel(r"$\%\Delta$ on pointwise")
  minmin = min(min(shielded_xs_percent_list), min(unshielded_xs_percent_list))
  maxmax = max(max(shielded_xs_percent_list), max(unshielded_xs_percent_list))
  span = maxmax - minmin
  gap = 0.05 * span 
  ymin = minmin - gap
  ymax = maxmax + gap
  ax.set_ylim(ymin, ymax)
  bx = ax.twinx()
  aymin = (ymin * pw_sigma) / 100 + pw_sigma
  aymax = (ymax * pw_sigma) / 100 + pw_sigma
  bx.set_ylim(aymin, aymax)
  bx.set_ylabel('Collapsed cross-section, $\sigma_{n,\gamma}$ [barns]')
  nuclide_str = '-'.join([N[0], str(N[1])])
  ax.set_title('{:s} collapsed cross-sections'.format(nuclide_str))
  rects_ush = ax.bar(np.arange(len(bin_count_list)) + width, unshielded_xs_percent_list, width=width, color='red', label="Unshielded")
  rects_sh = ax.bar(np.arange(len(bin_count_list)), shielded_xs_percent_list, width=width, color='blue', label="Shielded")
  ax.axhline(0, color='black', linewidth=1)
  handles, labels = ax.get_legend_handles_labels()
  ax.legend()
  plt.tight_layout()
  plt.subplots_adjust(left=0.17,right=0.83)
  f.savefig(nuclide_str + '.png', transparent=True)
  f.savefig(nuclide_str + '.eps')
  return

def get_collapsed_xs_table(N, output_path):
  """Looking for a string like the following, in one of two columns of text
     Fe 58  (n,g    )  Fe 59   4.51264E-02+- 0.00000E+00
     Returns collapsed (n,g) cross-section for specified nuclide"""
  pattern = "{:2s}{:3d}  (n,g".format(N[0], N[1])
  with open(output_path, 'r') as f:
    lines = f.readlines()
  for line in lines:
    if line[4:15] == pattern: # first column
      print(line, float(line[30:41]))
      return float(line[30:41])
    elif line[65:76] == pattern: # second column
      print(line, float(line[91:102]))
      return float(line[91:102])
    else: # no match
      continue 

def get_collapsed_xs(N, output_path):
  """Looking for a string like the following, 
     Co 59      Co 60       102    101    8.08817E-01    6.67718E-01         82.55
     Returns collapsed (n,g) cross-section for specified nuclide, unshielded and shielded"""
  element = N[0]
  initial_mass = N[1]
  capture_mass = initial_mass + 1
  pattern = r"{:2s}{:3d}\s+{:2s}{:3d}\s+102".format(element, initial_mass, element, capture_mass)
  with open(output_path, 'r') as f:
    lines = f.readlines()
  for line in lines:
    if re.search(pattern, line):
      return float(line[39:50]), float(line[54:65])

def run_FISPACT(nuclide, fispact_files_filename, input_path, output_path, log_path, nbins):
  """Function for main loop (over nuclides) to call. Needed as a seperate function for threading.
  """
  element = nuclide[0]
  nuclide_str = "".join([nuclide[0], str(nuclide[1])])
  input_filename = nuclide_str + '.i'
  output_filename = nuclide_str + '.out'
  if output_filename in os.listdir(output_path):
    return
  fispact_input_str = """<< -----collapse cross section data----- >>
CLOBBER
GETXS 1 {ebins:d}
SSFCHOOSE 1 1
  {nuclide}
SSFMASS 0.001 1
  {element} 100.0
PROBTABLE 1 1
FISPACT
* COLLAPSE tal2015-n/gxs-{nbins} with tp-{nbins}-294
PRINTLIB 4
END
* END OF RUN"""
  with open(input_filename, 'w') as f:
    if nbins == 709:
      ebins = 709
      fispact_exec = 'fispact_std'
    else:
      ebins = 1
      fispact_exec = 'fispact'
    f.write(fispact_input_str.format(ebins=ebins, nbins=nbins, nuclide=nuclide_str, element=element))
  # run simulation, suppressing output
  call('{} {} {}'.format(fispact_exec, input_filename[:-2], fispact_files_filename), shell=True, stdout=DEVNULL, stderr=DEVNULL)
  # using subprocess.call for mv commands was failing, shutil seems to work
  shutil.move("{}.i".format(nuclide_str), input_path)
  shutil.move("{}.out".format(nuclide_str), output_path)
  shutil.move("{}.log".format(nuclide_str), log_path)
  return os.path.join(output_path, output_filename)

if __name__ == "__main__":
  main()
