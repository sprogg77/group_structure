#!/usr/bin/env python3

import utility
import gen_SSF
import read_output
import gather_nuclide_set
import gen_MG
import gen_PT
import workflow
import run_printlib
from nuclide import Nuclide
from parse_mcnp_output import Simulation, Reaction, MG_Tally, PW_Tally
import os
import re
import pickle
import math
import uuid
import numpy as np
from tqdm import tqdm
from fill_between_steps import fill_between_steps
from collections import OrderedDict
import matplotlib.pyplot as plt
import mimic_alpha as ma
from subprocess import call
from datetime import datetime
from sys import argv

def main():
  # problem specifics
  N = ('W', 186)
  MT = 102
  temp = 294

  # mcnp tally gubbins
  cell_num = 4
  cell_vol = 1
  pw_tally_num = 994
  tally_str_filename = "conv_tally_str.txt"
  pregenerated_groups_filepath = 'conv_comp_groups.pkl'
  if os.path.exists(tally_str_filename):
    os.remove(tally_str_filename)

  # MCNP output location
  if len(argv) != 3:
    exit("Please give MCNP output and pickle file paths as arguments.")
  output_path = argv[1]
  pickle_filepath = argv[2]

  # bin generation parameters
  fit = False
  elements_list = ['W']
  spectrum_slope = 0
  E_min = 1E-5
  E_max = 1E8
  energy_decades = int(np.log10(E_max) - np.log10(E_min) + 1)
  start = 6
  stop = 11
  nums_dict = {}
  uids_dict = {}
  nbins_dict = {}
  bin_decades = stop - start + 1

  ##############
  # PLOT THAT! #
  ##############
  collapsed_data_filepath = 'collapsed_data.pkl'
  if os.path.exists(collapsed_data_filepath):
    data, pw_xs = pickle.load(open(collapsed_data_filepath, 'rb'))
    plot_xs_group_structure(N, MT, data, pw_xs)
    return

  #################
  # FLAT SPECTRUM #
  #################

  if os.path.exists(pregenerated_groups_filepath):
    # don't recreate groups and MCNP tally strings, instead load from file
    nums_dict, uids_dict, nbins_dict, onums_dict = pickle.load(open(pregenerated_groups_filepath, 'rb'))
  else:
    # create groups and MCNP tally strings
    for nbins in np.logspace(start, stop, num=bin_decades, base=2):
      nbins = int(nbins)

      # LOGSPACED GROUP
      bins = np.logspace(np.log10(E_min), np.log10(E_max), nbins + 1)
      uid = 'tunglogs'
      ebins_filepath = '/Users/Fred/projects/SSF_study/ebins/ebins_{:d}_{:s}'.format(nbins, uid)
      # will NOT overwrite if file already exists
      utility.make_ebins_file(bins, ebins_filepath) # record these new bins for next iteration
      tally_num, tally_str = gen_mcnp_tally(ebins_filepath, cell_num, cell_vol, optimised=0)
      nums_dict[ebins_filepath] = tally_num
      uids_dict[ebins_filepath] = uid
      nbins_dict[ebins_filepath] = nbins
      with open(tally_str_filename, 'a') as f:
        f.write(tally_str)

      # OPTIMISED GROUP
      bins, ssf_effect, archive_path, uid = workflow.run(spectrum_slope, temp, ebins_filepath, elements_list) 
      assert nbins == len(bins) - 1
      bpd_min = min([nbins / (2 * energy_decades), 50])
      bins, ebins_filepath, bounds_plot_path, A_ssf = workflow.optimise_group(bins, ssf_effect, nbins, archive_path, bpd_min, uid, fit=fit, E_min=E_min, E_max=E_max, new_uid='tungopti') # returns NEW reassigning bins
      utility.make_ebins_file(bins, ebins_filepath) # record these new bins for next iteration
      tally_num, tally_str = gen_mcnp_tally(ebins_filepath, cell_num, cell_vol, optimised=1)
      uid = 'tungopti'
      nums_dict[ebins_filepath] = tally_num
      uids_dict[ebins_filepath] = uid
      nbins_dict[ebins_filepath] = nbins
    onums_dict = OrderedDict(sorted(nums_dict.items(), key=lambda t: t[1]))
    with open(tally_str_filename, 'a') as f:
      f.write(tally_str)
    if not os.path.exists(pregenerated_groups_filepath):
      pickle.dump([nums_dict, uids_dict, nbins_dict, onums_dict], open(pregenerated_groups_filepath, 'wb'))

  ################
  # READ SPECTRA #
  ################
  nuclides = [N]
  mt_numbers = [MT] * len(nuclides)
  # only works for one nuclide
  pw_tally_dict = {nuclides[0]: pw_tally_num}
  # new Reaction class (16/01/17) takes a dict of tuples
  # mg_tally_dict nuclide key points to tally_num key points to tuple of 0) nbin & 1) uid
  this_is_ugly = {tally_num:(nbins_dict[path], uids_dict[path]) for path, tally_num in onums_dict.items()}
  # only works for one nuclide
  mg_tally_dict = {nuclides[0]: this_is_ugly}
  if os.path.exists(pickle_filepath):
    X = pickle.load(open(pickle_filepath, 'rb'))
  else:
    X = Simulation(output_path, nuclides, mt_numbers, pw_tally_dict, mg_tally_dict)
    pickle.dump(X, open(pickle_filepath, 'wb'))

  #################
  # REAL SPECTRUM #
  #################
  nuc = 'W186'
  unique_nbins = int(len(onums_dict.keys()) / 2)
  data = np.zeros(shape=(unique_nbins, 5))
  i = 0
  for ebins_path, tally_num in onums_dict.items():
    nbin = nbins_dict[ebins_path]
    uid = uids_dict[ebins_path]
    # recall mcnp spectrum and reverse order for FISPACT
    mcnp_spectrum = X.reactions[N, MT].mg_tallies[(nbin, uid)].nps[-1]['tally'][::-1]
    archive_path = collapse(mcnp_spectrum, temp, ebins_path, elements_list, parallel=False) 
    og_unshield, og_shield = analyse(archive_path, mcnp_spectrum, ebins_path)
    if nbin in data:
      if uid == 'tungopti':
        data[i,1] = og_shield[nuc]
        data[i,2] = og_unshield[nuc]
      elif uid == 'tunglogs':
        data[i,3] = og_shield[nuc]
        data[i,4] = og_unshield[nuc]
      else:
        exit("Unrecognised UID...") 
      i += 1
    else:
      data[i,0] = nbin
      if uid == 'tungopti':
        data[i,1] = og_shield[nuc]
        data[i,2] = og_unshield[nuc]
      elif uid == 'tunglogs':
        data[i,3] = og_shield[nuc]
        data[i,4] = og_unshield[nuc]
      else:
        exit("Unrecognised UID...") 
  # NBIN, OPT SH, OPT UNSH, LOG SH, LOG UNSH
  print(data)

  ####################
  # PW CROSS SECTION #
  ####################
  RR = X.reactions[N, MT].pw_tally.nps[-1]['tally']
  phi = X.reactions[N, MT].mg_tallies[(2048,'tunglogs')].nps[-1]['tally']
  pw_xs = RR/phi.sum()

  #############
  # SAVE DATA #
  #############
  if not os.path.exists(collapsed_data_filepath):
    pickle.dump([data, pw_xs], open(collapsed_data_filepath, 'wb'))
  return
    
def plot_xs_group_structure(N, MT, data, pw_sigma):
  # data is a 2D 5xlen(nbins) matrix
  # [:,0] NBIN, [:,1] OPT SH, [:,2] OPT UNSH, [:,3] LOG SH, [:,4] LOG UNSH
  f,ax = plt.subplots(figsize=(5.5,4.3))
  nbins = data[:,0]
  opt_sh = data[:,1]
  opt_unsh = data[:,2]
  log_sh = data[:,3]
  log_unsh = data[:,4]
  opt_sh_percent = 100 * (opt_sh - pw_sigma) / pw_sigma
  opt_unsh_percent = 100 * (opt_unsh - pw_sigma) / pw_sigma
  log_sh_percent = 100 * (log_sh - pw_sigma) / pw_sigma
  log_unsh_percent = 100 * (log_unsh - pw_sigma) / pw_sigma
  ax.set_xscale('log')
  ax.set_xticks(nbins)
  ax.set_xticklabels(labels=["{:d}".format(int(nbin)) for nbin in nbins], rotation='vertical', fontsize=10)
  ax.minorticks_off()
  ax.set_xlabel("Group structure")
  ax.set_ylabel(r"$\%\Delta$ on pointwise")
  minmin = min(min(opt_sh_percent), min(opt_unsh_percent))
  maxmax = max(max(log_sh_percent), max(log_unsh_percent))
  span = maxmax - minmin
  gap = 0.05 * span 
  ymin = minmin - gap
  ymax = maxmax + gap
  ax.set_ylim(ymin, 50)
  bx = ax.twinx()
  aymin = (ymin * pw_sigma) / 100 + pw_sigma
  aymax = (ymax * pw_sigma) / 100 + pw_sigma
  bx.set_ylim(aymin, aymax)
  bx.set_ylabel('Collapsed cross-section, $\sigma_{n,\gamma}$ [barns]')
  nuclide_str = '-'.join([N[0], str(N[1])])
  ax.set_title('{:s} collapsed cross-sections'.format(nuclide_str))
  ax.scatter(nbins, log_unsh_percent, label="Logspaced unshielded", color='blue', marker='x')
  ax.scatter(nbins, opt_unsh_percent, label="Optimised unshielded", color='purple', marker='o', facecolors='None')
  ax.scatter(nbins, log_sh_percent, label="Logspaced shielded", color='green', marker='x')
  ax.scatter(nbins, opt_sh_percent, label="Optimised shielded", color='red', marker='o', facecolors='None')
  ax.axhline(0, color='black', linewidth=1, linestyle='--')
  handles, labels = ax.get_legend_handles_labels()
  ax.legend(fontsize=8)
  plt.tight_layout()
  plt.subplots_adjust(left=0.17,right=0.83)
  f.savefig(nuclide_str + '.png', transparent=True)
  f.savefig(nuclide_str + '.eps')
  return

def analyse(run_path, phi, ebins_path):
  figure_path = os.path.join(run_path, 'figure')
  if os.path.exists(figure_path):
    pass
  else:
    os.mkdir(figure_path)
  output_path = os.path.join(run_path, 'output')
  ssf_data_filename = os.path.join(run_path, 'ssf_data.pkl')
  symbol_map_filename = 'elements.csv'
  E = utility.get_energies(ebins_path)

  # get database of SSFs
  if os.path.exists(ssf_data_filename):
    X = load_obj(ssf_data_filename)
  else:
    X = read_output.build_database(E, phi, output_path)
    read_output.save_obj(X, ssf_data_filename)

  sym_dict = read_output.get_sym_dict(symbol_map_filename)
  ssf_dict = {}
  ssf_abs_dict = {}
  
  og_unshield = {}
  og_shield = {}
  for nuc in tqdm(X.keys()):
    output_filepath = os.path.join(output_path, '{}.out'.format(nuc))
    N_tup = nuc_str_to_N_tuple(nuc)
    og_unshield[nuc], og_shield[nuc] = run_printlib.get_collapsed_xs(N_tup, output_filepath)
    E_res, rr_tot, rr_tot_sh, ssf, ssf_bar, g0, gn = read_output.fold_total(X, E, phi, nuc)
    E_res_abs, rr_abs, rr_abs_sh, ssf_abs, ssf_abs_bar, g0_abs, gn_abs = read_output.fold_abs(X, E, phi, nuc)
    ssf_dict[nuc] = ssf_bar
    ssf_abs_dict[nuc] = ssf_abs_bar
    Z, N = read_output.get_Z_N(sym_dict, nuc) # Z, N are ints

  # capture SSF bar for all nuclides, print output
  ssf_dict = OrderedDict(sorted(ssf_dict.items(), key=lambda t: t[1], reverse=True))
  ssf_list = []
  ssf_abs_dict = OrderedDict(sorted(ssf_abs_dict.items(), key=lambda t: t[1], reverse=True))
  ssf_abs_list = []
  with open(os.path.join(run_path, 'ssf_summary.out'), 'w') as f:
    for nuc in ssf_abs_dict.keys():
      ssf_bar = ssf_dict[nuc]
      ssf_abs_bar = ssf_abs_dict[nuc]
      if np.isfinite(ssf_bar):
        result_str = "{:8s} {:1.4f} {:1.4f}\n".format(nuc, ssf_bar, ssf_abs_bar)
        ssf_list.append(ssf_dict[nuc])
        ssf_abs_list.append(ssf_abs_dict[nuc])
        f.write(result_str)
  return og_unshield, og_shield

def nuc_str_to_N_tuple(nuc):
  """Expects 'W186' or 'O16' type formatting"""
  str_list = re.split("[^a-zA-Z]*", nuc)
  element = "".join(str_list)
  num_list = re.split("[^0-9]*", nuc)
  atomic_mass = int("".join(num_list))
  return (element, atomic_mass)

def collapse(phi, temp, ebins_filepath, elements_list=None, parallel=True):
  """With spectral information and a group structure, run FISPACT collapse for specified elements
  making nuclear data if necessary (PENDFs, GENDFs & PROBTABLES), then read output and store in
  .pkl database, then analyse and plot results."""
  # get initial bin bounds array
  ebins_filename = ebins_filepath.split('/')[-1]
  if len(ebins_filename.split('_')) != 3:
    exit('ebins filename, {:s} does not contain a unique ID'.format(ebins_filename))
  uid = ebins_filename.split('_')[2]
  nbins = int(ebins_filename.split('_')[1])
  bins = utility.get_energies(ebins_filepath)
  assert nbins == len(bins) - 1
  print('Using {} bins with group structure ID {}...'.format(nbins, uid))

  # directories and paths for SSF generation
  reduced_index_path = 'reduced_nuclide_index'
  fispact_files_filename = 'files.tendl15-n'
  ssf_data_dir = '/Users/Fred/projects/SSF_study/ssf_data/'

  # directories for nuclide list
  endfb7_transport_gxs_dir = '/opt/FISPACT-II-3-20/ENDFdata/ENDFB71data/endfb71-n/gxs-709/'
  tendl_probt_dir = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tal2015-n/tp-709-294/'
  tendl_decay_index = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tendl15_decay12_index'

  # gather list of desired nuclides (compare available ENDFs and desired files) and write nuclide index file 
  print('Assembling nuclide list...')
  nuclide_str_list, lines, mat_nums = gather_nuclide_set.get_list(endfb7_transport_gxs_dir, tendl_decay_index, tendl_probt_dir, elements_list)
  print('Writing {} nuclides to a reduced index...'.format(len(nuclide_str_list)))
  print(nuclide_str_list)
  gather_nuclide_set.write_list(lines, reduced_index_path)
  nuclide_object_list = [Nuclide(tup[0], tup[1]) for tup in zip(nuclide_str_list, mat_nums)]

  # bin dependent paths
  # does the MG and PT data exist?
  probt_dir = '/Users/Fred/projects/SSF_study/probt_data/tp-{:d}-{:d}_{:s}'.format(nbins, temp, uid)
  if not os.path.exists(probt_dir):
    gen_PT.run(ebins_filepath, nuclide_object_list, temp, parallel=parallel)
    if not os.path.exists(probt_dir):
      exit("Probability table data wasn't available, tried to create it and failed...")
  gendf_dir = '/Users/Fred/projects/SSF_study/gendf_data/gxs-{:d}_{:s}'.format(nbins, uid)
  if not os.path.exists(gendf_dir):
    gen_MG.run(ebins_filepath, nuclide_object_list, temp)
    if not os.path.exists(gendf_dir):
      exit("GENDF data wasn't available, tried to create it and failed...")
    
  # make fluxes file for MCNP sampled spectral information
  utility.write_arb_fluxes(phi)
  time_str = "_".join(str(datetime.now()).split())
  time_str = time_str.replace(":", "-")
  archive_path = os.path.join(ssf_data_dir, "{:d}_{:s}".format(nbins, uid), "arb-{:s}".format(time_str))

  # paths for archiving of results
  input_path = os.path.join(archive_path, 'input')
  output_path = os.path.join(archive_path, 'output')
  log_path = os.path.join(archive_path, 'log')

  # make symlinks to MG and PT data in working dir
  print('Symlinking MG and PT data into working directory...')
  init_dir = os.getcwd()
  gendf_symlink = 'gxs-{:d}'.format(nbins)
  probt_symlink = 'tp-{:d}-{:d}'.format(nbins, temp)
  call('ln -s {:s} {:s}'.format(gendf_dir, gendf_symlink), shell=True)
  call('ln -s {:s} {:s}'.format(probt_dir, probt_symlink), shell=True)

  # make files file
  print('Generating FISPACT files file...')
  gen_SSF.make_files_file(ebins_filepath, gendf_symlink, probt_symlink, fispact_files_filename)

  # do main loop of FISPACT runs for all nuclides (threaded)
  gen_SSF.run_FISPACT(fispact_files_filename, input_path, output_path, log_path, nbins, nuclide_str_list, init_dir, compressed=False)

  # delete MG and PT symlinks
  print('Removing symlinks...')
  os.chdir(init_dir)
  os.remove(gendf_symlink)
  os.remove(probt_symlink)
  return archive_path

def gen_mcnp_tally(ebins_path, cell_num, cell_vol, optimised, tally_type=4):
  arr = utility.get_energies(ebins_path)
  nbins = len(arr) - 1
  exponent = math.log(nbins) / math.log(2)
  tally_num = int(exponent) * 100 + tally_type + (optimised * 10)
  arr /= 1E6
  tally_str = 'c\nc {:s}\n'.format(ebins_path)
  tally_str += 'F{:d}:N {:d}\n'.format(tally_num, cell_num)
  tally_str += 'SD{:d} {:1.4f}\n'.format(tally_num, cell_vol)
  tally_str += 'E{:d}  '.format(tally_num)
  tally_str += utility.make_mcnp_energies(arr)
  return tally_num, tally_str
     
if __name__ == "__main__":
  main()

