#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt

#before_path='../ssf_data/280_fusifoil/E^0.19200/ssf_summary.out'
#after_path='../ssf_data/280_c9c95cd6/E^0.19200/ssf_summary.out'
before_path='../ssf_data/650_fusifoil/E^0.19200/ssf_summary.out'
after_path='../ssf_data/650_43accaf8/E^0.19200/ssf_summary.out'
 
 


def read_file(path):
  score_dict = {}
  with open(path, 'r') as f:
    for line in f.readlines():
      score_dict[line.split()[0]] = float(line.split()[-1])
  return score_dict

before = read_file(before_path)
after = read_file(after_path)

comparison = {}
for nuc, before_score in before.items():
  for after_nuc, after_score in after.items():
    if nuc == after_nuc:
      comparison[nuc] = after_score - before_score

comparison_list = sorted(comparison.items(), key=lambda x: x[1], reverse=True)
nuc_list = [x[0] for x in comparison_list]
score_list = [x[1] for x in comparison_list]
f,ax=plt.subplots(figsize=(7,4))
ax.plot(range(len(nuc_list)), score_list)
ax.axhline(0, ls='--', color='grey')
ax.set_xticks(range(len(nuc_list)))
ax.set_xticklabels(nuc_list, rotation='vertical', fontsize=8)
ax.set_ylabel(r'$\Delta\overline{\mathrm{SSF}}$')
ax.set_title('Comparison of self-shielding for radiative capture with/out rebinning')
plt.tight_layout()
plt.show()
