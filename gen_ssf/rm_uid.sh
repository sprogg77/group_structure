#!/bin/sh

nbins=$1
uids_to_delete=$2
while IFS= read -r line
do
  echo "Deleting ${line}..."
  gendf_path="/Users/Fred/projects/SSF_study/gendf_data/gxs-${1}_"${line}"/"
  probt_path="/Users/Fred/projects/SSF_study/probt_data/tp-${1}-294_"${line}"/"
  ssf_data="/Users/Fred/projects/SSF_study/ssf_data/${1}_"${line}"/"
  ebins_path="/Users/Fred/projects/SSF_study/ebins/ebins_${1}_"${line}""
  trash $gendf_path
  trash $probt_path
  trash $ebins_path
  trash $ssf_data
done <"${uids_to_delete}"

