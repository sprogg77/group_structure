#!/usr/bin/env python3

import utility
import gen_SSF
import read_output
import gather_nuclide_set
from determine_group import sk_gauss_rebin, skewed_gaussian, skewed_gaussian_fit, discrete_rebin
from nuclide import Nuclide
import gen_MG
import gen_PT
import os
import uuid
import numpy as np
from fill_between_steps import fill_between_steps
import matplotlib.pyplot as plt
import mimic_alpha as ma
from subprocess import call
from datetime import datetime
from sys import argv

def main():
  if len(argv) != 5:
    print("Please give x where phi(E) = E^x, temperature in K, minimum bins / dec. of new groups and starting ebins path...")
    exit("e.g. -1 294 10 /Users/Fred/projects/SSF_study/ebins/ebins_1001_4rez2d4f")
  else:
    exponent = float(argv[1])
    temp = int(np.round(float(argv[2])))
    bpd_min = int(argv[3])
    ebins_filepath = argv[4]
    nbins = int(ebins_filepath.split('/')[-1].split('_')[1])
    fit = False
#    elements_list = ['Fe', 'W', 'Mo', 'Nd', 'Sn', 'Zr', 'Cu', 'Co', 'Ta']
#    elements_list = None
    elements_list = ['H', 'O']
    iteration_limit = 1
    E_min = 1E-5
    E_max = 1E8

  bounds_plot_list = []
  A_ssf_list = []
  for i in range(1, iteration_limit + 1): # young becomes old...
    print("****************************")
    print("    Iteration {} of {}...   ".format(i, iteration_limit))
    print("****************************")
    # input ebins filepath and output bins are the same OLD bins
    bins, ssf_effect, archive_path, uid = run(exponent, temp, ebins_filepath, elements_list) 
    assert nbins == len(bins) - 1
    bins, ebins_filepath, bounds_plot_path, A_ssf = optimise_group(bins, ssf_effect, nbins, archive_path, bpd_min, uid, fit=fit, E_min=E_min, E_max=E_max) # returns NEW reassigning bins
    bounds_plot_list.append(bounds_plot_path)
    print("Integrated SSF effect: {:1.4f}".format(A_ssf))
    A_ssf_list.append(A_ssf)
    utility.make_ebins_file(bins, ebins_filepath) # record these new bins for next iteration
  arg_str = " ".join(bounds_plot_list)
  print("Making animation of bound creation...")
  call("convert -delay 20 {} anim.gif".format(arg_str), shell=True)
  print("All done...")
  print("UID      A_ssf")
  with open('uid_score', 'w') as f:
    for i, path in enumerate(bounds_plot_list):
      uid = path.split('/')[-3].split('_')[-1]
      print(uid, A_ssf_list[i])
      f.write("{:s} {:1.4f}\n".format(uid, A_ssf_list[i]))
  return

def run(exponent, temp, ebins_filepath, elements_list=None, fluxes=None):
  """With spectral information and a group structure, run FISPACT collapse for specified elements
  making nuclear data if necessary (PENDFs, GENDFs & PROBTABLES), then read output and store in
  .pkl database, then analyse and plot results."""
  # get initial bin bounds array
  ebins_filename = ebins_filepath.split('/')[-1]
  if len(ebins_filename.split('_')) != 3:
    exit('ebins filename, {:s} does not contain a unique ID'.format(ebins_filename))
  uid = ebins_filename.split('_')[2]
  nbins = int(ebins_filename.split('_')[1])
  bins = utility.get_energies(ebins_filepath)
  assert nbins == len(bins) - 1
  print('Using {} bins with group structure ID {}...'.format(nbins, uid))

  # directories and paths for SSF generation
  reduced_index_path = 'reduced_nuclide_index'
  fispact_files_filename = 'files.tendl15-n'
  ssf_data_dir = '/Users/Fred/projects/SSF_study/ssf_data/'

  # directories for nuclide list
  endfb7_transport_gxs_dir = '/opt/FISPACT-II-3-20/ENDFdata/ENDFB71data/endfb71-n/gxs-709/'
  tendl_probt_dir = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tal2015-n/tp-709-294/'
  tendl_decay_index = '/opt/FISPACT-II-3-20/ENDFdata/TENDL2015data/tendl15_decay12_index'

  # gather list of desired nuclides (compare available ENDFs and desired files) and write nuclide index file 
  print('Assembling nuclide list...')
  nuclide_str_list, lines, mat_nums = gather_nuclide_set.get_list(endfb7_transport_gxs_dir, tendl_decay_index, tendl_probt_dir, elements_list)
  print('Writing {} nuclides to a reduced index...'.format(len(nuclide_str_list)))
  print(nuclide_str_list)
  gather_nuclide_set.write_list(lines, reduced_index_path)
  nuclide_object_list = [Nuclide(tup[0], tup[1]) for tup in zip(nuclide_str_list, mat_nums)]

  # bin dependent paths
  # does the MG and PT data exist?
  probt_dir = '/Users/Fred/projects/SSF_study/probt_data/tp-{:d}-{:d}_{:s}'.format(nbins, temp, uid)
  if not os.path.exists(probt_dir):
    gen_PT.run(ebins_filepath, nuclide_object_list, temp)
    if not os.path.exists(probt_dir):
      exit("Probability table data wasn't available, tried to create it and failed...")
  gendf_dir = '/Users/Fred/projects/SSF_study/gendf_data/gxs-{:d}_{:s}'.format(nbins, uid)
  if not os.path.exists(gendf_dir):
    gen_MG.run(ebins_filepath, nuclide_object_list, temp)
    if not os.path.exists(gendf_dir):
      exit("GENDF data wasn't available, tried to create it and failed...")
    
  if fluxes: # passed from argument
    # make fluxes file for MCNP sampled spectral information
    utility.write_arb_fluxes(phi)
    time_str = "_".join(str(datetime.now()).split())
    time_str = time_str.replace(":", "-")
    archive_path = os.path.join(ssf_data_dir, "{:d}_{:s}".format(nbins, uid), "arb-{:s}".format(time_str))
  else:
    # make fluxes file given phi(E) = E*exponent
    print('Calculating fluxes from exponent and writing to file...')
    utility.write_fluxes(ebins_filepath, exponent, nbins)
    archive_path = os.path.join(ssf_data_dir, "{:d}_{:s}".format(nbins, uid), "E^{:1.5f}".format(exponent))

  # paths for archiving of results
  input_path = os.path.join(archive_path, 'input')
  output_path = os.path.join(archive_path, 'output')
  log_path = os.path.join(archive_path, 'log')

  # make symlinks to MG and PT data in working dir
  print('Symlinking MG and PT data into working directory...')
  init_dir = os.getcwd()
  gendf_symlink = 'gxs-{:d}'.format(nbins)
  probt_symlink = 'tp-{:d}-{:d}'.format(nbins, temp)
  call('ln -s {:s} {:s}'.format(gendf_dir, gendf_symlink), shell=True)
  call('ln -s {:s} {:s}'.format(probt_dir, probt_symlink), shell=True)

  # make files file
  print('Generating FISPACT files file...')
  gen_SSF.make_files_file(ebins_filepath, gendf_symlink, probt_symlink, fispact_files_filename)

  # do main loop of FISPACT runs for all nuclides (threaded)
  gen_SSF.run_FISPACT(fispact_files_filename, input_path, output_path, log_path, nbins, nuclide_str_list, init_dir, compressed=False)

  # delete MG and PT symlinks
  print('Removing symlinks...')
  os.chdir(init_dir)
  os.remove(gendf_symlink)
  os.remove(probt_symlink)

  # read in data, pickle to database, plot nuclide-wise and trends
  print('Assembling/reading output database and plotting results...')
  read_bins, ssf_effect = read_output.analyse(archive_path, exponent, ebins_filepath, nbins, uid)
  assert np.all(bins == read_bins)
  return bins, ssf_effect, archive_path, uid

def optimise_group(old_bins, bin_density, nbins, archive_path, bpd_min, old_uid, fit=False, E_min=1E-5, E_max=1E9, new_uid=None):
  for i, val in enumerate(old_bins[:-1]):
    if old_bins[i+1] > old_bins[i]:
      continue
    else:
      # reverse input arrays to ascending energy order
      old_bins = old_bins[::-1]
      bin_density = bin_density[::-1]
  # number of decades
  d = np.log10(E_max) - np.log10(E_min)
  if bpd_min * d > nbins:
    print("No excess bins available for increased resolution...")
    exit("{} * {} = {}, less than requested bins: {}".format(bpd_min, d, int(bpd_min * d), nbins))

  if fit: 
    # do fit to SSF effect data
    print('Fitting SSF effect distribution...')
    fitted_params, result = skewed_gaussian_fit(np.log10(old_bins), bin_density)
    func = skewed_gaussian
    # make bounds given distribution
    print('Generating new bin bounds using SSF effect distribution as bin density...')
    optimised_bin_bounds, offset_params, A_ssf = sk_gauss_rebin(func, E_min, E_max, nbins, d, bpd_min, *fitted_params, tol=0.0001, plot=False)
    background = offset_params[-1]
  else:
    print('Working directly from discrete data...')
    print('Generating new bin bounds using SSF effect distribution as bin density...')
    optimised_bin_bounds, background, A_ssf = discrete_rebin(np.log10(old_bins), bin_density, nbins, d, bpd_min, plot=True)

  if new_uid: # passed by argument
    pass
  else:
    # new group UID
    new_uid = str(uuid.uuid4()).split('-')[0] # generate new UID and corresponding path
  new_ebins_filepath = '/Users/Fred/projects/SSF_study/ebins/ebins_{:d}_{:s}'.format(nbins, new_uid)

  # plot SSF effect, group density and groups
  f,ax = plt.subplots(figsize=(6,5))
  max_y = 3
  light_red = ma.colorAlpha_to_rgb((1,0,0),0.4)[0]
  ax.step(np.log10(old_bins), 
    bin_density + background,
    label="Bin density", 
    color='red', 
    linewidth=0.75)
  fill_between_steps(ax, np.log10(old_bins), bin_density + background, 0, color=light_red)
  bin_lines = [([E, E], [0, max_y]) for E in np.log10(optimised_bin_bounds)]
  pale_green = ma.colorAlpha_to_rgb('g', 0.4)[0]
  for i, line in enumerate(bin_lines):
    if i != 0:
      ax.plot(line[0], line[1], color=pale_green, linewidth=0.5)
    else:
      ax.plot(line[0], line[1], color=pale_green, linewidth=0.5, label="Optimised bounds")
#  time_str = str(datetime.now())
#  ax.text(0.02, 0.95, time_str, transform=ax.transAxes, fontsize=8)
  if fit:
    params_str = "".join(["{:>14} = {: 1.2f}\n".format(p, result.params[p].value) for p in result.params])
    E_fine = np.linspace(-5,9,2000)
    ax.plot(E_fine, 
      skewed_gaussian(E_fine, *offset_params), 
      label="\nSkewed Gaussian fit:\n\n{:>14} = {: 1.2f}\n".format('const. offset', background) + params_str)
  ax.set_xlabel('log$_{10}$(E)')
  ax.set_ylabel(r'Bin density, $\rho(E)$')
  ax.set_ylim(0, max_y)
  ax.set_xlim(-2, 6)
  ax.set_title("Optimised {} bin group structure".format(nbins))
  ax.legend(prop={'family':'monospace'}, loc='upper right')
  ax.grid()
  plot_path = os.path.join(archive_path, 'optimised_bounds.eps')
  plot_path_png = os.path.join(archive_path, 'optimised_bounds.png')
  plt.savefig(plot_path)
  plt.savefig(plot_path_png, transparent=True)
  return optimised_bin_bounds, new_ebins_filepath, plot_path, A_ssf

if __name__ == "__main__":
  main()





